"use strict";
const Router = require('koa-router');
const auth = require('./sessionControl');
const mysqlConnections = require('./mysqlConnections');
const statusCode = require('../constants/httpStatusConstants.js');
const jsonBody = require('koa-json-body')();
const bcrypt = require('bcrypt');
const jwt = require('koa-jwt');
const mandrill = require('../experts/mandrill');

const USER_JWT_KEY = '38F720C5-F711-4FB2-B1EA-139861C936AA';
const IS_ACCEPTED = 1;
const IS_EMAIL_VERIFIED = 1;
const IS_TWITTER_VERIFIED = 1;
const IS_JOURNUALIST =  20;

const SOMETHING_WRONG = "Something was wrong, please try again.";


const router = new Router({
    prefix:'/users'
});

router.get('/', function *(next) {
        this.status = 404;
    })

.get('/email.verification/resend/:email', function *(next) {

    let user = yield _getUserByEmail(this.params.email);

    if (!user) {
        this.body = {
            message: SOMETHING_WRONG
        };
        this.status = statusCode.SERVER_ERROR;
        return
    }

    yield _sendVerificationEmail(user, user.user_code);

    this.status = statusCode.OK;
})

.get('/email.verification/:email/:code', function *(next) {

    const user = yield _getUserByEmail(this.params.email);
    const payload = _validateJwt(this.params.code);

    if (!payload || payload.code !== user.user_code) {
        this.body = "Validation error";
        this.status = statusCode.NOT_FOUND;
        return;
    }

    if(user.user_pass){
        this.status = statusCode.OK;
        this.body = { code: this.params.code };
        return;
    }

    const authorizationResult = yield _authorizeCode(user.user_id);

    if (authorizationResult.affectedRows != 1) {
        this.body = "Error while updating user";
        this.status = statusCode.NOT_FOUND;
        return;
    }

    this.status = statusCode.OK;
    this.body = { code: this.params.code, progress: 1 };
})

.post('/', jsonBody, function *(next) {

    let isValidEmail = _validateEmail(this.request.body.email);
    if(!isValidEmail){
        this.body = 'Invalid email';
        this.status = 400;
        return;
    }

    try {

        const insertNewUserResult = yield _insertNewUser(this.request.body);
        if (insertNewUserResult.affectedRows != 1) {
            this.body = {message: SOMETHING_WRONG};
            this.status = statusCode.SERVER_ERROR;
            return;
        }

        const userData = {user_name: this.request.body.name, user_email: this.request.body.email};
        const signedCode = yield _sendVerificationEmail(userData, insertNewUserResult.code);

        this.body = { c: signedCode };
        this.status = statusCode.CREATED;

    } catch (err){
        console.log(err);
        this.body = {message:"Email account already used"};
        this.status = statusCode.NOT_ALLOWED;
    }
})

.put("/reader/:user_id", function * (next) {

    /*
     *  HUEVO KINDER PARA EL DAMIAN !!
     */
    console.log('y ahora');
    if(parseInt(this.params.user_id) != this.params.user_id){
        this.status = 500;
        this.body = {
            message: "bad params"
        };
        return;
    }

    // if(this.params.user_id != auth.getSession(this.cookies.get("KOASESSID")).id ){
    //     this.status = 500;
    //     this.body = {
    //         message: "is not user"
    //     };
    //     return;
    // }

    const wasSetedAsReader = yield _setAsReader(this.params.user_id);

    if (!wasSetedAsReader) {
        this.status = 500;
        this.body = {
            message: "error to change role"
        };
        return;
    }

    this.status = statusCode.OK
})

.put("/email.change/:code/:email", jsonBody, function *(next) {

    const user = yield _getUserByEmail(this.params.email);
    const payload = jwt.verify(this.params.code, USER_JWT_KEY);

    if (!payload || payload.code !== user.user_code) {
        this.body = "Validation error";
        this.status = statusCode.NOT_FOUND;
        return;
    }

    const passSaveResult = yield _changeEmail(user.user_id, this.request.body.email);

    if ( !passSaveResult) {
        this.body = "Error while updating user";
        this.status = statusCode.SERVER_ERROR;
        return;
    }

    user.user_email = this.request.body.email;
    yield _sendVerificationEmail(user, user.user_code);

    this.body = 'Done';
})

.put("/:code/:email", jsonBody, function *(next) {

    const user = yield _getUserByEmail(this.params.email);
    const payload = jwt.verify(this.params.code, USER_JWT_KEY);

    if (!payload || payload.code !== user.user_code) {
        this.body = "Validation error";
        this.status = statusCode.NOT_FOUND;
        return;
    }

    const passSaveResult = yield _savePassword(user.user_id, this.request.body.password);

    if ( !passSaveResult) {
        this.body = "error to save";
        this.status = statusCode.SERVER_ERROR;
        return;
    }

    this.status = 200;
});

module.exports = router;


/******************************************************************************/
// Funciones privadas
/******************************************************************************/

function * _insertNewUser(user_data) {

    const query = "INSERT INTO users SET " +
        "user_name=?, " +
        "user_email=?, " +
        "user_date_create=?," +
        "user_accepted=?," +
        "user_code=?, " +
        "role_id=?";

    const user_code = yield _randomString(12);
    const user_date_created = yield _createUnixTimestamp();

    const queryParams = [
        user_data.name,
        user_data.email,
        user_date_created,
        IS_ACCEPTED,
        user_code,
        IS_JOURNUALIST
    ];

    const queryResult =  yield mysqlConnections.queryParamsDatabase(query, queryParams);

    return {
        code: user_code,
        affectedRows: queryResult.affectedRows
    }
}

function * _randomString(length) {
    let text = "";
    let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < length; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

function * _authorizeCode(user_id) {

    const query = "UPDATE users SET " +
        "user_email_verified=?, " +
        "user_date_email_verified = ?  " +
        "WHERE user_id = ?";

    const user_date_email_verified = yield _createUnixTimestamp();

    const queryParams = [
        IS_EMAIL_VERIFIED,
        user_date_email_verified,
        user_id
    ];

    return yield mysqlConnections.queryParamsDatabase(query, queryParams)
}

function * _savePassword(user_id, pass) {

    const query = "UPDATE users SET " +
        "user_pass=? " +
        "WHERE user_id = ?";

    const hash_pass = bcrypt.hashSync( pass, 10);

    const queryParams = [
        hash_pass,
        user_id
    ];

    const result =yield mysqlConnections.queryParamsDatabase(query, queryParams);

    return result.affectedRows == 1;
}

function * _setAsReader(user_id) {

    const query = "UPDATE users SET " +
        "role_id=? " +
        "WHERE user_id = ?";

    const queryParams = [
        10,
        user_id
    ];

    const result =yield mysqlConnections.queryParamsDatabase(query, queryParams);

    return result.affectedRows == 1;
}

function * _changeEmail(user_id, email) {

    const query = "UPDATE users SET " +
        "user_email=? " +
        "WHERE user_id = ?";

    const queryParams = [
        email,
        user_id
    ];

    const result =yield mysqlConnections.queryParamsDatabase(query, queryParams);

    return result.affectedRows == 1;
}

function * _sendVerificationEmail(userData, code) {

    var iat = new Date();
    var exp = new Date();

    // Set expiration time
    exp.setMinutes(exp.getMinutes() + 1440); // 24 horas
    iat = Math.floor(Number(iat)/1000);
    exp = Math.floor(Number(exp)/1000);

    const payload = { code: code, iat: iat, exp: exp };
    const signedCode = jwt.sign(payload, USER_JWT_KEY);

    // const hash_code = bcrypt.hashSync( code, 10);
    // const encode_hash = encodeURIComponent(encodeURIComponent(hash_code));
    console.log('Enviando email con ');
    console.log(userData);
    mandrill.sendEmailVerification(userData, signedCode);

    return signedCode;
}

function * _getUserByEmail(email) {
    const query = "SELECT * FROM users WHERE user_email = ? LIMIT 1";
    const values = [email];
    const response = yield mysqlConnections.queryParamsDatabase(query, values);

    if (response.length != 1) {
        return false
    }

    return response[0]
}

function * _createUnixTimestamp() {
    const local_time = new Date();
    return Math.floor((local_time.getTime() + local_time.getTimezoneOffset() * 60000) / 1000);
}

/*
 * Valida 8 caracteres minimo, 1 Mayuscula, 1 número
 */
function * _validatePassword(password){
    return /(?=^.{8,}$)(?=.*\d)(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/.test(password)
}


/*
 * Decodifica y verifica que el hash sea valido y no esté expirado
 */
function _validateJwt(signedCode){

    try {
        return jwt.verify(signedCode, USER_JWT_KEY);

    } catch(e) {
        console.log(e);
        return false;
    }
}

function _validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
