const Router = require('koa-router');
const jsonBody = require('koa-json-body')();
const models = require('express-cassandra');
const auth = require('./sessionControl');
const mysqlConnections = require('./mysqlConnections');

var router = new Router({
    prefix: '/resources'
});

router.get('/', auth.isJournalist, function *(next) {
    this.body = 'estoy adentro de resources !';
})

.get('/get.embassy.countries', auth.isJournalist, function *(next) {

    const query = "SELECT em_co_id as value, em_co_name as label FROM embassy_countries";
    const response = yield mysqlConnections.queryDatabaseResources(query);

    this.body = response;
})

.get('/get.embassies/:from/:to', auth.isJournalist, function *(next) {

    const params = [this.params.from, this.params.to];
    const query = "SELECT * FROM embassies WHERE em_co_origin_id = ? AND em_co_to_id = ?";
    const response = yield mysqlConnections.queryDatabaseResources(query, params);

    this.body = response;
})

.get('/get.visa.countries', auth.isJournalist, function *(next) {

    const query = "SELECT vi_na_id as value, vi_na_name as label FROM visa_nationalities";
    const response = yield mysqlConnections.queryDatabaseResources(query);

    this.body = response;
})

.get('/get.visa/:vi_na_id', auth.isJournalist, function *(next) {

    const query = "SELECT vi_image as url FROM visas WHERE vi_na_id = ?";
    const params = [this.params.vi_na_id];
    const response = yield mysqlConnections.queryDatabaseResources(query, params);

    this.body = response[0] ? response[0] : [];
})

.get('/get.grants/:country/:limit', auth.isJournalist, function *(next) {

    const limit = parseInt(this.params.limit) || 200;
    const country = this.params.country;

    var grants = yield function(findCallback) {
        models.instance.Grants.find({country_short: country, $limit: limit}, {raw:true}, findCallback);
    }

    this.body = grants;
})

.post('/save.grant', auth.isAdmin, jsonBody, function *(next) {

    var primaryKey = {
        country_short:this.request.body.country_short,
        timeuuid: models.timeuuid(),
        deadline: parseInt(this.request.body.deadline),
        amount: parseInt(this.request.body.amount) ? parseInt(this.request.body.amount) : 0
    };

    var fields = {
        title:this.request.body.title,
        description:this.request.body.description,
        url:this.request.body.url,
        location:this.request.body.location,
        focus:this.request.body.focus
    };

    models.instance.Grants.update(primaryKey, fields, function(err){
        if(err) {
            console.log(err);
        }
    });

    this.body = 'Done';
})

.post('/delete.grant', auth.isAdmin, jsonBody, function *(next) {

    var timeUuid = models.timeuuidFromString(this.request.body.timeuuid);

    var primaryKey = {
        country_short: this.request.body.country_short,
        timeuuid: timeUuid,
    };

    models.instance.Grants.delete(primaryKey, function(err){
        if(err) {
            console.log(err);
        }
    });

    this.body = 'Done';
});



module.exports = router;
