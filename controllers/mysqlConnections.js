const koaMysql = require('koa-mysql');

var dbPool = koaMysql.createPool({
    user: 'remoto',
    password: 'n0Sabr1aDec1rt3',
    database: 'mainwejo',
    host: '35.185.127.103'
});

var dbPoolResources = koaMysql.createPool({
    user: 'remoto',
    password: 'n0Sabr1aDec1rt3',
    database: 'resource_index',
    host: '35.185.127.103'
});

module.exports.queryDatabase = function *(queryString, paramValues){
    if(paramValues){
        return yield dbPool.query(queryString, paramValues);
    }else{
        return yield dbPool.query(queryString);
    }
};

/* Obsoleto, no usar*/
module.exports.queryParamsDatabase = function *(queryString, paramValues){
    return yield dbPool.query(queryString, paramValues);
};

module.exports.queryDatabaseResources = function *(queryString, paramValues){

    if(paramValues){
        return yield dbPoolResources.query(queryString, paramValues);
    }else{
        return yield dbPoolResources.query(queryString);
    }
};

// Por ahora no esta en uso. Desde discussions.jsno es posible usarlo adentro de un metodo export.
// module.exports.getPool = function *(queryString){
//     return dbPool;
// };
