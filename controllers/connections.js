const Router = require('koa-router');
const models = require('express-cassandra');
const auth = require('./sessionControl');
const mysqlConnections = require('./mysqlConnections');
const graphJournalist = require('./graph/journalists.js');


var router = new Router({
  prefix: '/connections'
});


router.get('/', function *(next) {
    this.body = 'estoy adentro de connections !';
})

.get('/isengaged/:user_id', auth.isJournalist, function *(next) {

    const currentUserID = auth.getSession(this.cookies.get("KOASESSID")).id;
    const userID = parseInt(this.params.user_id);

    if(parseInt(userID) != userID || userID == currentUserID){
        this.status = 400;
        return;
    }

    var engagement =  false;
    if(currentUserID !=userID){
        engagement = yield graphJournalist.isEngaged(currentUserID, userID);
    }

    if(engagement){
        this.body = {'timestamp_requested':engagement.timestamp_requested, 'timestamp_accepted':engagement.timestamp_accepted};
    }else{
        this.body = '';
    }
})


/*
 * Called to show a user profile
 */
.get('/profile/:user_id', auth.isJournalist, function *(next) {

    var userID = parseInt(this.params.user_id);
    var currentUserID = auth.getSession(this.cookies.get("KOASESSID")).id;

    if(userID != parseInt(userID) || currentUserID != parseInt(currentUserID)){
        this.body = 'Invalid Params';
        return;
    }

    if(userID == 0) userID = currentUserID;

    // Get GRAPH profile
    var userProfile = yield graphJournalist.getUser(userID);

    //Verify following status
    userProfile.isFollowing =  false;
    if(currentUserID !=userID){
        userProfile.isFollowing = yield graphJournalist.isFollowing(currentUserID, userID);
    }

    userProfile.engagement =  false;
    if(currentUserID !=userID){
        userProfile.engagement = yield graphJournalist.isEngaged(currentUserID, userID);
    }

    this.body = userProfile;
})

// /*
//  * Users admin dashboard
//  */
.get('/users', auth.isAdmin, function *(next) {

    //Build query string
    var mysqlSelect = "select user_id id, " +
        " user_name name, " +
        " user_email email, " +
        " user_date_create, " +
        " user_email_verified, " +
        " user_accepted, " +
        " user_twitter_followers, " +
        " user_twitter_retweets " +
        " from users order by user_date_create desc";

    //Get the mySQL users
    this.body = yield mysqlConnections.queryDatabase(mysqlSelect);
});

function findCallback(err, result){
    if(err) throw err;
    return result;
}

function mysqlCallback(err, rows){
    if(err) throw err;
    return rows;
}

function replaceAll(str, search, replacement) {
    // replacement = escapeRegExp(replacement);
    return str.replace(new RegExp(search, 'g'), replacement);
};

function escapeRegExp(str) {
  return str.replace(/[.*+?^${}()|[\]\\]/g, "\\$&"); // $& means the whole matched string
}


module.exports = router;

module.exports.requested = function(params){

    var userID = params.users[0];
    var otherUSerID = params.users[1];
    var timeRequest = Date.now();

    var connectionWhere = {
        user_id: userID,
        other_user_id: otherUSerID
    };

    var connectionUpdate = {
        accepted: false,
        irequested: true,
        timestamp_requested: timeRequest,
        timestamp_accepted: models.datatypes.unset,
        name: params.names[1],
        pic: params.pics[1],
        location: params.locations[1],
        country: models.datatypes.unset,
        country_id: models.datatypes.unset
    };

    models.instance.Connection.update(connectionWhere, connectionUpdate, function(err){
        if(err) console.log(err);
    });

    connectionWhere.user_id = otherUSerID;
    connectionWhere.other_user_id = userID;

    connectionUpdate.name = params.names[0];
    connectionUpdate.pic = params.pics[0];
    connectionUpdate.location = params.locations[0];
    connectionUpdate.irequested = false;

    models.instance.Connection.update(connectionWhere, connectionUpdate, function(err){
        if(err) console.log(err);
    });

    graphJournalist.engageRequested(userID, otherUSerID, timeRequest);
}

module.exports.rejected = function(params){
    _saveEngagementResponse(params, false);
}

module.exports.accepted = function(params){
    _saveEngagementResponse(params, true);
}

function _saveEngagementResponse(params, isAccepted){

    const userID = parseInt(params.user_id);
    const otherUserID = parseInt(params.other_user_id);

    var timeResponse = Date.now();

    // var connection = {user_id: userID,
    //                   other_user_id: otherUserID };
    //
    // models.instance.Connection.update(connection, {accepted: isAccepted, timestamp_accepted: timeResponse}, function(err){
    //     if(err) console.log(err);
    // });
    //
    // connection = {  user_id: userID,
    //                 other_user_id: otherUserID };
    //
    // models.instance.Connection.update(connection, {accepted: isAccepted, timestamp_accepted: timeResponse}, function(err){
    //     if(err) console.log(err);
    // });

    if(isAccepted){
        // userFrom, userTo, timestamp - The edge goes TO the one who accepted (current user)
        graphJournalist.engageAccepted(otherUserID, userID, timeResponse);
    }else{
        graphJournalist.engageRejected(otherUserID, userID, timeResponse);
    }
}
