"use strict";
const Router = require('koa-router');
const models = require('express-cassandra');
const activityConstants = require('../constants/activityConstants.js');
const countryConstants = require('../constants/countries');
const discussionsController = require('./graph/discussions');
const journalistsController = require('./graph/journalists');
const auth = require('./sessionControl');
const matchesExpert = require('../experts/matchesExpert');

const MAX_MATCHES_RESULT = 12;

const router = new Router({
    prefix: '/matches'
});

router.get('/', auth.isJournalist, function *(next) {
    this.body = 'estoy adentro de recommendations!';
})

/*
 * Get journalists profile matches by Country
 */
.get('/get.matches/:country', auth.isJournalist, function *(next) {

    const userID = auth.getSession(this.cookies.get("KOASESSID")).id;
    const country = this.params.country;
    let filterFields = {};

    if(country !== 'world'){
        filterFields = {user_id: userID, target_country_short:country, $limit: MAX_MATCHES_RESULT };
    }else{
        filterFields = {user_id: userID, $limit: MAX_MATCHES_RESULT+5 };
    }

    var matchesResult = yield function(findCallback) {
        models.instance.Match.find(filterFields, {raw:true}, findCallback);
    }

    let user_id;
    let responseArray = [];
    for(let i=0; i < matchesResult.length; i++){

        user_id = matchesResult[i].target_user_id;
        if (yield journalistsController.isActive(user_id)) {
            responseArray.push(matchesResult[i]);
        }
        if (responseArray.length >= MAX_MATCHES_RESULT) {
            break;
        }
    }

    this.body = responseArray;
})

.get('/get.discussions/:country_short', auth.isJournalist, function *(next) {

    const userID = auth.getSession(this.cookies.get("KOASESSID")).id;
    const qtyResults = 15;

    //1. Buscar 5 por tag matches  / Luego se suma por hot ranking
    //2. Buscar por las ultimas 5 por pais, publicadas en los últimos 20 días
    //3. Buscar las últimas 5 publicadas en los últimos 20 días

    // Buscar las ultimas 100 recomendaciones de los últimos 30 días
    var now = new Date();
    var lastMonthDate = now.setDate(now.getDate() - 30);
    var lastMatches = yield function(findCallback) {
        models.instance.Match_Counter.find({user_id: userID, type:'DISCUSSION', object_timestamp:{'$gt': lastMonthDate}, $limit: 100}, {raw:true}, findCallback);
    }

    // console.log('');
    // console.log('Ultimas 100 recomendaciones de los últimos 30 días: ');
    // console.log(lastMatches);
    // console.log('');

    var response;
    var isNotificationTable = false;
    if(lastMatches.length > 0){
    //Existen Matches

        // Reorder the recomendations
        response = matchesExpert.rank(lastMatches);

        // console.log('');
        // console.log('Response desde rank()');
        // console.log(response);
        // console.log('');


    }else{
    // No existen Matches, buscar notifications

        // Buscar discussions a partir de las notificaciones creadas para el usuario, filtrada por tipo ordenada por fecha
        response = yield function(findCallback) {
            let type = activityConstants.DISCUSSION_POST;
            models.instance.Activity_Notification.find({user_id: userID, type:type, $limit: qtyResults}, {raw:true, materialized_view: "activity_notification_type_date"}, findCallback);
        }
        isNotificationTable = true;
    }

    // Buscar por las ultimas 10 por pais
    if(response.length < qtyResults){
        //no existen notifications, quizas un usuario nuevo, registro posterior a la creacion de discussions
        response = yield discussionsController.getDiscussionsByCountry(userID, this.params.country_short, qtyResults);

        // console.log('');
        // console.log('Response desde getDiscussionsByCountry(uno solo)');
        // console.log(response);
        // console.log('');

        if(response.length > 0){
            this.body = response;
            return;
        }
    }

    // Buscar por las ultimas 10
    if(response.length < qtyResults){
        const countryShortNames = countryConstants.getShortNames();
        response = yield discussionsController.getDiscussionsByCountry(userID, countryShortNames, qtyResults);

        // console.log('');
        // console.log('Response desde getDiscussionsByCountry(ALL)');
        // console.log(response);
        // console.log('');

        if(response.length > 0){
            this.body = response;
            return;
        }
    }

    let vertexID;
    let arrayObjects = new Array();
    let discussionVertex;

    // Recorrer resultados y obtener las discussions una por una por vertexId.
    for(let i=0; i<response.length; i++){

        if(!isNotificationTable){
            vertexID = JSON.parse(response[i].object_id);
        }else{
            vertexID = JSON.parse(response[i].target_object_id);
        }

        discussionVertex = yield discussionsController.getDiscussion(vertexID);
        arrayObjects.push(discussionVertex);

        // Get 10 results
        if(i+1 == qtyResults) break;
    }

    this.body = arrayObjects;
})

//
// TODO: pendiente implementar... tiene return;
//
.get('/get.discussions.follow', auth.isJournalist, function *(next) {

    this.body = new Array();
    return;

    // TODO: Buscar las recomendaciones de los usuarios que estoy siguiendo

    const userID = auth.getSession(this.cookies.get("KOASESSID")).id;
    const type = activityConstants.DISCUSSION_POST;
    const qtyResults = 15;


    // Buscar las ultimas 100 recomendaciones de los últimos 30 días
    var now = new Date();
    var lastMonthDate = now.setDate(now.getDate() - 30);
    var lastMatches = yield function(findCallback) {
        models.instance.Match_Counter.find({user_id: userID, type:'DISCUSSION', object_timestamp:{'$gt': lastMonthDate}, $limit: 100}, {raw:true}, findCallback);
    }

    var response;
    if(lastMatches.length > 0){

        // Reorder the recomendations
        response = matchesExpert.rank(lastMatches);

    }

    let vertexID;
    let arrayObjects = new Array();
    let discussionVertex;

    for(let i=0; i<response.length; i++){

        vertexID = response[i].object_id;
        vertexID = JSON.parse(vertexID);
        discussionVertex = yield discussionsController.getDiscussion(vertexID);
        arrayObjects.push(discussionVertex);

        // Get 5 results
        if(i+1 == qtyResults) break;
    }

    this.body = arrayObjects;
});

function upsertCallback(err, result){
    if(err) throw err;
    return result;
}


module.exports = router;

module.exports.saveMatch = function(updateFields, primaryFields){
    models.instance.Match.update( primaryFields, updateFields, upsertCallback);
}
