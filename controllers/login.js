"use strict";
const Router = require('koa-router');
const passport = require('koa-passport');
// const jwt = require('koa-jwt');
const request = require('koa-request');
const http = require('http');
const mysqlConnections = require('./mysqlConnections');
const bcrypt = require('bcrypt');
const journalistsGraph = require('./graph/journalists');
const statusCode = require('../constants/httpStatusConstants');
const sessionControl = require('./sessionControl');
const activitiesController = require('./activities');
const activityConstants = require('../constants/activityConstants');
const expertMandrill = require('../experts/mandrill');
const jsonBody = require('koa-json-body')();
http.post = require('http-post');

var auth = require('./auth');

var router = new Router({
    prefix: '/login'
});

// const PHP_SERVER = 'http://www.kulectiv.com:88';
const FRONT_END = 'https://www.kulectiv.com';
// const FRONT_END = 'http://localhost:8000';


/*
 * Save new user
 */
router.post('/', jsonBody, function *(next) {

    var email = this.request.body.email.trim();
    var plainPassword = this.request.body.password.trim();
    var noexpire = this.request.body.noexpire;

    try {

        // Get Mysql User
        var userResult = yield _getUserByEmail(email);
        if(!userResult){
            this.status = statusCode.NOT_FOUND;
            return;
        }

        // Validate password
        var isValidPassword = bcrypt.compareSync(plainPassword, userResult.user_pass);
        if(!isValidPassword){

            this.status = statusCode.UNAUTHORIZED;
            return;
        }

        //User exists and the password is correct, but account has been deactivated
        if(!userResult.user_accepted){

            _loginDeactivated(userResult.user_id, userResult.user_name);

            this.status = statusCode.FORBIDDEN;
            return;
        }

        // Agregar usuario en Graph, si ya existe, lo recupera.
        var graphResult = yield journalistsGraph.add(userResult);

        // KOA session
        var payload = sessionControl.buildPayload(graphResult, noexpire);
        var sessionKOA = sessionControl.getKOASESSID(payload);
        this.cookies.set('KOASESSID', sessionKOA, { httpOnly: false });

        this.body = yield buildUserData(graphResult);

    } catch (e) {
        this.status = statusCode.UNAUTHORIZED;
        this.body = e.message;
    }
})

/*
 * Change password, after email verification.
 */
.post('/change.password', sessionControl.isJournalist, jsonBody, function *(next) {

    // Obtener user_id de session
    const userID = sessionControl.getSession(this.cookies.get("KOASESSID")).id;

    // Obtener parametros POST
    const currentPassword = this.request.body.password.trim();
    const newPassword = this.request.body.new_password.trim();
    const userResult = yield _getUserById(userID);

    const nodeHashedPassword = userResult.user_pass;

    // Validate password
    const isValidCurrentPassword = bcrypt.compareSync(currentPassword, nodeHashedPassword);
    if(!isValidCurrentPassword){
        this.status = statusCode.UNAUTHORIZED;
        return;
    }

    // La contraseña actual es válida, cambiar contraseña.
    const isValidNewPassword = yield _validatePassword(newPassword);
    if(!isValidNewPassword){
        this.status = statusCode.UNAUTHORIZED;
        return;
    }

    // Guardar password en la base de datos.
    const newPhpHashedPassword = _hashPassword(newPassword);

    //GUARDAR,hacer update.
    yield _updateUserPassword(userID, newPhpHashedPassword);

    this.body = 'Done';
})

//Login Twitter
.get('/twitter', passport.authenticate('twitter', { scope: ['include_email=true']}))
.get('/twitter/callback',
    passport.authenticate('twitter', { session: true, failureRedirect: 'http://kulectiv.com'}), // Cancelled by user, from twitter page
    function *(){

        //Twitter login failed, logout.
        if(!this.isAuthenticated()){

            this.cookies.set('KOASESSID', '0', { httpOnly: false });
            this.status = statusCode.UNAUTHORIZED;
            this.redirect(FRONT_END + "/#!/login");
            return;
        }

        // Once we got the user data, delete session to keep server side stateless :)
        var userTwitter = this.req.user;

        try{

            // Get/Save MySQL User
            var userMySQL = yield _getMySqlUserByTwitter(userTwitter);
            if(!userMySQL){

                // Usuario en Mysql no existe, es el primer login de twitter --> crear en Mysql.
                userMySQL = yield _saveNewUserFromTwitter(userTwitter);
                if(userMySQL){
                    expertMandrill.sendTwitterWelcomeEmail(userTwitter);
                }else{
                    console.log('ERROR  - al intentar guardar usuario mysql - twitter login');
                    this.status = statusCode.SERVER_ERROR;
                    this.redirect(FRONT_END + "/#!/login");
                    return;
                }


            }else if(!userMySQL.user_accepted){

                //User exists and the password is correct, but account has been deactivated
                this.status = statusCode.FORBIDDEN;
                this.redirect(FRONT_END + "/#!/login");
                return;

            }else{

                // usario existe y aceptado, actualizando twitter password
                yield _updateTwitterPassword(userMySQL.user_id, userTwitter.tokenSecret);
            }

            // Agregar usuario en Graph, si ya existe, lo recupera.
            var graphResult = yield journalistsGraph.add(userMySQL);

            if( userTwitter.bio !== '' && (!graphResult.bio || graphResult.bio !== '') ){
                // Actualizar la bio del vertex journalist
                yield journalistsGraph.updateBio(userMySQL.user_id, userTwitter.bio);
            }

            // Set KOA SESSION ID
            var payload = sessionControl.buildPayload(graphResult);

            var sessionKOA = sessionControl.getKOASESSID(payload);
            this.cookies.set('KOASESSID', sessionKOA, { httpOnly: false });


            this.redirect(FRONT_END + "/#!/login/twitter");

        }catch(e){
            console.log('ERROR LOGIN DE TWITTER - ', e.message);
            this.status = statusCode.SERVER_ERROR;
            this.redirect(FRONT_END + "/#!/login");
        }
})

/*
 * Request a password recovery. Build payload and send email.
 */
.post('/request.password.recovery', jsonBody, function *(next) {

    const userEmail = this.request.body.email.trim();
    const validationObject = _buildPayloadRecoveryPass(userEmail);

    const hashedCode = sessionControl.getKOASESSID(validationObject);
    const userMysql = yield _getUserByEmail(this.request.body.email.trim());

    if(!userMysql.user_accepted){
        this.status = statusCode.FORBIDDEN;
        return;
    }

    yield _updateUserRecoveryHash(userMysql.user_id, hashedCode);

    const userData ={
        id: userMysql.user_id,
        name: userMysql.user_name,
        email: userMysql.user_email
    };

    //Enviar email
    expertMandrill.emailPasswordRecovery(userData, hashedCode);


    this.body = 'Done';
})

/*
 * Valida si un codigo pertenece al usuario que envía el mail.
 */
.post('/validate.password.recovery', jsonBody, function *(next) {

    // Verify hash code and validate expiration
    const jwtObject = sessionControl.getJWTObject(this.request.body.code);

    if(!jwtObject){
        this.status = statusCode.NOT_ALLOWED;
        return;
    }
    // Validar que el email coincide
    if (jwtObject.email !== this.request.body.email) {
        this.status = statusCode.NOT_ALLOWED;
        return;
    }

    this.body = 'OK';
})


/*
 * Valida nuevamente y cambia el password
 */
.post('/change.password.recovery', jsonBody, function *(next) {

    // Verify hash code and validate expiration
    const jwtObject = sessionControl.getJWTObject(this.request.body.code);
    if(!jwtObject){
        this.status = statusCode.NOT_ALLOWED;
        return;
    }
    // Validar que el email coincide
    if (jwtObject.email !== this.request.body.email) {
        this.status = statusCode.NOT_ALLOWED;
        return;
    }

    const userMySql = yield _getUserByEmail(jwtObject.email);
    if(!userMySql){
        this.status = statusCode.SERVER_ERROR;
        return;
    }

    const isValidNewPassword = yield _validatePassword(this.request.body.password);
    if(!isValidNewPassword){
        this.status = statusCode.UNAUTHORIZED;
        return;
    }

    // Guardar password en la base de datos.
    const newPhpHashedPassword = _hashPassword(this.request.body.password);
    yield _updateUserPassword(userMySql.user_id, newPhpHashedPassword);

    this.body = 'OK';
})

/*
 * User data for the front end session
 */
.get('/get.session.data', sessionControl.isReader, function *(next) {

    const userID = sessionControl.getSession(this.cookies.get("KOASESSID")).id;
    this.body = yield buildUserData(userID);
})

.post('/logout', function *(next) {
    this.cookies.set('KOASESSID', '', { httpOnly: false });
    this.body = "Done";
});


function mysqlCallback(err, resut){
    if(err) throw err;
    return resut;
}

/*
 * Front end session data
 */
function * buildUserData(params){

    if(parseInt(params) == params){
        var currentUserID = params;
        params = yield journalistsGraph.getUser(currentUserID);
    }

    const userData = {  userID: params.user_id,
                        picture: params.picture ? params.picture[0] : '',
                        name: params.name[0],
                        location: params.location ? params.location[0] : '',
                        country_short: params.country_short ? params.country_short[0] : '',
                        adm_1_short: params.adm_1_short ? params.adm_1_short[0] : '',
                        position: params.position ? params.position[0] : '',
                        hasOffers: params.offers && params.offers.length > 0 ? true : false,
                        hasRequests: params.requests && params.requests.length > 0 ? true : false,
                        profileMessage: params.dashboard_profile_read[0],
                        communityMessage: params.dashboard_community_read[0],
                        enterToSend: params.enter_to_send[0],
                        dateEmailVerified: params.timestamp_email_verified[0] };

    return userData;
}

/*
 * User data and expiration for password recovery.
 */
function _buildPayloadRecoveryPass(email){

    var iat = new Date();
    var exp = new Date();

    // Set expiration time in 24 hours
    exp.setHours(exp.getHours() + 24);

    iat = Math.floor(Number(iat)/1000);
    exp = Math.floor(Number(exp)/1000);

    return {
        email: email,
        iat: iat,
        exp: exp,
    };
}

function * _getUserByEmail(email){

    var select = "SELECT * FROM users WHERE user_email = ? LIMIT 1";
    var values = [email];
    var result = yield mysqlConnections.queryParamsDatabase(select, values);

    if( result.length > 0){
        return result[0];
    }else{
        return false;
    }
}

function * _getUserById(user_id){

    var select = "SELECT * FROM users WHERE user_id = ? LIMIT 1";
    var values = [user_id];
    var result = yield mysqlConnections.queryParamsDatabase(select, values);

    if( result.length > 0){
        return result[0];
    }else{
        return false;
    }
}

function * _getMySqlUserByTwitter(userData){

    var select = "SELECT * FROM users WHERE user_twitter = ? OR user_email = ? LIMIT 1";

    var values = [userData.twitter_id, userData.email];
    const result = yield mysqlConnections.queryParamsDatabase(select, values);

    if( result.length > 0){
        return result[0];
    }else{
        return false;
    }
}

/*
 * Set the hashed code in database, to be validated later during the recovery process.
 */
function * _updateUserRecoveryHash(userID, recoveryHash){

    const params = [recoveryHash, userID];
    const sql = 'UPDATE users SET user_reco_hash = ? WHERE user_id = ?';
    yield mysqlConnections.queryDatabase(sql, params);
}

function * _updateUserPassword(userID, hashedPassword){

    const params = [hashedPassword, userID];
    const sql = 'UPDATE users SET user_pass = ? WHERE user_id = ?';
    yield mysqlConnections.queryDatabase(sql, params);
}

function * _updateTwitterPassword(userID, tokenSecret){

    const phpHashedPassword = _hashPassword(tokenSecret);
    const params = [phpHashedPassword, userID];
    const sql = 'UPDATE users SET user_php_twitter_pass = ? WHERE user_id =  ?';

    yield mysqlConnections.queryDatabase(sql, params);
}

function _hashPassword(password){
    return bcrypt.hashSync(password, 10);
}

/*
 *  Guarda nuevo usuario con los datos la API de autenticacion de twitter
 */
function * _saveNewUserFromTwitter(userData){

    const phpHashedPassword = _hashPassword(userData.tokenSecret);

    var now = Math.floor(Number(Date.now())/1000);
    var fields =    " user_name, "+
                    " user_email, "+
                    " user_twitter, "+
                    " role_id, "+
                    " user_date_create, "+
                    " user_fullname, "+
                    " user_email_verified, "+
                    " user_date_email_verified, "+
                    " user_accepted, "+
                    " user_date_accepted, "+
                    " user_twitter_verified, "+
                    " user_php_twitter_pass ";

    var sql = "INSERT INTO users (" + fields + ") VALUES ( ";

    sql += "'" + userData.name + "',";
    sql += "'" + userData.email + "',";
    sql += userData.twitter_id + ",";
    sql += 20 + ",";                      //role_id
    sql += now + ",";                     //user_date_create
    sql += "'" + userData.name + "',";    //user_fullname
    sql += 1 + ",";                       //user_email_verified
    sql += now + ",";                     //user_date_email_verified
    sql += 1 + ",";                       //user_accepted
    sql += now + ",";                     //user_date_accepted
    sql += 1 + ",";                       //user_twitter_verified
    sql += "'" + phpHashedPassword + "' )";

    var result = yield mysqlConnections.queryDatabase(sql);
    console.log('*************** NUEVO RESULTADO LUEGO DE CAMBIAR EL DRIVER MYSQL **************');
    console.log('no quiero pagar huevo kinder....');
    console.log('Lo dejo aca porque no voy a hacer un signup con twitter ahora.');
    console.log('Determinar segun el resultado, como obtener el estado de exito o error con este nuevo driver.');
    console.log('*******************************************************************************');
    console.log(result);

    // OkPacket {
    //     fieldCount: 0,
    //     affectedRows: 1,
    //     insertId: 591,
    //     serverStatus: 2,
    //     warningCount: 1,
    //     message: '',
    //     protocol41: true,
    //     changedRows: 0
    // }

    if(!result || result.affectedRows < 1){
        return false;
    }


    var mysqlRow = yield _getMySqlUserByTwitter(userData);
    if(parseInt(mysqlRow.user_id) != mysqlRow.user_id)
        throw new Error('Error while getting new user data from Mysql');

    return mysqlRow;
}


/*
 * Get the PHPSESSID calling a php service to authenticate.
 * Lo llamamos unicamente al momento de crear el usuario.
 * Si es llamado desde Twitter, usamos el secretToken como password.
 * Si es llamado desde el Signup normal, usamos el password del usuario.
 * Cuando se hace login desde Twitter, usamos el token con el servicio authenticate.external de PHP
 * Cuando hace login normal usando cuenta de email y pass validamos con el servicio authenticate de PHP
 */
// function * _phpAuthenticate(serviceName, id, password){
//
//     var options = {
//         url: PHP_SERVER + '/default/login/method/authenticate' +
//         '/id/' + id + '/password/' + password + '/external_service/' + serviceName,
//         headers: { 'User-Agent': 'request' }
//     };
//
//     var response = yield request(options);
//
//     var data = JSON.parse(response.body);
//     if(data.status != 200){
//         console.log(data.message);
//         throw new Error('Error while trying to open PHP Session - ' + data.message);
//     }
//
//     // Return PHPSESSID
//     return response.headers['set-cookie'][0].split(/=|;/)[1];
// }

/*
 * Valida 8 caracteres minimo, 1 Mayuscula, 1 número
 */
function * _validatePassword(password){
    return /(?=^.{8,}$)(?=.*\d)(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/.test(password)
}

/*
 * Logs the login attempt with a deactivated account
 */
function _loginDeactivated(userId, userName){

    var params = {
        type:               activityConstants.USER_LOGIN_DEACTIVATED,
        user_id:            userId,
        user_name:          userName,
        target_object_id:   userId,
        objects_id:         [''],
        details:            [''],
        users_id:           [''],
        users_name:         [''],
        users_pic:          ['']
    };

    activitiesController.logActivity(params);
}

module.exports = router;
