"use strict";
const Router = require('koa-router');
const auth = require('./sessionControl');
const fs = require('fs');
const jsonBody = require('koa-json-body')();

const base64ToImage = require('base64-to-image');
const gm = require('gm');

const router = new Router({
    prefix: '/images'
});

router.get('/', function *(next) {
    this.body = 'estoy adentro de images!';
})

.get('/get.marketplace.picture/:picture', function *(next) {

    this.params.folder = 'marketplace';
    var file = yield _readPictureFile(this.params);
    this.body = file;
})

.get('/get.email.profile.picture/:folder/:picture', function *(next) {

    this.params.folder = 'mini';
    var file = yield _readPictureFile(this.params);
    this.body = file;
})

.get('/get.picture/:folder/:picture', auth.isJournalist, function *(next) {

    var file = yield _readPictureFile(this.params);
    this.body = file;
})

.get('/get.picture/:folder/', auth.isJournalist, function *(next) {

    var file = yield _readPictureFile(this.params);
    this.body = file;
})

.post('/save.picture', auth.isJournalist, jsonBody, function *(next) {

    const fileName = this.request.body.imafileNamege;
    const base64Str = this.request.body.image;

    let resultFileName =  _savepicture(base64Str, fileName);

    this.body = {fileName:resultFileName};
});


var _savepicture = function(base64Str, fileName, fileType, arrayFolders){

    const type = fileType || 'jpg';
    const path = _buildPath(arrayFolders);

    const optionalObj = {'fileName': fileName, 'type':type};

    // Note base64ToImage function returns imageInfo which is an object with imageType and fileName.
    let imageInfo = base64ToImage(base64Str, path, optionalObj);

    setTimeout(function () {

        gm(path + optionalObj.fileName)
        .resize(200, 200)
        .noProfile()
        .write(path +'thumbnail/'+ optionalObj.fileName, function (err) {
            if (err) {
                console.log(err);
            }else{
                console.log('thumbnail done');
            }
        });

        gm(path + optionalObj.fileName)
        .resize(100, 100)
        .noProfile()
        .write(path +'mini/'+ optionalObj.fileName, function (err) {
            if (err){
                console.log(err);
            }else{
                console.log('mini done');
            }
        });


    }, 1000);

    return imageInfo;
};

function _buildPath(arrayFolders) {

    // let path = __dirname.replace('koa/controllers', '');
    let path = __dirname.replace('/koa/controllers', '/wejo/php_apps/wejo/pics/');

    if (arrayFolders && arrayFolders.length > 0) {
        for (let i = 0; i < arrayFolders.length; i++) {
            if (arrayFolders[i] === 'main') {
                continue;
            }
            path += arrayFolders[i] +'/';
        }
    }

    return path;
}

function _deletePicture(path, fileName) {

    if (fs.existsSync(path + fileName)) {
        fs.unlinkSync(path + fileName);
    }

    if (fs.existsSync(path + 'mini/' + fileName)) {
        fs.unlinkSync(path + 'mini/' + fileName);
    }

    if (fs.existsSync(path + 'medium/' + fileName)) {
        fs.unlinkSync(path + 'medium/' + fileName);
    }

    if (fs.existsSync(path + 'thumbnail/' + fileName)) {
        fs.unlinkSync(path + 'thumbnail/' + fileName);
    }

}

function * _readPictureFile(params){

    var folders = params.folder.split('.');
    var path = _buildPath(folders);
    var pictureName = params.picture;

    if(!pictureName || pictureName == 'undefined' || pictureName == 'default'){
        pictureName = 'default.png';
    }

    try {
        var file = fs.readFileSync(path + pictureName);

    } catch (e) {

        if(e.code == 'ENOENT'){
            // manejo de error cuando no existe el archivo
            pictureName = 'default.png';
            file = fs.readFileSync(path + pictureName);
        }
    }

    return file;
};

module.exports = router;

module.exports.saveItemPicture = function(path, fileName) {
    // guardar la imagen en la carpeta marketplace,
    //devolver el nombre del archivo para guardar en la base
    let folder = ['marketplace'];
    let newPath = _buildPath(folder);
    const data = fs.readFileSync(path);

    try{
        fs.writeFileSync(newPath + fileName, data);

        setTimeout(function () {

            gm(newPath + fileName)
            .resize(200, 200)
            .noProfile()
            .write(newPath +'thumbnail/'+ fileName, function (err) {
              if (err) {
                  console.log(err);
              }else{
                  console.log('thumbnail done');
              }

            });

            gm(newPath + fileName)
            .resize(100, 100)
            .noProfile()
            .write(newPath +'mini/'+ fileName, function (err) {
                if (err) {
                    console.log(err);
                }else{
                    console.log('mini done');
                }

            });

        }, 1000);


    }catch (err){

        _deletePicture(path, fileName);

        console.log('EXCEPTION Writing image: ');
        console.log(err.message);
        return false
    }
    return true;

    // _readTempFile(path, fileName, newPath );
};

module.exports.saveProfilePicture = function(base64Str, fileName) {
    return _savepicture(base64Str, fileName);
};

module.exports.deletePicture = function(fileName, folder) {

    let folders = folder.split('.');
    let path = _buildPath(folders);

    try {
        _deletePicture(path, fileName);
    }catch (err){
        console.log('EXCEPTION Writing image: ');
        console.log(err.message);
        return false
    }
    return true;
};
