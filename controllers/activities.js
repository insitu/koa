const Router = require('koa-router');
const jsonBody = require('koa-json-body')();
const models = require('express-cassandra');
const constants = require('../constants/activityConstants.js');
const notificationsController = require('../controllers/notifications.js');
const mysqlConnections = require('./mysqlConnections');
const auth = require('./sessionControl');

var router = new Router({
    prefix: '/activities'
});

var gameTasks = null;

router
    .get('/', auth.isJournalist, function *(next) {
    this.body = 'estoy adentro de activity!';
})

.get('/activities/:user_id', auth.isAdmin, function *(next) {

    const userID = parseInt(this.params.user_id);
    var result = yield function(findCallback) {
        models.instance.Activity.find({user_id:userID}, { select:['user_id','toTimestamp(timeuuid) as timestamp', 'type', 'details'], raw:true }, findCallback);
    }
    this.body = result;
})

.get('/daily.active.users', auth.isAdmin, function *(next) {
    this.body = yield _getActivityCounterByType('USER_LOGIN');
})

.get('/daily.messages', auth.isAdmin, function *(next) {
    this.body = yield _getActivityCounterByType('MESSAGE_SENT');
})

.get('/daily.discussions', auth.isAdmin, function *(next) {
    this.body = yield _getActivityCounterByType('DISCUSSION_POST');
})

.get('/daily.signups', auth.isAdmin, function *(next) {

    var last24hs = new Date();
    last24hs.setHours(last24hs.getHours() - 24);
    last24hs = Number(last24hs)/1000;

    var select = "SELECT count(*) as total "+
                 "FROM users WHERE user_date_create >= ? ";



    var values = [last24hs];
    var result = yield mysqlConnections.queryDatabase(select, values);

    if(result[0] && result[0].total){
        this.body =  result[0].total;

    }else{
        this.body = 0;
    }
})

.get('/game_status/:user_id', auth.isJournalist, function *(next) {

    const userID = auth.getSession(this.cookies.get("KOASESSID")).id;
    // var userID = parseInt(this.params.user_id);

    var userActivities = yield function(findCallback) {
        models.instance.Activity_Counter.find({user_id: userID}, {raw:true}, findCallback);
    }

    //Activity counters of the user
    var userActivityCounters = new Array();
    var activityItem;
    var result = {};

    userActivities.forEach(function(item){

        activityItem = new Object();
        activityItem.type = item.type;
        activityItem.counter = item.times_counter;
        userActivityCounters.push(activityItem);
    });
    result.activities = userActivityCounters;


    var i,j;
    var itemTask = null;
    var itemCounterActivity = null;
    var currentGame = null;
    var currentLevel = null;
    var itemSummary = null;

    result.summaries = new Array();
    result.items = new Array();
    result.totalPoints = 0;

    for(i=0; i<gameTasks.length; i++){

        itemTask = gameTasks[i];
        itemTask.completed = false;

        //Init Game
        if(currentGame != itemTask.game){

            currentGame = itemTask.game;
        }

        //Init level and summary item
        if(currentLevel !== itemTask.level){

            currentLevel = itemTask.level;

            itemSummary = new Object();
            itemSummary.game = currentGame;
            itemSummary.level = currentLevel;
            itemSummary.totalTasks = 0;
            itemSummary.completedTasks = 0;
            itemSummary.levelPoints = 0;
            itemSummary.done = false;
        }

        //Calculate Summary values
        for(j=0; j<userActivities.length; j++){

            if(userActivities[j].type == itemTask.type){

                if(userActivities[j].times_counter >= itemTask.quantity_goal){

                    userActivities[j].times_counter -= itemTask.quantity_goal;
                    itemTask.completed = true;

                    itemSummary.completedTasks += 1;
                    result.totalPoints += parseFloat(itemTask.points * itemTask.quantity_goal);
                }
            }
        }

        itemSummary.levelPoints += parseFloat(itemTask.points * itemTask.quantity_goal);
        itemSummary.totalTasks += 1;
        result.items.push(itemTask);

        //if next item belongs to the next level or it's the last level or it's the next game's level- ADD summary item
        if( (i+1) >= gameTasks.length || gameTasks[i+1].level != currentLevel){
            if(itemSummary.totalTasks == itemSummary.completedTasks){

                //TODO: verificar que el nivel anterior esta completo para marcar ete nivel como completo
                //TODO: en el bucle anterior obtener el valor del counter de la actividad LEVEL_NOTIFIED_PROFILE o LEVEL_NOTIFIED_comunity
                // segun el juego, y comparar el contador con el numero de nivel que se acaba de completar. en caso de ser menor,
                // lo incrementamos en la base de datos, pero lo mandamos al FE sin incrementar.

                itemSummary.done = true;
            }
            result.summaries.push(itemSummary);
        }
    }


    this.body = result;
})

.post('/log.activity', auth.isJournalist, jsonBody, function *(next) {

    const userID = auth.getSession(this.cookies.get("KOASESSID")).id;
    const params = this.request.body;

    _updateCounter(userID, this.request.body.type, 1);
    this.body = 'Done';
});

function * _getActivityCounterByType(activityType){

    var result = yield function(findCallback) {

        var last24hs = new Date();
        last24hs.setHours(last24hs.getHours() - 24);

        const whereQuery = { timeuuid: {'$gt': models.timeuuidFromDate(last24hs) }, type: activityType };

        const options = { materialized_view: 'activity_login',
                          select:['count(user_id) as total'],
                          raw: true};

        models.instance.Activity.find(whereQuery, options, findCallback);
    }

    return result[0].total;
}

function _updateMessageCounters(activity){

    var user_id = parseInt(activity.user_id);
    _updateCounter(user_id, constants.MESSAGE_SENT, 1);

    for(var i=0; i<activity.users_id.length; i++){

        // avoid myself
        if(user_id == activity.users_id[i]) continue;

        user_id = parseInt(activity.users_id[i]);
        _updateCounter(user_id, constants.MESSAGE_RECEIVED, 1);
    }
}


function _updateCounter(userID, activityType, times){

    times = models.datatypes.Long.fromInt(times);
    models.instance.Activity_Counter.update({user_id:userID, type: activityType}, {times_counter: times}, function(err, result){
        if(err) console.log('Error Activity_Counter '+ activityType +  ' user '+ userID +' - error message: ' + err);
    });
}


function findCallback(err, result){
    if(err) throw err;
    return result;
}


module.exports = router;

module.exports.initGammification = function(){

    var tasksCallback = function(err, result){
        if(err) throw err;

        gameTasks = result;
    }

    models.instance.Activity_Task.find({id: 1}, {raw:true}, tasksCallback);
}


module.exports.logActivity = function(params, socketCallback, headers){

    var user_id = parseInt(params.user_id);
    var target_user_id = parseInt(params.target_object_id);
    var notifyUsers = true; // create cassandra notification and socket emit notification to the users
    var revertNotification = false; // revert cassandra counters
    var notificationType = params.type;

    switch(params.type){

        case constants.PROJECT_ADD_MEMBER:
        case constants.LEVEL_NOTIFIED_PROFILE:
        case constants.LEVEL_NOTIFIED_COMMUNITY:
        case constants.DISCUSSION_POST:
        case constants.DISCUSSION_POST_UP:
        case constants.DISCUSSION_POST_DOWN:
        case constants.DISCUSSION_REPLY_UP:
        case constants.DISCUSSION_REPLY_DOWN:
        case constants.DISCUSSION_PIN:
        case constants.DISCUSSION_POST_REPLY:
        case constants.DISCUSSION_REPLY:
            _updateCounter(user_id, params.type, 1);
            break;

        case constants.DISCUSSION_UNPIN:
            notifyUsers = false;
            revertNotification = true;
            notificationType = constants.DISCUSSION_PIN;
            _updateCounter(user_id, notificationType, -1);
            break;

        case constants.DISCUSSION_READ:
            notifyUsers = false;
            _updateCounter(user_id, params.type, 1);
            break;

        case constants.MARKETPLACE_POST:
            _updateCounter(user_id, params.type, 1);
            break;

        case constants.USER_ENGAGE_REQUEST:
            if(target_user_id <= 0) return false;
            _updateCounter(user_id, constants.USER_ENGAGE_REQUEST_SENDER, 1);
            _updateCounter(target_user_id, constants.USER_ENGAGE_REQUEST_TARGET, 1);
            break;

        case constants.USER_ENGAGE_ACCEPT:
            if(target_user_id <= 0) return false;
            _updateCounter(user_id, constants.USER_ENGAGE_ACCEPT_TARGET, 1);
            _updateCounter(target_user_id, constants.USER_ENGAGE_ACCEPT_SENDER, 1);
            break;

        case constants.USER_FOLLOW:
            if(target_user_id <= 0) return false;
            _updateCounter(user_id, constants.USER_FOLLOW, 1);
            _updateCounter(target_user_id, constants.USER_FOLLOWED, 1);
            break;

        case constants.USER_UNFOLLOW:
            if(target_user_id <= 0) return false;
            notifyUsers = false;
            revertNotification = true;
            notificationType = constants.USER_FOLLOW;
            _updateCounter(user_id, constants.USER_FOLLOW, -1);
            _updateCounter(target_user_id, constants.USER_FOLLOWED, -1);
            break;

        case constants.USER_REFER:
            _updateCounter(user_id, params.type, 1);
            break;

        case constants.PROFILE_UPLOAD_COVER:
        case constants.PROFILE_UPLOAD_PIC:
        case constants.PROFILE_COMPLETE_QUOTE:
        case constants.PROFILE_COMPLETE_BIO:
        case constants.PROFILE_COMPLETE_LOCATION:
        case constants.PROFILE_COMPLETE_POSITION:
        case constants.PROFILE_UPDATE_TWITTER:
        case constants.PROFILE_UPDATE_FACEBOOK:
        case constants.PROFILE_ADD_PUBLICATION:
        case constants.PROFILE_ADD_LANGUAGE:
        case constants.PROFILE_ADD_EXPERTISE:
        case constants.PROFILE_ADD_OFFER:
        case constants.PROFILE_ADD_REQUEST:

            notifyUsers = false;

            _updateCounter(user_id, params.type, 1);
            break;

        case constants.PROFILE_DELETE_EXPERTISE:
            notifyUsers = false;
            notificationType = constants.PROFILE_ADD_EXPERTISE;
            _updateCounter(user_id, notificationType, -1);
        break;

        case constants.PROFILE_DELETE_LANGUAGE:
            notifyUsers = false;
            notificationType = constants.PROFILE_ADD_LANGUAGE;
            _updateCounter(user_id, notificationType, -1);
        break;


        case constants.PROFILE_DELETE_OFFER:
            notifyUsers = false;
            notificationType = constants.PROFILE_ADD_OFFER;
            _updateCounter(user_id, notificationType, -1);
        break;

        case constants.PROFILE_DELETE_REQUEST:
            notifyUsers = false;
            notificationType = constants.PROFILE_ADD_REQUEST;
            _updateCounter(user_id, notificationType, -1);
        break;

        case constants.MESSAGE_SENT:
            notifyUsers = false;
            _updateMessageCounters(params);
            break;

        case constants.USER_LOGIN:
        case constants.USER_LOGIN_DEACTIVATED:
            params.details = [JSON.stringify(headers)];
            _updateCounter(user_id, params.type, 1);
            break;

        // case constants.USER_SIGNUP:
        //     El activity de SIGNUP lo obtenemos de tabla usuarios en MySQL
        //     break;

        default:
            console.log('************* * **********');
            console.log('ACTIVITY type no registrado: ', params.type);
            console.log('************* * **********');
            return;
            // notifyUsers = false;
            // _updateCounter(user_id, params.type, 1);
    }

    var activityModel = { user_id: user_id,
        timeuuid:         models.timeuuid(),
        type:             params.type,
        target_object_id: params.target_object_id + '',
        user_id:          params.user_id,
        user_name:        params.user_name,
        user_pic:         params.user_pic,
        objects_id:       params.objects_id,
        details:          params.details,
        users_id:         params.users_id,
        users_name:       params.users_name,
        users_pic:        params.users_pic
    };


    // Save activity
    var activity = new models.instance.Activity(activityModel);
    activity.save(function(err){

        if(err){
            console.log('Error save de Activity ' + err);
            console.log(' ');
            console.log(activityModel);
            console.log(' ');
        }

        //Create and emit notifications
        activityModel.notifyUsers = notifyUsers;
        activityModel.revertNotification = revertNotification;
        activityModel.notificationType = notificationType;

        if (user_id != parseInt(params.users_id[0]) && socketCallback) {
            notificationsController.createNotification(activityModel, socketCallback);
        }

    });
}
