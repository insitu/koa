const Router = require('koa-router');
const models = require('express-cassandra');
const auth = require('./sessionControl');
const jsonBody = require('koa-json-body')();
const mysqlConnections = require('./mysqlConnections');
const mandril = require('../experts/mandrill');
const journalistsController = require('./graph/journalists');
const graphConnections = require('./graph/graphConnections');
const matchesExpert = require('../experts/matchesExpert');

var router = new Router({
  prefix: '/admin'
});

const client = graphConnections.getClient();

var graphCallback = function(err, result){
    assert.ifError(err);
    return result;
}
var csvCallback = function(err, result){
    assert.ifError(err);
    return result;
}

router.get('/', function *(next) {
        this.body = 'estoy adentro de admin services';
})

.get('/list.matches', auth.isAdmin, function *(next) {

  const MAX_MATCHES_RESULT = 10;
  let filterFields = {};
  let response = [];

  console.time('buscandoJournalists');
  // Buscar todos los usuarios con el perfil completo
  let queryGraph = 'g.V().hasLabel("journalist").match(';
  // let queryGraph = 'g.V().hasId("{~label=journalist, user_id='+678+'}").match(';
  queryGraph += '__.as("j").hasNot("timestamp_deactivated")';
  queryGraph += ',__.as("j").has("offers")';
  queryGraph += ',__.as("j").has("requests")';
  queryGraph += ',__.as("j").has("location")';
  queryGraph += ').select("j").dedup().valueMap("user_id", "timestamp_email_verified", "email", "name", "location", "offers", "requests")';

  let users = yield function(graphCallback) {
      client.executeGraph(queryGraph, graphCallback);
  }
  console.timeEnd('buscandoJournalists');

  if(users.length == 0)return;

  console.time('convirtiendoArray');
  let arrayUsers = users.toArray();
  console.timeEnd('convirtiendoArray');

  console.time('armandoResultadoFor');
  // Por cada user buscar sus matches
  // users.forEach( yield function *(journalist){
  for (let i = 0; i < arrayUsers.length; i++) {
      let journalist = arrayUsers[i];

      // Buscar TODOS matches del usuario
      console.time('buscarMatches');
      filterFields = { user_id:journalist.user_id[0] };
      let matchesResult = yield function(findCallback) {
          models.instance.Match.find(filterFields, {raw:true}, findCallback);
      }
      console.timeEnd('buscarMatches');

      // Calcular rank y ordenar en forma DESC
      console.time('rankearMatches');
      let ranked = matchesExpert.rankUsers(matchesResult);
      console.timeEnd('rankearMatches');

      console.log('');
      console.time('armarResultado');
      let loopsCount = MAX_MATCHES_RESULT > ranked.length ? ranked.length : MAX_MATCHES_RESULT;
      for (let i = 0; i < loopsCount; i++) {

        let user = {
          user_id:ranked[i].user_id,
          name: journalist.name[0],
          location: journalist.location[0],
          email:journalist.email[0],
          offers:journalist.offers[0],
          requests:journalist.requests[0],
          type: JSON.parse(ranked[i].tags_info[0]).type,
          rank:ranked[i].rank
        };

        // Buscar target user to graph para completar informacion
        // console.time('buscarInfoTarget');
        let targetUser = yield journalistsController.getUser(ranked[i].target_user_id);
        // console.timeEnd('buscarInfoTarget');
        user.target_name = targetUser.name[0];
        user.target_location = targetUser.location ? targetUser.location[0] : '';
        user.target_email = targetUser.email ? targetUser.email[0] : '';
        user.target_offers = targetUser.offers ? targetUser.offers[0] : '';
        user.target_requests = targetUser.requests ? targetUser.requests[0] : '';

        response.push(user);
      }
      console.timeEnd('armarResultado');

  }
  // );
  console.log('');
  console.timeEnd('armandoResultadoFor');

    let converter = require('json-2-csv');
    let options = {
        delimiter : {
            // wrap  : '"', // Double Quote (") character
            field : ';', // Comma field delimiter
            array : ',', // Semicolon array value delimiter
            // eol   : '\n' // Newline delimiter
        }
    };

    console.time('armandoCSV');
    this.body = yield function(csvCallback) {
        converter.json2csv(response, csvCallback, options);
    }
    console.timeEnd('armandoCSV');
})

.get('/list.incomplete.profile', auth.isAdmin, function *(next) {

    // No tienen ni offers ni requests
    let query = 'g.V().hasLabel("journalist").match(';
    query += '__.as("j").hasNot("timestamp_deactivated")';
    query += ',__.as("j").hasNot("offers")';
    query += ',__.as("j").hasNot("requests")';
    query += ').select("j").dedup().valueMap("user_id", "timestamp_email_verified", "email", "name")';
    // query += ').select("j").dedup().valueMap("user_id", "timestamp_email_verified", "email", "name", "location", "position", "offers", "requests")';

    // No tienen or picture or offers or requests or location
    let query2 = 'g.V().hasLabel("journalist").hasNot("picture").hasNot("timestamp_deactivated").union( '+
                 ' g.V().hasLabel("journalist").hasNot("location").hasNot("timestamp_deactivated").valueMap("user_id", "name", "email", "timestamp_email_verified"), '+
                 ' g.V().hasLabel("journalist").hasNot("offers").hasNot("timestamp_deactivated").valueMap("user_id", "name", "email", "timestamp_email_verified"),   '+
                 ' g.V().hasLabel("journalist").hasNot("requests").hasNot("timestamp_deactivated").valueMap("user_id", "name", "email", "timestamp_email_verified"), '+
                 ' valueMap("user_id", "name", "email", "timestamp_email_verified")).dedup() ';


    var result = yield function(graphCallback) {
        client.executeGraph(query2, graphCallback);
    }
    // result = result.toArray();

    let response = [];
    let obj;
    result.forEach(function(journalist){
        obj = {
            user_id:journalist.user_id[0],
            name:journalist.name[0],
            email:journalist.email[0],
            email_verified:journalist.timestamp_email_verified[0],
        };
        response.push(obj);
    });

    var converter = require('json-2-csv');

    var result = yield function(csvCallback) {
        converter.json2csv(response, csvCallback);
    }

    this.body = result;
})

.get('/list.complete.profile', auth.isAdmin, function *(next) {

    let query = 'g.V().hasLabel("journalist").match(';
    query += '__.as("j").hasNot("timestamp_deactivated")';
    query += ',__.as("j").has("offers")';
    query += ',__.as("j").has("requests")';
    query += ',__.as("j").has("location")';
    query += ').select("j").dedup().valueMap("user_id", "timestamp_email_verified", "email", "name", "location", "offers", "requests")';


    let result = yield function(graphCallback) {
        client.executeGraph(query, graphCallback);
    }
    // result = result.toArray();

    let response = [];
    let obj;
    result.forEach(function(journalist){
        obj = {
            user_id:journalist.user_id[0],
            name:journalist.name[0],
            email:journalist.email[0],
            location:journalist.location[0],
            email_verified:journalist.timestamp_email_verified[0],
            offers:journalist.offers[0],
            requests:journalist.requests[0],
        };
        response.push(obj);
    });

    let converter = require('json-2-csv');
    let options = {
        delimiter : {
            // wrap  : '"', // Double Quote (") character
            field : ';', // Comma field delimiter
            array : ',', // Semicolon array value delimiter
            // eol   : '\n' // Newline delimiter
        }
    };

    this.body = yield function(csvCallback) {
        converter.json2csv(response, csvCallback, options);
    }
})

.post('/activate.user', auth.isAdmin, jsonBody, function *(next) {

    const userID = this.request.body.user_id;
    const dateTime = Number(new Date()) / 1000;

    if(userID != parseInt(userID) || parseInt(userID) <= 0){
        this.status = 500;
        this.body = 'Params errors';
        return;
    }

    yield journalistsController.activateAccount(userID);

    const select = 'UPDATE users set user_accepted = 1, user_date_accepted = ? WHERE user_id = ?';
    const values = [dateTime, userID];
    yield mysqlConnections.queryParamsDatabase(select, values);

    this.body = 'Done';
})

.post('/deactivate.user', auth.isAdmin, jsonBody, function *(next) {

    const userID = this.request.body.user_id;

    if(!userID || userID != parseInt(userID) || parseInt(userID) <= 0){
        this.status = 500;
        this.body = 'Params errors';
        return;
    }

    yield journalistsController.deactivateAccount(userID);

    mandril.sendDeactivateEmail(this.request.body);

    const select = 'UPDATE users set user_accepted = 0, user_date_accepted = null WHERE user_id = ?';
    const values = [userID];
    yield mysqlConnections.queryParamsDatabase(select, values);

    this.body = 'Done';
});


module.exports = router;
