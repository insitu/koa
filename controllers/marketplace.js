"use strict";
const Router = require('koa-router');
const auth = require('./sessionControl');
const mysqlConnections = require('./mysqlConnections');
const statusCode = require('../constants/httpStatusConstants.js');
const activityConstants = require('../constants/activityConstants.js');
const body = require('koa-better-body');
const imagesController = require('./images');
const notificationsController = require('./notifications');
const jsonBody = require('koa-json-body')();


const router = new Router({
  prefix: '/marketplace'
});

router
    .get('/', auth.isJournalist , function *(next) {

        const userID = auth.getSession(this.cookies.get("KOASESSID")).id;
        const params = new Array();
        let query = "SELECT i.*, u.user_name, u.user_id, country_short, country_long, adm_level_1_short, adm_level_1_long, " +
        "( GROUP_CONCAT(DISTINCT ic.ca_id ORDER BY ic.ca_id ASC SEPARATOR ',') ) as ca_ids, " +
        "( GROUP_CONCAT(DISTINCT ca_name ORDER BY c.ca_id ASC SEPARATOR ',') ) as ca_names, " +
        "pic_name, pic_id " +
        "FROM items i " +
        "INNER JOIN users u ON u.user_id = i.user_id " +
        "INNER JOIN addresses a ON a.a_id = i.a_id " +
        "LEFT JOIN item_pictures p ON p.item_id = i.item_id " +
        "LEFT JOIN item_categories ic ON ic.item_id = i.item_id " +
        "LEFT JOIN categories c ON c.ca_id = ic.ca_id";

       let filters = "";

        if (this.request.query.search) {
            // filters += " i.item_title LIKE '%?%' "
            // params.push(this.request.query.search);
            filters += " i.item_title LIKE '%"+this.request.query.search+"%' "
        }

        if (this.request.query.cat) {

            if (filters) {
                filters += " AND "
            }
            filters += " ic.ca_id =  ? ";
            params.push(this.request.query.cat);
        }

        if (this.request.query.mine) {

            if (filters) {
                filters += " AND ";
            }

            filters += " i.user_id = ?";
            params.push(auth.getSession(this.cookies.get("KOASESSID")).id);
        }

        if (this.request.query.co_short) {

            if (filters) {
                filters += " AND ";
            }

            filters += " a.country_short = ? ";
            params.push(this.request.query.co_short);
        }

        if(filters){
            query += " WHERE" + filters;
        }

        query += " GROUP BY i.item_id ORDER BY item_date DESC";

        const result = yield mysqlConnections.queryDatabase(query, params);
        yield notificationsController.resetCounter(activityConstants.MARKETPLACE_POST, userID);

        this.body = result;
    })

    .post('/', auth.isJournalist, jsonBody, function *(next) {

        const userID = auth.getSession(this.cookies.get("KOASESSID")).id;

        const location = yield _IsJsonString(this.request.body.location);

        if (!location) {
            this.body = "No hay location";
            this.status = statusCode.SERVER_ERROR;
        }


        const saveLocationResponse = yield _saveLocation(location);

        if (saveLocationResponse.affectedRows != 1) {
            this.body = "Error al guardar location";
            this.status = statusCode.SERVER_ERROR;
            return;
        }


        const saveMarketplaceItemResponse = yield _upsertMarketplaceItem(this.request.body, userID, saveLocationResponse.insertId);

        if (saveMarketplaceItemResponse.affectedRows != 1){
            this.body = "Error al guardar item";
            this.status = statusCode.SERVER_ERROR;
            return;
        }
        const categories = JSON.parse(this.request.body.categories);

        const saveItemCategoryResponse = yield _saveItemCategory(saveMarketplaceItemResponse.insertId, categories[0].ca_id)

        if (saveItemCategoryResponse.affectedRows != 1) {
            this.body = "Error al guardar Category";
            this.status = statusCode.SERVER_ERROR;
            return;
        }

        this.body = {
            item_id: saveMarketplaceItemResponse.insertId
        };

        this.status = statusCode.CREATED;

    })

    .put('/:item_id', auth.isJournalist,jsonBody, function *(next) {


        // esta es una buena validación para un parametro id entero:
        if(parseInt(this.params.item_id) != this.params.item_id){
            this.status = 500;
            this.body = 'Error parametros';
            return;
        }

        const params = [this.params.item_id];
        const query = "SELECT user_id FROM items WHERE item_id = ?";

        const response = yield mysqlConnections.queryDatabase(query, params);
        const userID = auth.getSession(this.cookies.get("KOASESSID")).id;

        if (response[0].user_id != userID) {
            this.status = statusCode.UNAUTHORIZED;
            this.body = 'is not owner';
            return;
        }

        const location = yield _IsJsonString(this.request.body.location);

        let saveLocationResponse = {};

        /* Sino hay location, no intenta guardar una nueva y manda como null el parametro
        * correspondiente al id del location
        * a la funcion _upsertMarketplaceItem
        * */
        if (location) {

            saveLocationResponse = yield _saveLocation(location);

            if (saveLocationResponse.affectedRows != 1) {
                this.body = "Error al guardar location";
                this.status = statusCode.SERVER_ERROR;
                return;
            }


        }else {
            saveLocationResponse.insertId = null;
        }

        const saveMarketplaceItemResponse = yield _upsertMarketplaceItem(
            this.request.body,
            userID,
            saveLocationResponse.insertId,
            this.params.item_id);

        if (saveMarketplaceItemResponse.affectedRows != 1){
            this.body = "Error al guardar item";
            this.status = statusCode.SERVER_ERROR;
            return;
        }
        const categories = JSON.parse(this.request.body.categories);

        const updateItemCategoryResponse = yield _updateItemCategory(this.params.item_id, categories[0].ca_id);

        if (updateItemCategoryResponse.affectedRows != 1) {
            this.body = "Error al guardar Category";
            this.status = statusCode.SERVER_ERROR;
            return;
        }

        this.body = {
            item_id: updateItemCategoryResponse.insertId
        };

        this.status = statusCode.OK;
    })

    .get('category/:ca_id', auth.isJournalist, function *(next) {

        const query = "SELECT * FROM items";
        const result = yield mysqlConnections.queryDatabase(query);
        this.body = result;
    })


    .get('/:item_id', auth.isJournalist, function *(next) {

        const params = [this.params.item_id];
        const query = "SELECT * FROM items WHERE item_id = ?";
        const picturesQuery = "SELECT pic_name FROM item_pictures WHERE item_id = ? ";

        let result = yield mysqlConnections.queryDatabase(query, params);
        result[0].pictures = yield mysqlConnections.queryDatabase(picturesQuery, params);

        this.body = result[0];
    })

    .get('/categories', auth.isJournalist, function *(next) {

        const query = "SELECT * FROM categories";
        const result = yield mysqlConnections.queryDatabase(query);

        this.body = result;
    })

    .get('/categories/:item_id', auth.isJournalist, function *(next) {

        const params = [this.params.item_id];
        const query = "SELECT cat.* , item_ca_id AS selected " +
            "FROM categories cat  " +
            "LEFT JOIN  item_categories item_cat  " +
            "ON cat.ca_id = item_cat.ca_id " +
            "AND item_cat.item_id = ? ";

        const result = yield mysqlConnections.queryDatabase(query, params);

        this.body = result;
    })

    .put('/sold/:item_id', auth.isJournalist, function *(next) {

        // esta es una buena validación para un parametro id entero:
        if(parseInt(this.params.item_id) != this.params.item_id){
            this.status = 500;
            this.body = 'Error parametros';
            return;
        }

        const paramsItems = [this.params.item_id];
        const query = "SELECT user_id FROM items WHERE item_id =  ?";

        const response = yield mysqlConnections.queryDatabase(query, paramsItems);
        const userID = auth.getSession(this.cookies.get("KOASESSID")).id;

        if (response[0].user_id != userID) {
            this.status = statusCode.UNAUTHORIZED;
            this.body = 'is not owner';
            return;
        }

        const soldQuery = "UPDATE items SET item_sold = ?  WHERE item_id = ?";
        const params = [
            1,
            this.params.item_id
        ];

        const resp = yield mysqlConnections.queryParamsDatabase(soldQuery, params);

        if (resp.affectedRows == 1) {
            this.status = statusCode.OK;
        }else{
            this.status = statusCode.SERVER_ERROR
        }
    })

    .post('/pictures/:item_id', auth.isJournalist, body(), function *(next) {

        if(parseInt(this.params.item_id) != this.params.item_id){
            this.status = 500;
            this.body = 'Error parametros';
            return;
        }

        const params = [this.params.item_id];
        const query = "SELECT user_id FROM items WHERE item_id = ? ";

        const response = yield mysqlConnections.queryDatabase(query, params);
        const userID = auth.getSession(this.cookies.get("KOASESSID")).id;

        if (response[0].user_id != userID) {
            this.status = statusCode.UNAUTHORIZED;
            this.body = 'is not owner';
            return;
        }

        const fileName = yield _buildPicturename(this.params.item_id, this.request.files[0].type);

        if(!imagesController.saveItemPicture(this.request.files[0].path, fileName)){
            this.status = 500;
            this.body = 'Error al guardarla imagen';
            return;
        }

        if (!(yield _savePicture(fileName, this.params.item_id))) {
            this.status = 500;
            this.body = 'Error al guardarla imagen';
            imagesController.deletePicture(fileName, 'marketplace')
            return;
        }

        this.body ={
            pic_name: fileName
        };
        this.status = 200
    })

    .post('/pictures.delete/:item_id', auth.isJournalist, jsonBody, function *(next) {

        if (! (yield _isOwner(this.params.item_id, this.cookies.get("KOASESSID")))) {
            this.status = statusCode.UNAUTHORIZED;
            this.body = 'is not owner';
            return;
        }

        const arraPic = JSON.parse(this.request.body.pictures);

        for(let i in arraPic){

            if (!(yield _deletePicture(arraPic[i].pic_name, this.params.item_id))) {
                this.status = 500;
                this.body = 'Error al borrar imagen';
                return;
            }

            if (! imagesController.deletePicture(arraPic[i].pic_name, 'marketplace')) {
                console.error("Error al borrar la imagen del servidor");
            }

        }

        this.status = statusCode.OK;

    });

function * _buildPicturename(item_id, type) {
    const random = Math.floor(Math.random()*(100000-10000))+10000;
    const extension = type.replace('image/', '.');
    return item_id + '_' + random + extension;
}

function * _saveLocation (location) {

    const query = "INSERT INTO addresses SET " +
        "country_short = ?," +
        "country_long = ?," +
        "locality_short = ?," +
        "locality_long = ?," +
        "adm_level_1_short = ?," +
        "adm_level_1_long = ?," +
        "adm_level_2_short = ?," +
        "adm_level_2_long = ?," +
        "postal_code = ?";

    const queryParams = [
        location.country_short,
        location.country_long,
        location.locality_short,
        location.locality_long,
        location.adm_level_1_short,
        location.adm_level_1_long,
        location.adm_level_2_short,
        location.adm_level_2_long,
        location.postal_code
    ];

    return yield mysqlConnections.queryParamsDatabase(query, queryParams);
}

function * _upsertMarketplaceItem(item_data, owner_id, address_id, item_id) {

    let queryHeader;

    let query = "item_title = ?," +
        "item_summary = ?," +
        "item_amount = ?," +
        "item_sold = ?," +
        "item_published = ?," +
        "item_date = ?," +
        "item_condition = ?," +
        "user_id = ? ";

    const queryParams = [
        item_data.title,
        item_data.text,
        item_data.amount,
        0,
        0,
        Math.round((Date.now()) / 1000),
        item_data.condition,
        owner_id
    ];

    /*Para prevenir que se pise el address y el a_id en la edición*/
    if (address_id) {
        query += ", item_address = ?, a_id = ?";
        queryParams.push(item_data.address);
        queryParams.push(address_id);
    }

    if (item_id) {
        queryHeader = "UPDATE items SET ";
        query += " WHERE item_id = ?";
        if (typeof item_id) {
            item_id = parseInt(item_id);
        }
        queryParams.push(item_id);
    }else{
        queryHeader = "INSERT INTO items SET "

    }
     query = queryHeader + query;

    return yield mysqlConnections.queryParamsDatabase(query, queryParams);
}

function * _saveItemCategory(item_id, category_id) {

    const query = "INSERT INTO item_categories SET " +
        "item_id =?," +
        "ca_id=?";

    const queryParams = [
        item_id,
        category_id
    ];

    return yield mysqlConnections.queryParamsDatabase(query, queryParams);
}

function * _updateItemCategory(item_id, category_id) {

    const params = [item_id];
    const getItemCategoryQuery = "SELECT item_ca_id FROM item_categories WHERE item_id= ?";
    const getItemResult = yield mysqlConnections.queryDatabase(getItemCategoryQuery, params);

    const query = "UPDATE item_categories SET ca_id=? WHERE item_ca_id=?";
    const queryParams = [
        category_id,
        getItemResult[0].item_ca_id
    ];

    return yield mysqlConnections.queryParamsDatabase(query, queryParams);
}

function * _isMainPicture(item_id) {

    const params = [item_id];
    const query = "SELECT COUNT(*) AS counter FROM item_pictures WHERE item_id = ?";
    const result =yield mysqlConnections.queryDatabase(query, params);
    return result[0].counter == 0
}

function * _savePicture(name, item_id) {

    const query = "INSERT INTO item_pictures SET " +
        "item_id =?," +
        "pic_main =?," +
        "pic_name=?";

    const queryParams = [
        item_id,
        yield _isMainPicture(item_id),
        name
    ];

    const response = yield mysqlConnections.queryParamsDatabase(query, queryParams);
    return response.affectedRows == 1
}

function * _deletePicture(name, item_id) {

    const query = "DELETE FROM item_pictures WHERE " +
        "item_id=? AND " +
        "pic_name=?";

    const queryParams = [
        item_id,
        name
    ];

    const response = yield mysqlConnections.queryParamsDatabase(query, queryParams);
    return response.affectedRows == 1
}

function * _isOwner(item_id, cookie) {

    const params = [item_id];
    const query = "SELECT user_id FROM items WHERE item_id = ?";

    const response = yield mysqlConnections.queryDatabase(query, params);
    const userID = auth.getSession(cookie).id;

    return response[0].user_id == userID;
}

function * _IsJsonString(str) {

    let parsedJson;
    try {
       parsedJson = JSON.parse(str);
    } catch (e) {
        return false;
    }

    return parsedJson;
}

module.exports = router;
