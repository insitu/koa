// var session = require('../socket/session');
const constants = require('../constants/activityConstants.js');
const Router = require('koa-router');
const models = require('express-cassandra');
const activitiesController = require('./activities');
const checkSession = require('./checkSession');
const auth = require('./sessionControl');
const graphJournalist = require('./graph/journalists.js');

var router = new Router({
    prefix: '/messages'
});

function findCallback(err, result){
    if(err) console.log(err);

    return result;
}

router.get('/', auth.isJournalist, function *(next) {
    this.body = 'estoy adentro de Messages!';
})
/*
 * Devuelve los threads pertenecientes a un usuario
 */
.get('/threads/:user', auth.isJournalist, function *(next) {

    const userID = auth.getSession(this.cookies.get("KOASESSID")).id;

    var resultCounter = yield function(findCallback) {
        models.instance.Message_Counter.find({user_id: userID}, {raw:true}, findCallback);
    }

    var result = yield function(findCallback) {
        models.instance.Message_Thread.find({user: userID}, { materialized_view: "thread_by_last_message" }, findCallback);
    }

    var arr = Array();

    for(var i in result){

        arr[i] = {};
        arr[i].join_timestamp = result[i].join_timestamp;
        arr[i].last_message = result[i].last_message;
        arr[i].last_message_timestamp = result[i].last_message_timestamp;
        arr[i].last_status = result[i].last_status;
        arr[i].last_user_name = result[i].last_user_name;
        arr[i].mine = result[i].mine;
        arr[i].pending_delivery = result[i].pending_delivery;
        arr[i].pending_read = result[i].pending_read;
        arr[i].single_user_target = result[i].single_user_target;
        arr[i].subject = result[i].subject;
        arr[i].thread_id = result[i].thread_id;
        arr[i].type = result[i].type;
        arr[i].user = result[i].user;
        arr[i].user_target = result[i].user_target;
        arr[i].user_target_location = result[i].user_target_location;
        arr[i].user_target_name = result[i].user_target_name;
        arr[i].user_target_pic = result[i].user_target_pic;
        arr[i].online = result[i].single_user_target ? checkSession.check(result[i].single_user_target) : false;
        if(result[i].single_user_target){
            //arr[i].online = checkSession.check(result[i].single_user_target);
        }

        for(var e = 0; e < resultCounter.length; e++){

            if(result[i].user === resultCounter[e].user_id && result[i].thread_id+"" === resultCounter[e].thread_id+"" ){
                arr[i].counter = parseInt(resultCounter[e].unreads_counter);
                resultCounter.splice(e, 1);
                break;
            }

        }
    }

    this.body = arr;
})
/*
 * Devuelve la cantidad de mensajes sin leer por usuario
 */
.get('/messages-unread/:user', auth.isJournalist, function *(next) {

    const userID = auth.getSession(this.cookies.get("KOASESSID")).id;

    var resultCounter = yield function(findCallback) {
        models.instance.Message_Counter.find({user_id: userID}, {raw:true}, findCallback);
    }
    var total = 0;
    for(var e = 0; e < resultCounter.length; e++){
        total += parseInt(resultCounter[e].unreads_counter);
    }
    this.body = total;
})
/*
 * Devuelve los messages pertenecientes a un thread
 */
.get('/messages/:thread_id/:last_timeuuid/:user_id', auth.isJournalist, function *(next) {

    const currentUserID = auth.getSession(this.cookies.get("KOASESSID")).id;
    var lastTime = this.params.last_timeuuid;
    var grupalThread = false;
    var otherUserID = 0;
    var emptyMessages = true;

    var result = {items: null,
        engagement: null};


    var query = {
        thread_id: models.uuidFromString(this.params.thread_id),
        $limit: 20
    };

    if(lastTime != 0){
        query.timeuuid = {'$lt': models.timeuuidFromString(this.params.last_timeuuid)};
    }

    var messages = yield function(findCallback) {
        models.instance.Message.find(query, {raw:true}, findCallback);
    }
    result.items = messages;

    for(var i in messages){

        //En la primer vuelta obtener informacion para ir a buscar la connection (sólo uno a uno)
        if(i == 0 && messages[i].info && messages[i].info.length == 1){

            var senderID = parseInt(messages[i].sender_id);
            if(senderID != currentUserID){
                otherUserID = senderID;
            }else{
                var objInfo = JSON.parse(messages[i].info[0]);
                otherUserID = objInfo.user_id;
            }
        }

        //Format date
        messages[i].timestamp = Math.floor(Number(messages[i].timeuuid.getDate())/1000);
        emptyMessages = false;
    }

    //It's a group thread by default
    var objConnection = {mine:false, status:'group'};

    //If It is a p2p thread get the connection info
    if(otherUserID > 0){

        engagement = yield graphJournalist.isEngaged(currentUserID, otherUserID);

        const isMyRequest = ( engagement && engagement.id.out_vertex.user_id == currentUserID ) ? true : false;
        objConnection = { mine: isMyRequest};

        if(engagement && engagement.timestamp_accepted){

            objConnection.status = 'accepted';

        }else if(engagement && engagement.timestamp_rejected){

            objConnection.status = 'rejected';

        }else if(engagement){

            objConnection.status = 'pending';

        }else{

            objConnection.status = 'no_connection';
        }

        result.engagement = objConnection;
    }

    // Si es un request de Mo y no esta rejected, dejarlo unlocked
    if (currentUserID === 358 && result.engagement && result.engagement.mine && result.engagement.status != 'rejected') {
        result.engagement.status = 'accepted';
    }

    //Si no hay mensajes devolver el objeto vacío
    if(emptyMessages){
        result.engagement = { mine: false, status: 'no_messages'};
    }

    this.body = result;
});

module.exports = router;

module.exports.makeThreads = function(messageThread){

    var threadToSend = [];
    var timestamp = Number(new Date());
    var threadID = models.uuid();

    for (var i in messageThread.users) {
        var targetIDs = [];
        var targetNames = [];
        var targetPics = [];
        var targetLocations = [];

        for (var e in messageThread.users) {
            if (i !== e) {
                targetIDs.push(messageThread.users[e]);
                targetNames.push(messageThread.names[e]);
                targetPics.push(messageThread.pics[e]);
                targetLocations.push(messageThread.locations[e]);
            }
        }

        var thread = {
            user: messageThread.users[i],
            last_message_timestamp: timestamp,
            thread_id: threadID,
            user_target: targetIDs,
            user_target_name: targetNames,
            user_target_pic: targetPics,
            user_target_location: targetLocations,
            last_message: messageThread.message,
            // last_user_name: i == 0 ? "you" : messageThread.names[0],
            last_user_name: i == 0 ? messageThread.sender_name : messageThread.names[0],
            join_timestamp: timestamp,
            subject: messageThread.subject,
            mine: i == 0,
            last_status : i == 0 ? 1 : 0,
            type: messageThread.type
        };

        if(thread.user_target.length == 1)
        /* La charla es entre dos usuarios */
        /* El single_user_target del sender será igual al usuario target */
            thread.single_user_target = thread.user_target[0];
        else
            thread.single_user_target = 0;

        threadToSend.push(thread);
    }

    var message = {
        thread_id: threadID,
        timeuuid: models.timeuuid(),
        sender_id: messageThread.users[0],
        sender_name: messageThread.names[0],
        message: messageThread.message,
        info:  messageThread.info
    };

    return {threads: threadToSend, message: message};
}

module.exports.newThread = function(messageThread, isDeliveredArray, isSent) {

    var info = [];

    for (var i in messageThread.threads) {

        var counter = 0;

        if(i != 0){
            info.push(JSON.stringify({user_id: messageThread.threads[i].user,
                delivery: isDeliveredArray[i-1] ? messageThread.threads[i].last_message_timestamp : null,
                read: null}));

            messageThread.threads[i].pending_delivery = !isSent;
            messageThread.threads[i].pending_read = true;
            counter = 1;

        }else{
            messageThread.threads[i].pending_delivery = false;
            messageThread.threads[i].pending_read = false;
        }

        var threadModel = new models.instance.Message_Thread(messageThread.threads[i]);
        threadModel.save(function (err) {
            if(err){
                console.log("err 1");
                console.log(err);
            }
        });

        counter = models.datatypes.Long.fromInt(counter);
        messageThread.threads[i].user = parseInt(messageThread.threads[i].user);

        models.instance.Message_Counter.update(
            {user_id:messageThread.threads[i].user, thread_id: messageThread.threads[i].thread_id},
            {unreads_counter:counter}, function(err){
                if(err) console.log('Error notification counter upsert ' + err);
            });
    }

    messageThread.message.info = info;
    // messageThread.message.pending_delivery = !isSent;
    // messageThread.message.pending_read = true;

    _saveMessage(messageThread.message, !isSent);

}

module.exports.makeMessages = function(messageThread){

    var timestamp = Number(new Date());//messageThread.date_frontend;

    var message = {
        last_message_timestamp : timestamp,
        thread_id: messageThread.thread_id,
        timeuuid: models.timeuuid(),
        sender_id: messageThread.sender_id,
        sender_name: messageThread.sender_name,
        message: messageThread.message,
        info: messageThread.info,
        type: messageThread.type
    };

    var threadMine = {
        last_message_timestamp : timestamp,
        last_message : messageThread.message,
        last_user_name : messageThread.sender_name,
        mine : true,
        last_status : 1
    };

    var threadTarget = {
        last_message_timestamp : timestamp,
        last_message : messageThread.message,
        last_user_name : messageThread.sender_name,
        mine : false
    };

    var users = messageThread.users;

    return {message: message, threadMine: threadMine, threadTarget: threadTarget, users: users};

}

/*
 * Actualiza todos los threads y mensajes de un usuario como delivered
 */
module.exports.deliveryAll = function(userID, sendDelivered){

    var threadReturn = [];

    //Recorre todos los thread del usuario que esten pending_delivery = true

    models.instance.Message_Thread.find({user: parseInt(userID), pending_delivery: true},{ materialized_view: "thread_by_pending_delivery" }, function(err, result){

        var timestamp = Number(new Date());

        for(var i in result){

            models.instance.Message.find({thread_id: result[i].thread_id, pending_delivery: true},
                { materialized_view: "message_by_pending_delivery" }, function(eerr, messagesResult){

                    if(eerr){
                        console.log("err 2");
                        console.log(eerr);
                        return;
                    }

                    var isThreadDeliveredToAll = true;
                    var senderID;

                    // Recorremos los mensajes del thread
                    for(var e in messagesResult){

                        if(e == 0){
                            senderID = messagesResult[messagesResult.length - 1].sender_id;
                            threadReturn.push({user_id: parseInt(senderID), thread_id: messagesResult[messagesResult.length - 1].thread_id});

                            if(i == result.length-1){
                                sendDelivered(threadReturn);
                            }
                        }

                        var arr = [];
                        //Recorremos los objetos INFO dentro del mensaje
                        for(var index in messagesResult[e].info){

                            //obtenemos el objeto a partir del string
                            var objUserInfo = JSON.parse(messagesResult[e].info[index]);

                            if(objUserInfo.user_id == userID && objUserInfo.delivery == null){
                                objUserInfo.delivery = timestamp;

                            }else if(objUserInfo.delivery == null){
                                //There is still a pending delivery for another user
                                isThreadDeliveredToAll = false;
                            }
                            arr.push(JSON.stringify(objUserInfo));
                        }

                        var update_values_object_message = {
                            pending_delivery: !isThreadDeliveredToAll,
                            info: arr
                        };

                        models.instance.Message.update({thread_id: messagesResult[e].thread_id, timeuuid: messagesResult[e].timeuuid},
                            update_values_object_message, function(err){
                                if(err){
                                    console.log("err 3");
                                    console.log(err);
                                }
                            }
                        );
                    }

                    var update_values_object = {
                        pending_delivery: false
                    };

                    models.instance.Message_Thread.update({user: parseInt(userID), thread_id: result[i].thread_id, type: result[i].type}, update_values_object, function(err){
                        if(err){
                            console.log("err 4");
                            console.log(err);
                        }
                    });

                    if(isThreadDeliveredToAll){

                        var update_values_object_sender = {
                            last_status: 2 // delivered
                        };

                        const objUpdateKey = {user: parseInt(senderID), thread_id: result[i].thread_id, type: result[i].type};
                        models.instance.Message_Thread.update(objUpdateKey, update_values_object_sender, function(err){
                            if(err){
                                console.log("err 5");
                            }
                        });
                    }
                });
        }
    });

}

module.exports.getThreadOnlyByUser = function(userID, online){
    models.instance.Message_Thread.find({user: userID}, {raw:true}, online);
}

module.exports.readAll = function(data, socketCallback){

    // Actualizar contador de mensajes no leídos que emite el evento.
    var counter = 0;
    var threadID = models.uuidFromString(data.thread);
    models.instance.Message_Counter.find({user_id:data.user, thread_id: threadID}, function(eerr, xresult) {

        if(eerr){
            console.log("err 6");
            console.log(eerr);
            return;
        }

        if(!xresult){
            console.log(xresult);
            console.log("err1166");
            return;
        }

        console.log(' ');
        console.log('Error potencial: Leyendo unreads_counter de threadID: ', threadID);
        console.log(' ');

        counter = -1 * xresult[0].unreads_counter;
        counter = models.datatypes.Long.fromInt(counter);

        models.instance.Message_Counter.update( {user_id:parseInt(data.user), thread_id: threadID},
            {unreads_counter: counter}, function(err){

                if(err){
                    console.log("err 7");
                }
        });

    });

    var senderID;

    //Buscar THREAD por user y thread_id, se usa en todo el método
    models.instance.Message_Thread.find({user: data.user, thread_id: threadID}, {raw:true}, function(err, result){
        if(err){
            console.log("err 7");
            console.log(err);
        }

        var timestamp = Number(new Date());

        // For principal, todo itera en este for. Una sola vuelta ya que busca thread por threadID.
        for(var i in result){

            //Se buscan los mensajes pendientes de leer, para verificar si está pendiente para el usuario del evento
            models.instance.Message.find({thread_id: result[i].thread_id, pending_read: true},
                { materialized_view: "message_by_pending_read" }, function(eerr, messagesResult){

                    if(eerr){
                        console.log("err 8");
                        console.log(eerr);
                    }

                    // Si el thread no tiene pendiente de read, salir
                    if(!messagesResult){
                        return;
                    }

                    var isThreadReadByAll = true;

                    for(var e in messagesResult){

                        // Busco el sender_id del último mensaje del thread, en la primer vuelta.
                        if(e == 0){
                            senderID = messagesResult[messagesResult.length - 1].sender_id;
                        }

                        // Iteramos la info de estado del mensaje para cada integrante del thread. (recipients)
                        var arrayInfoUpdated = [];
                        for(var o in messagesResult[e].info){

                            var objInfo = JSON.parse(messagesResult[e].info[o]);

                            // Si es la info del usuario que disparó evento, marcar como leído.
                            if(objInfo.user_id == data.user && objInfo.read == null){
                                objInfo.read = timestamp;

                            }else if(objInfo.read == null){
                                // There is still users pending to read
                                isThreadReadByAll = false;
                            }

                            arrayInfoUpdated.push(JSON.stringify(objInfo));
                        }

                        var update_values_object_message = {
                            pending_read: !isThreadReadByAll,
                            info: arrayInfoUpdated
                        };

                         // Se actualiza a pending_read = false para un mensaje en particular y se actualiza el campo info.
                        models.instance.Message.update(
                            {thread_id: messagesResult[e].thread_id, timeuuid: messagesResult[e].timeuuid},
                            update_values_object_message, function(err){

                                if(err) {
                                    console.log("err 9");
                                    console.log(err);
                                }

                            }
                        );
                    }

                    // Se actualiza el thread del target a pending_read = false
                    var update_values_object = {
                        pending_read: false
                    };
                    models.instance.Message_Thread.update(
                        {user: parseInt(result[i].user), thread_id: result[i].thread_id, type: result[i].type}, update_values_object, function(err){
                            if(err){
                                console.log("err 10");
                                console.log(err);
                            }
                        }
                    );

                    if(isThreadReadByAll){

                        if(senderID){

                            socketCallback(data.thread, senderID);

                            // Actualizar el thread del sender con last status: leído
                            var update_values_object_sender = {
                                last_status: 3
                            };
                            models.instance.Message_Thread.update(
                                {user: parseInt(senderID), thread_id: result[i].thread_id, type: result[i].type}, update_values_object_sender, function(err){
                                    if(err){
                                        console.log("err 11");
                                        console.log(err);
                                    }
                                }
                            );

                        }

                    }else{
                        // todo leído, nada para hacer.
                    }
                }
            );
        }
    });
    return senderID;
}


module.exports.newMessage = function(messageThread, isDeliveredArray, isSent){

    var info = [];
    var counter = models.datatypes.Long.fromInt(1);
    var threadID = messageThread.message.thread_id;
    var threadType = messageThread.message.type;

    if(threadID instanceof models.datatypes.Uuid){
        console.log('es Uuid');
    }else{
        threadID = models.uuidFromString(messageThread.message.thread_id);
        messageThread.message.thread_id = threadID;
    }

    for (var i in messageThread.users) {

        if (messageThread.users[i] == messageThread.message.sender_id) continue;

        models.instance.Message_Counter.update(
            {user_id:parseInt(messageThread.users[i]), thread_id:  threadID},
            {unreads_counter: counter},
            function(err){
                if(err){
                    console.log('Error notification counter upsert ' + err);
                    return;
                }
            }
        );

        info.push(JSON.stringify({user_id: parseInt(messageThread.users[i]),
            delivery: isDeliveredArray[i] ? messageThread.message.last_message_timestamp : null,
            read: null}));
    }

    messageThread.message.info = info;
    messageThread.message.pending_delivery = !isSent;
    messageThread.message.pending_read = true;

    _saveMessage(messageThread.message, !isSent);

    messageThread.threadMine.pending_delivery = false;
    messageThread.threadMine.pending_read = false;

    messageThread.threadTarget.pending_delivery = !isSent;
    messageThread.threadTarget.pending_read = true;

    messageThread.threadMine.last_status = isSent ? 2 : 1;
    messageThread.threadTarget.last_status = 0;

    messageThread.message.sender_id = parseInt(messageThread.message.sender_id);
    models.instance.Message_Thread.update({user: messageThread.message.sender_id, thread_id: threadID, type:threadType}, messageThread.threadMine, function(err){
        if(err) {
            console.log("err 14");
            console.log(err);
        }
    });

    // Array de usuarios Target para crear mensajes excluyendo el que envia
    var targetUsers = new Array();
    for (var i = 0; i < messageThread.users.length; i++) {
        if(messageThread.users[i] !== messageThread.message.sender_id){
            targetUsers.push(parseInt(messageThread.users[i]));
        }
    }

    if(targetUsers.length < 1) return;

    models.instance.Message_Thread.update({user: {'$in':targetUsers}, thread_id: threadID, type:threadType}, messageThread.threadTarget, function(err){
        if(err){
            console.log("err 15");
            console.log(err);
        }
    });
}

module.exports.getThreadByUser = function ( senderID, targetID, threadType, callback ){
    const filterFields = {
        user: senderID,
        single_user_target: targetID,
        type: threadType
    };
    models.instance.Message_Thread.find(filterFields,{ materialized_view: "thread_by_single_target" }, callback);
}

/*
 * Build the activity object to log MESSAGE_SENT activity
 */
function _getActivityInfo(message){

    var userInfo = null;
    var users_id = [];
    for(var i=0; i < message.info.length; i++){
        userInfo = JSON.parse(message.info[i]);
        users_id.push(userInfo.user_id);
    }

    return {
        type:               constants.MESSAGE_SENT,
        user_id:            message.sender_id,
        user_name:          message.sender_name,
        user_pic:           null,
        target_object_id:   message.thread_id + '',
        objects_id:         [message.thread_id + ''],
        details:            [message.message],
        users_id:           users_id,
        users_name:         [],
        users_pic:          []
    };
}

function _saveMessage(message, sent){

    message.pending_read = true;
    message.pending_delivery = sent;
    var messageModel = new models.instance.Message(message);

    messageModel.save(function(err){
        if(err){
            console.log("err 16");
            console.log(err);
        }
    });

}

module.exports.getMessages = function(thread_id, limit, callback) {

    var query = {
        thread_id: models.uuidFromString(this.params.thread_id),
        $limit: limit
    };
    models.instance.Message.find(query, {raw: true}, callback);
}
