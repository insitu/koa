const jwt = require('koa-jwt');
const httpStatus = require('../constants/httpStatusConstants.js');
const key = '38F720C5-F711-4FB2-B1EA-139861C936AA';

const ROLE_READER = 10;
const ROLE_JOURNALIST = 20;
const ROLE_ADMIN = 30;

const EXPIRE_MINUTES = 30;
const NOEPIRE_DAYS = 14;

module.exports.getKOASESSID = function(payload) {
    return jwt.sign(payload, key);
}

module.exports.getSession = function(sessionKlt) {
    if (!sessionKlt) {
        throw Error('sessionKlt must be provided.');
    }
    return jwt.verify(sessionKlt, key);
}

/*
 * Receives a jwt encoded hash to be verified
 */
module.exports.getJWTObject = function(encoded) {
    return _getJWTObject(encoded);
}

module.exports.isReader = function *(next) {

    const sessionKlt = this.cookies.get("KOASESSID");
    const objResult = _validate(sessionKlt, ROLE_READER);

    if (objResult.status !== httpStatus.OK) {
        this.status = objResult.status;
        this.body = objResult.body;
        return;
    }
    // renew session
    this.cookies.set('KOASESSID', objResult.koaSessId, { httpOnly: false });
    yield next;
}

module.exports.isJournalist = function *(next) {

    const sessionKlt = this.cookies.get("KOASESSID");
    const objResult = _validate(sessionKlt, ROLE_JOURNALIST);

    if (objResult.status !== httpStatus.OK) {
        this.status = objResult.status;
        this.body = objResult.body;
        return;
    }
    // renew session
    this.cookies.set('KOASESSID', objResult.koaSessId, { httpOnly: false });
    yield next;
}

module.exports.isAdmin = function *(next) {

    const sessionKlt = this.cookies.get("KOASESSID");
    const objResult = _validate(sessionKlt, ROLE_ADMIN);

    if (objResult.status !== httpStatus.OK) {
        this.status = objResult.status;
        this.body = objResult.body;
        return;
    }
    // renew session
    this.cookies.set('KOASESSID', objResult.koaSessId, { httpOnly: false });
    yield next;
}

module.exports.isAdminRole = function() {

    const sessionKlt = this.cookies.get("KOASESSID");
    const objResult = _validate(sessionKlt, ROLE_ADMIN);

    if (objResult.status !== httpStatus.OK) return false;
    return true;
}

module.exports.buildPayload = function(userData, noexpire) {

    if(!userData || parseInt(userData.user_id) != userData.user_id){
        console.log('KABUMMMMM ****');
        console.log(userData);
        throw new Error('Invalid user to build payload');
    }

    var exp = _getExpirationTimestamp(noexpire);
    var iat = new Date();
    iat = Math.floor(Number(iat)/1000);

    console.log('');
    console.log('Payload for ' + userData.email + ' expiration date: ' + new Date(exp*1000) + ' timestamp ' + exp + ' noexpire: ' + noexpire);
    console.log('');

    return {
        id: userData.user_id,
        role: userData.role ? userData.role[0] : 0,
        iat: iat,
        exp: exp,
        noexpire: noexpire
    };
}

/******************************************************************************/
// private scope
/******************************************************************************/

function _validate(sessionKlt, role){

    const currentSession = _getJWTObject(sessionKlt);
    var objResult = { status: httpStatus.OK };

    if (!currentSession) {
        objResult.status = httpStatus.NOT_ALLOWED;
        objResult.body = "Session expired or invalid";
        return objResult;
    }

    if (!_isValidRole(role, currentSession) ) {
        objResult.status = httpStatus.UNAUTHORIZED;
        objResult.body = "Access denied";
        return objResult;
    }

    // renew and hash new expiration date and set cookie
    currentSession.exp = _getExpirationTimestamp(currentSession.noexpire);
    objResult.koaSessId = jwt.sign(currentSession, key);
    return objResult;
}

function _getExpirationTimestamp(noexpire){

    var expDate = new Date();

    if(noexpire){
        expDate = expDate.setDate(expDate.getDate() + NOEPIRE_DAYS);
    }else{
        expDate = expDate.setMinutes(expDate.getMinutes() + EXPIRE_MINUTES);
    }

    return Math.floor(Number(expDate)/1000);
}

function _getJWTObject(sessionKlt){

    try {
        return jwt.verify(sessionKlt, key);
    } catch (e) {
        return false;
    }
}

function _isValidRole(role, session){

    try {

        if(!parseInt(session.role) || parseInt(session.role) < role){
            this.status = httpStatus.UNAUTHORIZED;
            this.body = "Access denied";
            return false;
        }

    } catch(err) {
        this.status = httpStatus.NOT_ALLOWED;
        this.body = "Session expired or invalid";
        return false;
    }
    return session;
}
