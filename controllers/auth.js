const passport = require('koa-passport')
var mysqlConnections = require('./mysqlConnections')
var request = require('koa-request');

const NODE_APP = 'https://www.kulectiv.com:3000';
// const NODE_APP = 'http://localhost:3000';


passport.serializeUser(function(user, done) {
    done(null, user);
});

passport.deserializeUser(function(user, done) {
    done(null, user);
});

const TwitterStrategy = require('passport-twitter').Strategy
passport.use(new TwitterStrategy({
        // consumerKey: 'P6BR9RGvoTPNl4os74fYLHt1J', //pablo
        includeEmail: true,
        consumerKey: 'NGPEp16WKvHTKZCv94xmvy1iw',
        // consumerSecret: 'mtGPgRneyUOmrHLKwRzoDooGfjH7YnI0H7fKdp1ar6VKnPp0iy', //pablo
        consumerSecret: 'h8YCl9AOL2yRDEKM10ZXwlAQBtMwjCLRnXTWy96VIqhAXxZYgk',
        // callbackURL: 'http://localhost:3000/login/twitter/callback'
        callbackURL: NODE_APP + '/login/twitter/callback'
    },
    function(token, tokenSecret, profile, cb) {

        var email = profile.emails ? profile.emails[0].value : 'no_email';

        var twitterUser = {
            twitter_id: profile.id,
            tokenSecret: tokenSecret,
            name: profile.displayName,
            email: email,
            bio: profile._json.description
        };

        return cb(null, twitterUser);
    }
));
