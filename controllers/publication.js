"use strict";
const Router = require('koa-router');
const auth = require('./sessionControl');
const mysqlConnections = require('./mysqlConnections');
const statusCode = require('../constants/httpStatusConstants.js');
const jsonBody = require('koa-json-body')();


const router = new Router({
    prefix: '/publications'
});

router
    .get('/:user_id', auth.isJournalist, function *(next) {

        if(parseInt(this.params.user_id) != this.params.user_id){
            this.status = 500;
            this.body = {
                message: "bad params"
            };
            return;
        }

        const publications = yield _getPublications(this.params.user_id);

        this.body = publications;
        this.status = statusCode.OK;
    })

    .post('/', auth.isJournalist, jsonBody, function * (next) {

        const userID = auth.getSession(this.cookies.get("KOASESSID")).id;
        let publication = this.request.body;

        const pubilcation_id = yield _insertPublications(publication, userID);

        if (!pubilcation_id) {
            this.body = "Error to save publication";
            this.status = statusCode.SERVER_ERROR;
            return;
        }
        publication.pu_id = pubilcation_id;

        this.body = publication;
        this.status = statusCode.OK
    })

    .post('/:pu_id', auth.isJournalist, function * (next) {

        const is_owner = yield _isOwner(this.params.pu_id, this.cookies.get("KOASESSID"));

        if (!is_owner) {
            this.status = statusCode.UNAUTHORIZED;
            this.body = 'is not owner';
            return;
        }

        if (!(yield _deletePublication(this.params.pu_id))) {
            this.status = 500;
            this.body = 'Error to delete publication';
            return;
        }
        this.status = statusCode.OK
    });




function  * _getPublications(user_id) {

    const query = "SELECT * FROM publications WHERE user_id=" + user_id;
    return yield mysqlConnections.queryDatabase(query);

}

function * _insertPublications(publication, user_id) {
    const query ="INSERT INTO publications SET " +
        "user_id = ?, " +
        "pu_title = ?, " +
        "pu_url = ?, " +
        "pu_publisher = ?";

    const queryParams = [
        user_id,
        publication.pu_title,
        publication.pu_url,
        publication.pu_publisher
    ];

    const response =  yield mysqlConnections.queryParamsDatabase(query, queryParams);

    if (response.affectedRows == 1) {
        return response.insertId;
    }

    return false
}

function * _deletePublication(pu_id) {
    const query = "DELETE FROM publications WHERE pu_id=" + pu_id;
    const response = yield mysqlConnections.queryDatabase(query);
    return response.affectedRows == 1
}

function * _isOwner(item_id, cookie) {

    const query = "SELECT user_id FROM publications WHERE pu_id=" + item_id;

    const response = yield mysqlConnections.queryDatabase(query);
    const userID = auth.getSession(cookie).id;

    return response[0].user_id == userID;
}

module.exports = router;

