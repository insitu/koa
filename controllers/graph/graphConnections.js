const dse = require('dse-driver');

const client = new dse.Client({
    // contactPoints: ['35.185.35.220', '104.196.171.120', '35.185.77.178'],
    contactPoints: ['35.185.35.220'],
    graphOptions: { name: 'journalist' }
});


module.exports.getClient = function(){
    return client;
};

module.exports.getTypes = function(){
    return dse.types;
};
