const Router = require('koa-router');
const auth = require('../sessionControl');
const graphConnections = require('./graphConnections');

const router = new Router({
  prefix: '/countries'
});

const client = graphConnections.getClient();

var graphCallback = function(err, result){
    if(err){
        throw err;
    }
    return result;
}

router.get('/', function *(next) {
    this.body = 'estoy adentro de countries !';
})

// .get('/insert', function *(next) {
//
//     var arrayCountries  = [];
//
//     arrayCountries.push({shortName: "AD", longName: "Andorra" });
//     arrayCountries.push({shortName: "AE", longName: "United Arab Emirates" });
//     arrayCountries.push({shortName: "AF", longName: "Afghanistan" });
//     arrayCountries.push({shortName: "AG", longName: "Antigua and Barbuda" });
//     arrayCountries.push({shortName: "AI", longName: "Anguilla" });
//     arrayCountries.push({shortName: "AL", longName: "Albania" });
//     arrayCountries.push({shortName: "AM", longName: "Armenia" });
//     arrayCountries.push({shortName: "AO", longName: "Angola" });
//     arrayCountries.push({shortName: "AQ", longName: "Antarctica" });
//     arrayCountries.push({shortName: "AR", longName: "Argentina" });
//     arrayCountries.push({shortName: "AS", longName: "American Samoa" });
//     arrayCountries.push({shortName: "AT", longName: "Austria" });
//     arrayCountries.push({shortName: "AU", longName: "Australia" });
//     arrayCountries.push({shortName: "AW", longName: "Aruba" });
//     arrayCountries.push({shortName: "AX", longName: "Åland Islands" });
//     arrayCountries.push({shortName: "AZ", longName: "Azerbaijan" });
//     arrayCountries.push({shortName: "BA", longName: "Bosnia and Herzegovina" });
//     arrayCountries.push({shortName: "BB", longName: "Barbados" });
//     arrayCountries.push({shortName: "BD", longName: "Bangladesh" });
//     arrayCountries.push({shortName: "BE", longName: "Belgium" });
//     arrayCountries.push({shortName: "BF", longName: "Burkina Faso" });
//     arrayCountries.push({shortName: "BG", longName: "Bulgaria" });
//     arrayCountries.push({shortName: "BH", longName: "Bahrain" });
//     arrayCountries.push({shortName: "BI", longName: "Burundi" });
//     arrayCountries.push({shortName: "BJ", longName: "Benin" });
//     arrayCountries.push({shortName: "BL", longName: "Saint Barthélemy" });
//     arrayCountries.push({shortName: "BM", longName: "Bermuda" });
//     arrayCountries.push({shortName: "BN", longName: "Brunei Darussalam" });
//     arrayCountries.push({shortName: "BO", longName: "Bolivia (Plurinational State of)" });
//     arrayCountries.push({shortName: "BQ", longName: "Bonaire, Sint Eustatius and Saba" });
//     arrayCountries.push({shortName: "BR", longName: "Brazil" });
//     arrayCountries.push({shortName: "BS", longName: "Bahamas" });
//     arrayCountries.push({shortName: "BT", longName: "Bhutan" });
//     arrayCountries.push({shortName: "BV", longName: "Bouvet Island" });
//     arrayCountries.push({shortName: "BW", longName: "Botswana" });
//     arrayCountries.push({shortName: "BY", longName: "Belarus" });
//     arrayCountries.push({shortName: "BZ", longName: "Belize" });
//     arrayCountries.push({shortName: "CA", longName: "Canada" });
//     arrayCountries.push({shortName: "CC", longName: "Cocos (Keeling) Islands" });
//     arrayCountries.push({shortName: "CD", longName: "Congo (Democratic Republic of the)" });
//     arrayCountries.push({shortName: "CF", longName: "Central African Republic" });
//     arrayCountries.push({shortName: "CG", longName: "Congo" });
//     arrayCountries.push({shortName: "CH", longName: "Switzerland" });
//     arrayCountries.push({shortName: "CI", longName: "Côte d'Ivoire" });
//     arrayCountries.push({shortName: "CK", longName: "Cook Islands" });
//     arrayCountries.push({shortName: "CL", longName: "Chile" });
//     arrayCountries.push({shortName: "CM", longName: "Cameroon" });
//     arrayCountries.push({shortName: "CN", longName: "China" });
//     arrayCountries.push({shortName: "CO", longName: "Colombia" });
//     arrayCountries.push({shortName: "CR", longName: "Costa Rica" });
//     arrayCountries.push({shortName: "CU", longName: "Cuba" });
//     arrayCountries.push({shortName: "CV", longName: "Cabo Verde" });
//     arrayCountries.push({shortName: "CW", longName: "Curaçao" });
//     arrayCountries.push({shortName: "CX", longName: "Christmas Island" });
//     arrayCountries.push({shortName: "CY", longName: "Cyprus" });
//     arrayCountries.push({shortName: "CZ", longName: "Czech Republic" });
//     arrayCountries.push({shortName: "DE", longName: "Germany" });
//     arrayCountries.push({shortName: "DJ", longName: "Djibouti" });
//     arrayCountries.push({shortName: "DK", longName: "Denmark" });
//     arrayCountries.push({shortName: "DM", longName: "Dominica" });
//     arrayCountries.push({shortName: "DO", longName: "Dominican Republic" });
//     arrayCountries.push({shortName: "DZ", longName: "Algeria" });
//     arrayCountries.push({shortName: "EC", longName: "Ecuador" });
//     arrayCountries.push({shortName: "EE", longName: "Estonia" });
//     arrayCountries.push({shortName: "EG", longName: "Egypt" });
//     arrayCountries.push({shortName: "EH", longName: "Western Sahara" });
//     arrayCountries.push({shortName: "ER", longName: "Eritrea" });
//     arrayCountries.push({shortName: "ES", longName: "Spain" });
//     arrayCountries.push({shortName: "ET", longName: "Ethiopia" });
//     arrayCountries.push({shortName: "FI", longName: "Finland" });
//     arrayCountries.push({shortName: "FJ", longName: "Fiji" });
//     arrayCountries.push({shortName: "FK", longName: "Falkland Islands (Malvinas)" });
//     arrayCountries.push({shortName: "FM", longName: "Micronesia (Federated States of)" });
//     arrayCountries.push({shortName: "FO", longName: "Faroe Islands" });
//     arrayCountries.push({shortName: "FR", longName: "France" });
//     arrayCountries.push({shortName: "GA", longName: "Gabon" });
//     arrayCountries.push({shortName: "GB", longName: "United Kingdom of Great Britain and Northern Ireland" });
//     arrayCountries.push({shortName: "GD", longName: "Grenada" });
//     arrayCountries.push({shortName: "GE", longName: "Georgia" });
//     arrayCountries.push({shortName: "GF", longName: "French Guiana" });
//     arrayCountries.push({shortName: "GG", longName: "Guernsey" });
//     arrayCountries.push({shortName: "GH", longName: "Ghana" });
//     arrayCountries.push({shortName: "GI", longName: "Gibraltar" });
//     arrayCountries.push({shortName: "GL", longName: "Greenland" });
//     arrayCountries.push({shortName: "GM", longName: "Gambia" });
//     arrayCountries.push({shortName: "GN", longName: "Guinea" });
//     arrayCountries.push({shortName: "GP", longName: "Guadeloupe" });
//     arrayCountries.push({shortName: "GQ", longName: "Equatorial Guinea" });
//     arrayCountries.push({shortName: "GR", longName: "Greece" });
//     arrayCountries.push({shortName: "GS", longName: "South Georgia and the South Sandwich Islands" });
//     arrayCountries.push({shortName: "GT", longName: "Guatemala" });
//     arrayCountries.push({shortName: "GU", longName: "Guam" });
//     arrayCountries.push({shortName: "GW", longName: "Guinea-Bissau" });
//     arrayCountries.push({shortName: "GY", longName: "Guyana" });
//     arrayCountries.push({shortName: "HK", longName: "Hong Kong" });
//     arrayCountries.push({shortName: "HM", longName: "Heard Island and McDonald Islands" });
//     arrayCountries.push({shortName: "HN", longName: "Honduras" });
//     arrayCountries.push({shortName: "HR", longName: "Croatia" });
//     arrayCountries.push({shortName: "HT", longName: "Haiti" });
//     arrayCountries.push({shortName: "HU", longName: "Hungary" });
//     arrayCountries.push({shortName: "ID", longName: "Indonesia" });
//     arrayCountries.push({shortName: "IE", longName: "Ireland" });
//     arrayCountries.push({shortName: "IL", longName: "Israel" });
//     arrayCountries.push({shortName: "IM", longName: "Isle of Man" });
//     arrayCountries.push({shortName: "IN", longName: "India" });
//     arrayCountries.push({shortName: "IO", longName: "British Indian Ocean Territory" });
//     arrayCountries.push({shortName: "IQ", longName: "Iraq" });
//     arrayCountries.push({shortName: "IR", longName: "Iran (Islamic Republic of)" });
//     arrayCountries.push({shortName: "IS", longName: "Iceland" });
//     arrayCountries.push({shortName: "IT", longName: "Italy" });
//     arrayCountries.push({shortName: "JE", longName: "Jersey" });
//     arrayCountries.push({shortName: "JM", longName: "Jamaica" });
//     arrayCountries.push({shortName: "JO", longName: "Jordan" });
//     arrayCountries.push({shortName: "JP", longName: "Japan" });
//     arrayCountries.push({shortName: "KE", longName: "Kenya" });
//     arrayCountries.push({shortName: "KG", longName: "Kyrgyzstan" });
//     arrayCountries.push({shortName: "KH", longName: "Cambodia" });
//     arrayCountries.push({shortName: "KI", longName: "Kiribati" });
//     arrayCountries.push({shortName: "KM", longName: "Comoros" });
//     arrayCountries.push({shortName: "KN", longName: "Saint Kitts and Nevis" });
//     arrayCountries.push({shortName: "KP", longName: "Korea (Democratic People's Republic of)" });
//     arrayCountries.push({shortName: "KR", longName: "Korea (Republic of)" });
//     arrayCountries.push({shortName: "KW", longName: "Kuwait" });
//     arrayCountries.push({shortName: "KY", longName: "Cayman Islands" });
//     arrayCountries.push({shortName: "KZ", longName: "Kazakhstan" });
//     arrayCountries.push({shortName: "LA", longName: "Lao People's Democratic Republic" });
//     arrayCountries.push({shortName: "LB", longName: "Lebanon" });
//     arrayCountries.push({shortName: "LC", longName: "Saint Lucia" });
//     arrayCountries.push({shortName: "LI", longName: "Liechtenstein" });
//     arrayCountries.push({shortName: "LK", longName: "Sri Lanka" });
//     arrayCountries.push({shortName: "LR", longName: "Liberia" });
//     arrayCountries.push({shortName: "LS", longName: "Lesotho" });
//     arrayCountries.push({shortName: "LT", longName: "Lithuania" });
//     arrayCountries.push({shortName: "LU", longName: "Luxembourg" });
//     arrayCountries.push({shortName: "LV", longName: "Latvia" });
//     arrayCountries.push({shortName: "LY", longName: "Libya" });
//     arrayCountries.push({shortName: "MA", longName: "Morocco" });
//     arrayCountries.push({shortName: "MC", longName: "Monaco" });
//     arrayCountries.push({shortName: "MD", longName: "Moldova (Republic of)" });
//     arrayCountries.push({shortName: "ME", longName: "Montenegro" });
//     arrayCountries.push({shortName: "MF", longName: "Saint Martin (French part)" });
//     arrayCountries.push({shortName: "MG", longName: "Madagascar" });
//     arrayCountries.push({shortName: "MH", longName: "Marshall Islands" });
//     arrayCountries.push({shortName: "MK", longName: "Macedonia (the former Yugoslav Republic of)" });
//     arrayCountries.push({shortName: "ML", longName: "Mali" });
//     arrayCountries.push({shortName: "MM", longName: "Myanmar" });
//     arrayCountries.push({shortName: "MN", longName: "Mongolia" });
//     arrayCountries.push({shortName: "MO", longName: "Macao" });
//     arrayCountries.push({shortName: "MP", longName: "Northern Mariana Islands" });
//     arrayCountries.push({shortName: "MQ", longName: "Martinique" });
//     arrayCountries.push({shortName: "MR", longName: "Mauritania" });
//     arrayCountries.push({shortName: "MS", longName: "Montserrat" });
//     arrayCountries.push({shortName: "MT", longName: "Malta" });
//     arrayCountries.push({shortName: "MU", longName: "Mauritius" });
//     arrayCountries.push({shortName: "MV", longName: "Maldives" });
//     arrayCountries.push({shortName: "MW", longName: "Malawi" });
//     arrayCountries.push({shortName: "MX", longName: "Mexico" });
//     arrayCountries.push({shortName: "MY", longName: "Malaysia" });
//     arrayCountries.push({shortName: "MZ", longName: "Mozambique" });
//     arrayCountries.push({shortName: "NA", longName: "Namibia" });
//     arrayCountries.push({shortName: "NC", longName: "New Caledonia" });
//     arrayCountries.push({shortName: "NE", longName: "Niger" });
//     arrayCountries.push({shortName: "NF", longName: "Norfolk Island" });
//     arrayCountries.push({shortName: "NG", longName: "Nigeria" });
//     arrayCountries.push({shortName: "NI", longName: "Nicaragua" });
//     arrayCountries.push({shortName: "NL", longName: "Netherlands" });
//     arrayCountries.push({shortName: "NO", longName: "Norway" });
//     arrayCountries.push({shortName: "NP", longName: "Nepal" });
//     arrayCountries.push({shortName: "NR", longName: "Nauru" });
//     arrayCountries.push({shortName: "NU", longName: "Niue" });
//     arrayCountries.push({shortName: "NZ", longName: "New Zealand" });
//     arrayCountries.push({shortName: "OM", longName: "Oman" });
//     arrayCountries.push({shortName: "PA", longName: "Panama" });
//     arrayCountries.push({shortName: "PE", longName: "Peru" });
//     arrayCountries.push({shortName: "PF", longName: "French Polynesia" });
//     arrayCountries.push({shortName: "PG", longName: "Papua New Guinea" });
//     arrayCountries.push({shortName: "PH", longName: "Philippines" });
//     arrayCountries.push({shortName: "PK", longName: "Pakistan" });
//     arrayCountries.push({shortName: "PL", longName: "Poland" });
//     arrayCountries.push({shortName: "PM", longName: "Saint Pierre and Miquelon" });
//     arrayCountries.push({shortName: "PN", longName: "Pitcairn" });
//     arrayCountries.push({shortName: "PR", longName: "Puerto Rico" });
//     arrayCountries.push({shortName: "PS", longName: "Palestine, State of" });
//     arrayCountries.push({shortName: "PT", longName: "Portugal" });
//     arrayCountries.push({shortName: "PW", longName: "Palau" });
//     arrayCountries.push({shortName: "PY", longName: "Paraguay" });
//     arrayCountries.push({shortName: "QA", longName: "Qatar" });
//     arrayCountries.push({shortName: "RE", longName: "Réunion" });
//     arrayCountries.push({shortName: "RO", longName: "Romania" });
//     arrayCountries.push({shortName: "RS", longName: "Serbia" });
//     arrayCountries.push({shortName: "RU", longName: "Russian Federation" });
//     arrayCountries.push({shortName: "RW", longName: "Rwanda" });
//     arrayCountries.push({shortName: "SA", longName: "Saudi Arabia" });
//     arrayCountries.push({shortName: "SB", longName: "Solomon Islands" });
//     arrayCountries.push({shortName: "SC", longName: "Seychelles" });
//     arrayCountries.push({shortName: "SD", longName: "Sudan" });
//     arrayCountries.push({shortName: "SE", longName: "Sweden" });
//     arrayCountries.push({shortName: "SG", longName: "Singapore" });
//     arrayCountries.push({shortName: "SH", longName: "Saint Helena, Ascension and Tristan da Cunha" });
//     arrayCountries.push({shortName: "SI", longName: "Slovenia" });
//     arrayCountries.push({shortName: "SJ", longName: "Svalbard and Jan Mayen" });
//     arrayCountries.push({shortName: "SK", longName: "Slovakia" });
//     arrayCountries.push({shortName: "SL", longName: "Sierra Leone" });
//     arrayCountries.push({shortName: "SM", longName: "San Marino" });
//     arrayCountries.push({shortName: "SN", longName: "Senegal" });
//     arrayCountries.push({shortName: "SO", longName: "Somalia" });
//     arrayCountries.push({shortName: "SR", longName: "Suriname" });
//     arrayCountries.push({shortName: "SS", longName: "South Sudan" });
//     arrayCountries.push({shortName: "ST", longName: "Sao Tome and Principe" });
//     arrayCountries.push({shortName: "SV", longName: "El Salvador" });
//     arrayCountries.push({shortName: "SX", longName: "Sint Maarten (Dutch part)" });
//     arrayCountries.push({shortName: "SY", longName: "Syrian Arab Republic" });
//     arrayCountries.push({shortName: "SZ", longName: "Swaziland" });
//     arrayCountries.push({shortName: "TC", longName: "Turks and Caicos Islands" });
//     arrayCountries.push({shortName: "TD", longName: "Chad" });
//     arrayCountries.push({shortName: "TF", longName: "French Southern Territories" });
//     arrayCountries.push({shortName: "TG", longName: "Togo" });
//     arrayCountries.push({shortName: "TH", longName: "Thailand" });
//     arrayCountries.push({shortName: "TJ", longName: "Tajikistan" });
//     arrayCountries.push({shortName: "TK", longName: "Tokelau" });
//     arrayCountries.push({shortName: "TL", longName: "Timor-Leste" });
//     arrayCountries.push({shortName: "TM", longName: "Turkmenistan" });
//     arrayCountries.push({shortName: "TN", longName: "Tunisia" });
//     arrayCountries.push({shortName: "TO", longName: "Tonga" });
//     arrayCountries.push({shortName: "TR", longName: "Turkey" });
//     arrayCountries.push({shortName: "TT", longName: "Trinidad and Tobago" });
//     arrayCountries.push({shortName: "TV", longName: "Tuvalu" });
//     arrayCountries.push({shortName: "TW", longName: "Taiwan, Province of China[a]" });
//     arrayCountries.push({shortName: "TZ", longName: "Tanzania, United Republic of" });
//     arrayCountries.push({shortName: "UA", longName: "Ukraine" });
//     arrayCountries.push({shortName: "UG", longName: "Uganda" });
//     arrayCountries.push({shortName: "UM", longName: "United States Minor Outlying Islands" });
//     arrayCountries.push({shortName: "US", longName: "United States of America" });
//     arrayCountries.push({shortName: "UY", longName: "Uruguay" });
//     arrayCountries.push({shortName: "UZ", longName: "Uzbekistan" });
//     arrayCountries.push({shortName: "VA", longName: "Holy See" });
//     arrayCountries.push({shortName: "VC", longName: "Saint Vincent and the Grenadines" });
//     arrayCountries.push({shortName: "VE", longName: "Venezuela (Bolivarian Republic of)" });
//     arrayCountries.push({shortName: "VG", longName: "Virgin Islands (British)" });
//     arrayCountries.push({shortName: "VI", longName: "Virgin Islands (U.S.)" });
//     arrayCountries.push({shortName: "VN", longName: "Viet Nam" });
//     arrayCountries.push({shortName: "VU", longName: "Vanuatu" });
//     arrayCountries.push({shortName: "WF", longName: "Wallis and Futuna" });
//     arrayCountries.push({shortName: "WS", longName: "Samoa" });
//     arrayCountries.push({shortName: "YE", longName: "Yemen" });
//     arrayCountries.push({shortName: "YT", longName: "Mayotte" });
//     arrayCountries.push({shortName: "ZA", longName: "South Africa" });
//     arrayCountries.push({shortName: "ZM", longName: "Zambia" });
//     arrayCountries.push({shortName: "ZW", longName: "Zimbabwe" });
//
//     const query = 'g.addV(label, "country", "level", "1", "country_short", shortName, "country_long", longName)';
//     var params;
//
//     for(var i=0; i < arrayCountries.length; i++){
//
//         params = { "shortName": arrayCountries[i].shortName, "longName": arrayCountries[i].longName};
//         client.executeGraph(query, params, graphCallback);
//     }
//
//     this.body = 'Done';
// })

.get('/get.combo.list', auth.isJournalist, function *(next) {

    //TODO: arreglar esto porque no tiene indice
    this.body = 'Temporalmente inhabilitado';
    return;

    var result = yield function(graphCallback) {
        const query = 'g.V().hasLabel("country").has("level", 1).limit(250)';
        client.executeGraph(query, graphCallback );
    }
    console.log('**** RESULTAD');
    console.log(result.length);

    var countryList = [];
    result.forEach(function(vertex) {
        countryList.push({value:vertex.properties.country_short[0].value, label: vertex.properties.country_long[0].value});
    });

    this.body = countryList;
})

.get('/country.by.shortname/:short_name', auth.isJournalist, function *(next) {

    this.body = 'Temporalmente inhabilitado';
    return;

    const shortName = this.params.short_name;

    if(!shortName || shortName.length < 2){
        this.body = '';
        return;
    }

    var result = yield function(graphCallback) {
        const query = 'g.V().hasLabel("country").has("level", 1).has("country_short", "'+shortName+'").limit(1).values("country_long")';
        client.executeGraph(query, graphCallback );
    }

    // Note: por alguna extraña razon recibo null en el front end si mando el string en lugar del array
    var objResult = result.first();
    this.body = objResult ? objResult.country_long : '';
});


module.exports = router;
