const Router = require('koa-router');
const auth = require('../sessionControl');
const graphConnections = require('./graphConnections');
const jsonBody = require('koa-json-body')();

const router = new Router({
  prefix: '/requirements'
});

const client = graphConnections.getClient();

var graphCallback = function(err, result){
    if(err){
        throw err;
    }
    return result;
}

router.get('/', function *(next) {
    this.body = 'estoy adentro de requirements !';
})

.post('/save', auth.isAdmin, jsonBody, function *(next) {

    var result = yield function(graphCallback) {

        const query = 'g.addV(label, "requirement", "country_short", countryShort, "description", description, "url", linkApply, "how_to", howTo, "link_source", linkSource)';
        const params = { "countryShort": this.request.body.countryShort,
                         "description": this.request.body.description,
                         "linkApply": this.request.body.linkApply,
                         "howTo": this.request.body.howTo,
                         "linkSource": this.request.body.linkSource };

        client.executeGraph(query, params, graphCallback );
    }
    this.body = 'Done';
})

.post('/delete', auth.isAdmin, jsonBody, function *(next) {

    var result = yield function(graphCallback) {

        const query = 'g.V().hasId("{~label=requirement, community_id='+this.request.body.community_id+', member_id='+this.request.body.member_id+'}").drop()';
        client.executeGraph(query, graphCallback );
    }

    this.body = 'Done';
})

.get('/get.list', auth.isJournalist, function *(next) {

    var result = yield function(graphCallback) {
        const query = 'g.V().hasLabel("requirement").valueMap(true)';
        client.executeGraph(query, graphCallback );
    }

    this.body = result.toArray();
})

.get('/get.by.country/:country_short', auth.isJournalist, function *(next) {

    var result = yield function(graphCallback) {
        const params = {"countryShort": this.params.country_short};
        const query = 'g.V().hasLabel("requirement").has("country_short", countryShort) valueMap(true)';
        client.executeGraph(query, params, graphCallback );
    }

    this.body = result.toArray();
});

module.exports = router;
