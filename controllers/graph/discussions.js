"use strict";
const Router = require('koa-router');
const auth = require('../sessionControl');
const tagConstants = require('../../constants/tagConstants');
const countryConstants = require('../../constants/countries');
const graphConnections = require('./graphConnections');
const models = require('express-cassandra');
const graphHelper = require('../../graph_helper/models');
const tagsController = require('./tags');
const notificationsController = require('../notifications');
const activityConstants = require('../../constants/activityConstants.js');
const jsonBody = require('koa-json-body')();
const matchesExpert = require('../../experts/matchesExpert');

const router = new Router({
  prefix: '/discussions'
});

const graphClient = graphConnections.getClient();

var graphCallback = function(err, result){
    if(err){
        throw err;
    }
    return result;
}

router.get('/:filter', auth.isJournalist, function *(next) {

    const userID = auth.getSession(this.cookies.get("KOASESSID")).id;
    const shortNames = countryConstants.getShortNames();
    const filter = this.params.filter.split('.');

    this.body = yield _getDiscussions(userID, shortNames, filter);

    // if it's the main list, clear notification counter
    if (filter[0] == 'recent') {
        yield notificationsController.resetCounter(activityConstants.DISCUSSION_POST, userID);
    }
})

.get('/replies/:uuid/:timestamp', auth.isJournalist, function *(next) {

    const userID = auth.getSession(this.cookies.get("KOASESSID")).id;
    // const discussionID = {'~label':'discussion', uuid:this.params.uuid, timestamp:this.params.timestamp};

    this.body = yield _getDiscussionReplies(userID, this.params.uuid);
})

.get('/reply.replies/:uuid/:timestamp/:user_id', auth.isJournalist, function *(next) {

    const userID = auth.getSession(this.cookies.get("KOASESSID")).id;
    const replyID = {'~label':'reply', uuid:this.params.uuid, timestamp:this.params.timestamp, user_id:this.params.user_id};

    this.body = yield _getReplyReplies(userID, replyID);
})

.get('/detail/:uuid/:timestamp', auth.isJournalist, function *(next) {

    const userID = auth.getSession(this.cookies.get("KOASESSID")).id;
    // const vertexID = {'~label':'discussion', uuid:this.params.uuid, timestamp:this.params.timestamp};
    const discussionUUID = this.params.uuid;
    const params = {
        'userID':userID,
        'discussionUUID':discussionUUID
    };
    var query = 'g.V().hasLabel("discussion").has("uuid", discussionUUID).as("id","uuid","title","timestamp","text","location","pinned", "replies","votesUp","votesDown","isVotingUp","isVotingDown","tags")' +
                '.in("posted").as("user_id","name","picture","user_location")' +
                '.select("id","uuid","title","timestamp","text","location","pinned","replies","votesUp","votesDown","isVotingUp","isVotingDown","tags","user_id","name","picture","user_location")' +
                '.by(id()).by("uuid").by("title").by("timestamp").by("text").by("location")' +
                '.by( inE("pins").outV().has("user_id",userID).count() )' +
                '.by( inE("replies").count() )' +
                '.by( inE("votes").values("up").sum() )' +
                '.by( inE("votes").values("down").sum() )' +
                '.by( inE("votes").has("up").outV().has("user_id",userID).count() )' +
                '.by( inE("votes").has("down").outV().has("user_id",userID).count() )' +
                '.by( outE("focus").inV().group().by("name").by("name") )' +
                '.by("user_id").by("name")' +
                '.by( coalesce( has("picture").values("picture"), constant("") ) )' +
                '.by( coalesce( has("location").values("location"), constant("") ) )';
                '.by("location")';

    const result = yield function(graphCallback) {
        graphClient.executeGraph(query, params, graphCallback);
    }
    // TODO: antes de marcar como leida, verificar que no es de mi propiedad
    _saveEdgeRead(userID, discussionUUID);

    this.body = result.first();
})

.post('/', auth.isJournalist, jsonBody, function *(next) {

    const userID = auth.getSession(this.cookies.get("KOASESSID")).id;
    const tagNames = this.request.body.tags;
    var vertexID = this.request.body.id; // Partition Key

    var objModel = {
        user_id: userID,
        name: this.request.body.name,
        title: this.request.body.title,
        text: this.request.body.text
        // location: this.request.body.address
    };

    // If there is location object, add it to the model
    if(this.request.body.location){
        objModel.country_short = this.request.body.location.country_short;
        objModel.adm_1_short = this.request.body.location.adm_level_1_short;
        objModel.locality_short = this.request.body.location.locality_short;
        objModel.location = this.request.body.address;
        // objModel.lat = this.request.body.location.lat;
        // objModel.lng = this.request.body.location.lng;
    }

    if(vertexID){

        yield _updateDiscussion(vertexID, objModel);
        objModel.uuid = vertexID.uuid;
        objModel.timestamp = vertexID.timestamp;

    }else{

        objModel.uuid = models.uuid();
        objModel.timestamp = Number(new Date());
        vertexID = yield _insertDiscussion(objModel);
    }
    objModel.id = vertexID;

    var deletedEdges = new Array();
    var deletedTags = new Array();
    var isNewTag;

    if(vertexID){
        deletedEdges = yield _deleteEdgeFocus(vertexID);
    }

    // Copio los tags que tenía la discussion en un array para manipularlos mejor
    deletedEdges.forEach(function(edge, index){
        deletedTags.push(edge.inV.name);
    });

    // Recorrer los tags de la discussion
    // Identificar los nuevos tags para crear nuevas recomendaciones
    for(var i=0; i<tagNames.length; i++){

        yield tagsController.saveTag(tagNames[i]);
        yield tagsController.saveEdgeHas(userID, tagNames[i], tagConstants.TYPE_DISCUSSION_POST);
        _saveEdgeFocus(vertexID, tagNames[i]);

        // Identificar los nuevos tags
        isNewTag = true;
        var indexToDelete;
        deletedTags.forEach(function(edge, index){
            if(tagNames[i] == deletedTags[index]){
                isNewTag = false;
                indexToDelete = index;
            }
        });

        deletedTags.splice(indexToDelete, 1);

        // Crear nuevas recomendaciones
        if(isNewTag){
            console.log('**** NUEVO TAG --> crar nuevo match con: ' + tagNames[i]);
            yield matchesExpert.matchDiscussion(tagNames[i], objModel, userID);
        }else{
            console.log('**** TAG EXISTENTE --> no hacer nada con: ' + tagNames[i]);
        }
    }

    // eliminar las recomendaciones de los edges que fueron borrados permanentemente
    var removeMatch = true;
    for(i = 0; i < deletedTags.length; i++) {
        console.log('**** TAG ELIMINADO PERMANENTEMENTE --> quitar recomendacion de: ' + deletedTags[i]);
        yield matchesExpert.matchDiscussion(deletedTags[i], objModel, userID, removeMatch);
    }

    this.body = objModel;
})

/*
 * Reply the main discussion post
 */
.post('/reply', auth.isJournalist, jsonBody, function *(next) {

    const userID = auth.getSession(this.cookies.get("KOASESSID")).id;
    var objModel = {
        uuid: this.request.body.parent_id.uuid,   // Partition Key
        timestamp: Number(new Date()),  // clustering key
        user_id: userID,                // clustering key
        name: this.request.body.name,
        text: this.request.body.text,
    };

    // replace \n by <br> to avoid dse graph driver error while parsing \n in the string
    objModel.text = objModel.text.replace(/\n/g, "<br/>");

    var result = yield _insertReply(objModel, 'discussion');

    // Insert EDGE
    const commentID = result.id;
    _insertRepliesEdge(this.request.body.parent_id, commentID);

    this.body = result;
})

/*
 * Reply a second or further level in a discussion post
 */
.post('/reply.toreply', auth.isJournalist, jsonBody, function *(next) {

    const userID = auth.getSession(this.cookies.get("KOASESSID")).id;

    var objModel = {
        uuid: this.request.body.parent_id.uuid, // Partition Key
        timestamp: Number(new Date()),
        user_id: userID,
        name: this.request.body.name,
        text: this.request.body.text,
    };

    // replace \n by <br> to avoid dse graph driver error while parsing \n in the string
    objModel.text = objModel.text.replace(/\n/g, "<br/>");

    var result = yield _insertReply(objModel, 'reply');

    // Insert EDGE
    const commentID = result.id;
    _insertRepliesEdge(this.request.body.parent_id, commentID);

    this.body = result;
})

/*
 * Pin a discussion post
 */
.post('/pin', auth.isJournalist, jsonBody, function *(next){

    const userID = auth.getSession(this.cookies.get("KOASESSID")).id;
    const vertexID = this.request.body.id;

    const query = 'g.V().hasId("{~label=journalist, user_id='+userID+'}").as("a")'+
                  '.V().hasId(vertexID).addE("pins").from("a")'+
                  '.property("timestamp", timestamp)';

    const params = {
        "vertexID": vertexID,
        "timestamp": Number(new Date())
    };

    yield function(graphCallback) {
        graphClient.executeGraph(query, params, graphCallback);
    }
    this.body = 'Done';
})

.post('/unpin', auth.isJournalist, jsonBody, function *(next){

    const userID = auth.getSession(this.cookies.get("KOASESSID")).id;
    const vertexID = this.request.body.id;

    const query = 'g.V().hasId("{~label=journalist, user_id='+userID+'}")'+
                  '.outE("pins").as("e").inV().hasId(vertexID).select("e").drop()';
    const params = { "vertexID": vertexID };

    yield function(graphCallback) {
        graphClient.executeGraph(query, params, graphCallback);
    }
    this.body = 'Done';
})

/*
 * Vote a discussion or a reply
 */
.post('/voteup', auth.isJournalist, jsonBody, function *(next){

    const userID = auth.getSession(this.cookies.get("KOASESSID")).id;
    const vertexID = this.request.body.id;
    const voteUp = true;

    yield _deletePreviousVote(userID, vertexID);
    yield _vote(userID, vertexID, voteUp);

    this.body = 'Done';
})

/*
 * Vote a discussion or a reply
 */
.post('/votedown', auth.isJournalist, jsonBody, function *(next){

    const userID = auth.getSession(this.cookies.get("KOASESSID")).id;
    const vertexID = this.request.body.id;
    const voteUp = false;

    var result = yield _deletePreviousVote(userID, vertexID);
    var results = yield _vote(userID, vertexID, voteUp);

    this.body = 'Done';
});


module.exports = router;


/*****************************************************************************************/
// Exports
/*****************************************************************************************/


module.exports.getDiscussion = function *(vertexID){

    let query = 'g.V().hasId(vertexID)';

    query +=    '.as("uuid","title","timestamp","location","replies","votesUp","votesDown")' +
                '.in("posted")' +
                '.as("user_id","name","picture","user_location")' +
                '.select("uuid","title","timestamp","location","replies","votesUp","votesDown","user_id","name","picture","user_location")' +
                '.by("uuid").by("title").by("timestamp").by("location")' +
                '.by( inE("replies").count() )' +
                '.by( inE("votes").values("up").sum() )' +
                '.by( inE("votes").values("down").sum() )' +
                '.by("user_id")' +
                '.by("name")' +
                '.by( coalesce( has("picture").values("picture"), constant("") ) )' +
                '.by( coalesce( has("location").values("location"), constant("") ) )';

    const params = {"vertexID": vertexID};

    var result = yield function(graphCallback) {
        graphClient.executeGraph(query, params, graphCallback);
    }
    return result.first();
}

module.exports.getDiscussionsByCountry = function *(userID, countryShortName, limit){
    if (!countryShortName instanceof Array){
        countryShortName = [countryShortName];
    }
    return yield _getDiscussions(userID, countryShortName, ['recent'], limit);
}

/*****************************************************************************************/
// Funciones privadas
/*****************************************************************************************/


function * _getDiscussions(userID, countryShortNames, filterParam, limitResult){

    const limit = limitResult || 1000;
    const params = { 'userID':userID, 'shortNames':countryShortNames, 'limitResult': limit};

    // var query = 'g.V().hasLabel("discussion").has("country_short", within(["AR", "US"]))';
    var query = 'g.V().hasLabel("discussion").has("country_short", within(shortNames))';

    switch(filterParam[0]){
        case 'recent':
            //
        break;

        case 'pinned':
            query += '.match(__.as("d").inE("pins").outV().has("user_id",'+userID+')).select("d")';
        break;

        case 'following':
            query += '.match(__.as("d").inE("posted").outV().inE("follows").outV().has("user_id",'+userID+') ).select("d")';
        break;

        case 'mine':
            query += '.match(__.as("d").inE("posted").outV().has("user_id",'+userID+') ).select("d")';
        break;

        case 'tag':
            query += '.match(__.as("d").outE("focus").inV().has("name", "'+filterParam[1]+'") ).select("d")';
        break;

        case 'search':
            // TODO: verificar por que esto esta dando error.
            // LAS BUSQUEDAS DE DISCUSSION ESTAN DESHABILITADAS HASTA RESOLVER ESTO.
            //g.V().hasLabel("discussion").has("title", Search.tokenRegex(".*what.*") ).match(__.as("d").has("title", Search.tokenRegex(".*'+filterParam[1]+'.*") ) ).select("d")
            // query = '.match(__.as("d").has("title", Search.tokenRegex(".*'+filterParam[1]+'.*") ) ).select("d")';
        break;
    }

    query +=    '.as("id","uuid","title","timestamp","location","mine","replies","votesUp","votesDown", "isRead", "tags")' +
                '.in("posted")' +
                '.as("user_id","name","picture","user_location")' +
                '.select("id","uuid","title","timestamp","location","mine","replies","votesUp","votesDown","isRead","tags","user_id","name","picture","user_location")' +
                '.by(id()).by("uuid").by("title").by("timestamp").by("location")' +
                '.by( inE("posted").outV().has("user_id",userID).count() )' +
                '.by( repeat(__.in("replies")).emit().path().count() )' +
                '.by( inE("votes").values("up").sum() )' +
                '.by( inE("votes").values("down").sum() )' +
                '.by( inE("read").outV().has("user_id", userID).count() )' +
                '.by( outE("focus").inV().group().by("name").by("name") )' +
                '.by("user_id")' +
                '.by("name")' +
                '.by( coalesce( has("picture").values("picture"), constant("") ) )' +
                '.by( coalesce( has("location").values("location"), constant("") ) ).limit(limitResult)';

    const result = yield function(graphCallback) {
        graphClient.executeGraph(query, params, graphCallback);
    }

    return result.toArray();
}

function * _insertDiscussion(modelProperties){

    // replace \n by <br> to avoid dse graph driver error while parsing \n in the string
    modelProperties.title = modelProperties.title.replace(/\n/g, "<br/>");
    modelProperties.text = modelProperties.text.replace(/\n/g, "<br/>");

    const insertQuery = graphHelper.insertQuery(modelProperties, 'discussion');

    const result = yield function(graphCallback) {
        graphClient.executeGraph(insertQuery, graphCallback);
    }
    const vertexID = result.first().id;
    const queryEdge = 'g.V().hasId("{~label=journalist, user_id='+modelProperties.user_id+'}").as("j").V(vertexID).addE("posted").from("j")';

    var params = {"vertexID":vertexID};

    // no hace falta esperar el callback
    graphClient.executeGraph(queryEdge, params, function(err, result){
        if (err) throw Error('Error guardando edge posted - ' + err);
    });

    return vertexID;
}

/*
 * Update every vertex property.
 */
function * _updateDiscussion(vertexID, objModel){

    var params = {'vertexID':vertexID};
    var query = 'g.V().hasId(vertexID)';

    query = graphHelper.updatePropertiesQuery(query, objModel);

    yield function(graphCallback) {
        graphClient.executeGraph(query, params, graphCallback);
    }
};

function _saveEdgeRead(userID, uuID){

    const query = 'g.V().hasId("{~label=journalist, user_id='+userID+'}").as("j")'+
                  '.V().hasLabel("discussion")has("uuid", uuID).addE("read").from("j")'+
                  '.property("timestamp", timestamp)';

    const params = {"uuID": uuID, "timestamp": Number(new Date())};

    graphClient.executeGraph(query, params, function(err, result){
        if (err) {
            console.log(err);
        }
    });
}

/*
 *  Save the relationhsip between a discussion and a tag
 */
function _saveEdgeFocus(vertexID, tagName){

    const query = 'g.V().hasId("{~label=discussion, uuid='+vertexID.uuid+', timestamp='+vertexID.timestamp+'}").as("a").V().has("tag", "name", tagName).addE("focus").from("a")';
    const params = {"tagName": tagName};
    graphClient.executeGraph(query, params, function(err, result){
        if(err) console.log('Error guardando discussion FOCUS - ' + err);
    });
}

function * _deleteEdgeFocus(discussionID){

    const params = {'discussionID':discussionID};
    var query = 'g.V().hasId(discussionID).outE("focus")';
    var edgesToDelete = yield function(graphCallback) {
        graphClient.executeGraph(query, params, graphCallback);
    }

    query = 'g.V().hasId(discussionID).outE("focus").drop()';
    graphClient.executeGraph(query, params, function(err, result){
        if (err) {
            console.log(err);
        }
    });

    return edgesToDelete;
}

/*
 * Creates a "votes" relation between the journalist and the discussion/reply Vertex
 */
function * _vote(userID, vertexID, voteUp){

    const params = {
        "vertexID": vertexID,
        "timestamp": Number(new Date())
    };

    // Guardar voto
    var query = 'g.V().hasId("{~label=journalist, user_id='+userID+'}").as("a")'+
            '.V().hasId(vertexID).addE("votes").from("a")';

    if (voteUp) {
        query += '.property("up", 1).property("timestamp", timestamp)';
    }else{
        query += '.property("down", 1).property("timestamp", timestamp)';
    }

    return yield function(graphCallback) {
        graphClient.executeGraph(query, params, graphCallback);
    }
}

function * _deletePreviousVote(userID, vertexID){

    const params = {'vertexID':vertexID};
    const query = 'g.V().hasId("{~label=journalist, user_id='+userID+'}").outE("votes").as("e").inV().hasId(vertexID).select("e").drop()';

    return yield function(graphCallback) {
        graphClient.executeGraph(query, params, graphCallback);
    }
}


function * _getDiscussionReplies(userID, uuID){

    const params = { 'uuID':uuID, 'userID':userID };
    const query = 'g.V().hasLabel("discussion").has("uuid", uuID)' +
                  '.in("replies").as("id","text", "name","replies","votesUp","votesDown","isVotingUp","isVotingDown")' +
                  '.select("id","text","name","replies","votesUp","votesDown","isVotingUp","isVotingDown")' +
                  '.by(id()).by("text").by("name")' +
                  '.by( inE("replies").count() )' +
                  '.by( inE("votes").values("up").sum() )' +
                  '.by( inE("votes").values("down").sum() )' +
                  '.by( inE("votes").has("up").outV().has("user_id",userID).count() )' +
                  '.by( inE("votes").has("down").outV().has("user_id",userID).count() )'

    const result = yield function(graphCallback) {
      graphClient.executeGraph(query, params, graphCallback);
    }
    return result.toArray();
}

/*
 *  Get replies from another parent reply
 */
function * _getReplyReplies(userID, vertexID){

    const params = { 'vertexID':vertexID, 'userID':userID };
    const query = 'g.V().hasId(vertexID)' +
                  '.in("replies").as("id","text", "name","replies","votesUp","votesDown","isVotingUp","isVotingDown")' +
                  '.select("id","text","name","replies","votesUp","votesDown","isVotingUp","isVotingDown")' +
                  '.by(id()).by("text").by("name")' +
                  '.by( inE("replies").count() )' +
                  '.by( inE("votes").values("up").sum() )' +
                  '.by( inE("votes").values("down").sum() )' +
                  '.by( inE("votes").has("up").outV().has("user_id",userID).count() )' +
                  '.by( inE("votes").has("down").outV().has("user_id",userID).count() )'

    const result = yield function(graphCallback) {
      graphClient.executeGraph(query, params, graphCallback);
    }
    return result.toArray();
}

function * _insertReply(modelProperties, toVertexLabel){

    // Insert VERTEX
    const insertQuery = graphHelper.insertQuery(modelProperties, 'reply');

    const result = yield function(graphCallback) {
        graphClient.executeGraph(insertQuery, graphCallback);
    }

    // Devolver el nuevo reply
    const objResponse = result.first();
    return {
        id: objResponse.id,
        label:objResponse.label,
        name: objResponse.properties.name[0].value,
        text: objResponse.properties.text[0].value
    };
}

/*
 * Insert replies edge from a reply vertex to a discussion / reply vertex
 */
function _insertRepliesEdge(toVertexID, commentID){

    const queryEdge = 'g.V(commentID).as("c").V().hasId(toVertexID).addE("replies").from("c")';
    const params = {
        "toVertexID":toVertexID,
        "commentID":commentID
    };

    // no hace falta esperar el callback
    graphClient.executeGraph(queryEdge, params, function(err, result){
        if (err) throw Error('Error guardando edge replies - ' + err);
    });

}
