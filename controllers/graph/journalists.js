"use strict";
const Router = require('koa-router');
const assert = require('assert');
const auth = require('../sessionControl');
const graphConnections = require('./graphConnections');
const jsonBody = require('koa-json-body')();
const constants = require('../../constants/tagConstants');
const activityConstants = require('../../constants/activityConstants');
const mandrillExpert = require('../../experts/mandrill');
const imagesController = require('../images');

const activities = require('../activities');



const router = new Router({
  prefix: '/journalists'
});

const client = graphConnections.getClient();

var graphCallback = function(err, result){
    assert.ifError(err);
    return result;
}

router.get('/', function *(next) {
    this.body = 'estoy adentro de vertices !';
})

.get('/is.admin', auth.isAdmin, function *(next) {
    this.body = 'Done';
})

.get('/get.info/:user_id', auth.isJournalist, function *(next) {

    const user_id = this.params.user_id;

    var result = yield function(graphCallback) {
        const query = 'g.V().hasId("{~label=journalist, user_id='+user_id+'}").valueMap("user_id", "name", "picture", "position", "location")';
        client.executeGraph(query, graphCallback);
    };

    this.body = result.toArray();
})

.get('/get.info.users/:users', auth.isJournalist, function *(next) {

    if(!this.params.users){
        this.body = [];
        return;
    }
    const arrayUsers = this.params.users.split(',');

    if(!arrayUsers || arrayUsers.length < 1){
        this.body = [];
        return;
    }

    const params = {'arrayUsers':arrayUsers};
    const query = 'g.V().hasLabel("journalist").has("user_id", within(arrayUsers)).valueMap("user_id", "name", "picture", "position", "location")';
    const result = yield function(graphCallback) {
        client.executeGraph(query, params, graphCallback);
    };

    this.body = result.toArray();
})

.post('/match/', auth.isJournalist, jsonBody, function *(next) {

    const tags = this.request.body.tags;
    const country = this.request.body.country;
    const experience = this.request.body.experience;
    const userID = auth.getSession(this.cookies.get("KOASESSID")).id;
    // const PAGE_SIZE = 10;
    // const currentPage = this.request.body.page;
    let searchValue = this.request.body.searchValue;

    // Search using the search bar
    if(searchValue && searchValue.length >= 1){
        searchValue = searchValue.toLowerCase();
        var query = 'g.V().hasLabel("journalist").has("name", Search.tokenRegex(".*'+searchValue+'.*")).match( ';
    }else{
        var query = 'g.V().hasLabel("journalist").match( ';
    }

    // Is not me, has location, offers and requests
    query += '__.as("j").has("user_id", neq('+userID+')) ';
    query += ', __.as("j").hasNot("timestamp_deactivated") ';
    query += ', __.as("j").has("location").has("location", neq("")) ';
    query += ', __.as("j").has("offers").has("offers", neq("")) ';
    query += ', __.as("j").has("requests").has("requests", neq("")) ';

    // Categories OFFERS
    if(tags)
    tags.forEach(function(item, index){
        query += ', __.as("j").out("has", "type", "TYPE_OFFER").has("name", "'+ item.label +'").as("t")';
    });

    // Country
    if(country){
        query += ', __.as("j").out("operates").has("country_short", "'+ country +'").as("c")';
    }

    // Skills-Beats-Focus
    if(experience){
        experience.forEach(function(item, index){
            query += ', __.as("j").outE("has").has("type", within("TYPE_SKILL", "TYPE_BEAT", "TYPE_FOCUS")).inV().has("name", "'+item+'").as("e")';
        });
    }

    query += ').select("j").dedup().valueMap("user_id", "name", "picture", "location", "position", "offers", "requests")';

    // PAGINATION
    // query += '.range('+(currentPage*(currentPage-1))+', '+  ((PAGE_SIZE*currentPage)) +')';

    var results = yield function(graphCallback) {
        client.executeGraph(query, graphCallback);
    }

    if(!results || results.length === 0){
        this.body = [];
        return;
    }

    this.body = JSON.stringify(results.toArray());
})

.get('/get.email', auth.isJournalist, function *(next) {

    const currentUserID = auth.getSession(this.cookies.get("KOASESSID")).id;

    var result = yield function(graphCallback) {
        const query = 'g.V().hasId("{~label=journalist, user_id='+currentUserID+'}").values("email")';
        client.executeGraph(query, graphCallback);
    };

    if(result.length > 0){
        this.body = result.toArray();
    }else{
        this.body = ['email not found'];
    }
})

.get('/get.new.ones/:country', auth.isJournalist, function *(next) {

    let date = new Date();
    date.setHours(date.getHours() - 24);
    date = Number(date);

    const currentUserID = auth.getSession(this.cookies.get("KOASESSID")).id;
    const country = this.params.country;

    var result = yield function(graphCallback) {
        const params = {'country':country, 'date': date, 'currentUserID':currentUserID};
        const query = 'g.V().hasLabel("journalist").has("country_short", country).has("timestamp_email_verified", gt(date)).has("user_id", neq(currentUserID)).valueMap(true)';
        client.executeGraph(query, params, graphCallback);
    };

    this.body = result.toArray();
})

.get('/search.engaged/:searchName', auth.isJournalist, function *(next) {

    const userID = auth.getSession(this.cookies.get("KOASESSID")).id;
    var searchName = this.params.searchName;

    if(!searchName){
        this.body = [];
        return;
    }
    searchName = searchName.toLowerCase();

    const query = 'g.V().hasId("{~label=journalist, user_id='+userID+'}").bothE("engaged").has("timestamp_accepted").otherV().has("name", Search.tokenRegex(".*'+searchName+'.*")).valueMap("user_id", "name", "picture", "location") ';

    var result = yield function(graphCallback) {
        client.executeGraph(query, graphCallback);
    }

    this.body = result.toArray();
})

.get('/count.engagement', auth.isJournalist, function *(next) {

    const currentUserID = auth.getSession(this.cookies.get("KOASESSID")).id;

    var result = yield function(graphCallback) {
        const query = 'g.V().hasId("{~label=journalist, user_id='+currentUserID+'}").bothE("engaged").has("timestamp_accepted").count()';
        client.executeGraph(query, graphCallback);
    }

    this.body = result.first();
})

.get('/get.by.country/:country_short', auth.isJournalist, function *(next) {

    const currentUserID = auth.getSession(this.cookies.get("KOASESSID")).id;
    const countryShort = this.params.country_short

    var result = yield function(graphCallback) {
        const params = {'countryShort': countryShort, 'currentUserID':currentUserID};
        const query = 'g.V().hasLabel("journalist").has("country_short", eq(countryShort)).has("user_id", neq(currentUserID)).valueMap("user_id", "name", "picture", "location", "position", "offers", "requests")';
        client.executeGraph(query, params, graphCallback);
    }

    this.body = result.toArray();
})

.get('/get.followers/:user_id', auth.isJournalist, function *(next) {

    var currentUserID;

    if(this.params.user_id == parseInt(this.params.user_id) && this.params.user_id != 0){
        currentUserID = parseInt(this.params.user_id);
    }else{
        currentUserID = auth.getSession(this.cookies.get("KOASESSID")).id;
    }

    var result = yield function(graphCallback) {
        const query = 'g.V().hasId("{~label=journalist, user_id='+currentUserID+'}").in("follows").valueMap("user_id", "name", "picture", "position", "location")';
        client.executeGraph(query, graphCallback);
    }

    this.body = result.toArray();
})

.get('/get.following/:user_id', auth.isJournalist, function *(next) {

    var currentUserID;

    if(this.params.user_id == parseInt(this.params.user_id) && this.params.user_id != 0){
        currentUserID = parseInt(this.params.user_id);
    }else{
        currentUserID = auth.getSession(this.cookies.get("KOASESSID")).id;
    }

    var result = yield function(graphCallback) {
        const query = 'g.V().hasId("{~label=journalist, user_id='+currentUserID+'}").out("follows").valueMap("user_id", "name", "picture", "position", "location")';
        client.executeGraph(query, graphCallback);
    }

    this.body = result.toArray();
})

.get('/isfollowing/:user_id', auth.isJournalist, function *(next) {

    const targetUserID = this.params.user_id;
    const currentUserID = auth.getSession(this.cookies.get("KOASESSID")).id;

    if(targetUserID != parseInt(targetUserID) || currentUserID == targetUserID ){
        this.body = 'Invalid parameters';
        return;
    }

    var result = yield function(graphCallback) {
        const query = 'g.V().hasId("{~label=journalist, user_id='+currentUserID+'}").outE("follows").as("e").inV().hasId("{~label=journalist, user_id='+targetUserID+'}").select("e")';
        // const query = 'g.V().hasId("{~label=journalist, user_id='+currentUserID+'}").outE("follows").has("in_vertex", g.V().hasId("{~label=journalist, user_id='+targetUserID+'}"))';
        client.executeGraph(query, graphCallback);
    }


    if(result.length > 0){
        this.body = {'following':true};
    }else{
        this.body = {'following':false};
    }
})

.post('/follow', auth.isJournalist, jsonBody, function *(next) {

    const userToFollow = this.request.body.user_id;
    const currentUserID = auth.getSession(this.cookies.get("KOASESSID")).id;
    const timestamp = Date.now();

    if(userToFollow != parseInt(userToFollow) || currentUserID == userToFollow ){
        this.body = 'Invalid parameters';
        return;
    }

    var result = yield function(graphCallback) {
        const query = 'g.V().hasId("{~label=journalist, user_id='+currentUserID+'}").as("a").V().hasId("{~label=journalist, user_id='+userToFollow+'}").addE("follows").from("a").property("timestamp", '+timestamp+')';
        client.executeGraph(query, graphCallback);
    }

    this.body = ['Following'];
})

.post('/unfollow', auth.isJournalist, jsonBody, function *(next) {

    const userToUnfollow = this.request.body.user_id;
    const currentUserID = auth.getSession(this.cookies.get("KOASESSID")).id;

    if(userToUnfollow != parseInt(userToUnfollow) || currentUserID == userToUnfollow ){
        this.body = 'Invalid parameters';
        return;
    }

    var result = yield function(graphCallback) {
        const query = 'g.V().hasId("{~label=journalist, user_id='+currentUserID+'}").outE("follows").as("e").inV().hasId("{~label=journalist, user_id='+userToUnfollow+'}").select("e").drop()';
        client.executeGraph(query, graphCallback);
    }

    this.body = ['Unfollowed'];
})

/*
 * Get Offers / Requests - Used in the collaborate list
 */
.get('/get.tags/:user_id/:type', auth.isJournalist, function *(next) {

    if(parseInt(this.params.user_id) != this.params.user_id || parseInt(this.params.user_id) <= 0
        || ( this.params.type != constants.TYPE_OFFER && this.params.type != constants.TYPE_REQUEST) ){
        this.body = 'Invalid params';
        return;
    }


    var userID = this.params.user_id;
    var params = {"userID": userID, "type": this.params.type};
    var results = yield function(graphCallback) {
        client.executeGraph('g.V().has("journalist", "user_id", userID).out("has", "type", type).dedup().values("name")', params, graphCallback);
    }

    this.body = JSON.stringify(results.toArray());
})

.post('/engage', auth.isJournalist, jsonBody, function *(next) {

    //Validate parameters
    var currentUserID = auth.getSession(this.cookies.get("KOASESSID")).id;
    if(this.request.body.targetUser != parseInt(this.request.body.targetUser)
        || currentUserID == this.request.body.targetUser ){

        this.body = 'Invalid parameters';
        return;
    }

    var timestamp = Date.now();
    var targetUserID = parseInt(this.request.body.targetUser);

    var result = yield function(graphCallback) {
        const query = 'g.V().has("journalist", "user_id", currentUserID).as("a").V().has("journalist", "user_id", targetUserID).addE("engaged").from("a").property("timestamp", timestamp)';
        client.executeGraph(query, { "currentUserID":currentUserID, "targetUserID":targetUserID, "timestamp":timestamp }, graphCallback);
    }
    this.body = 'Done';
})

// .get('/path.count/:from/:to', function *(next) {
//
//     var userFrom = parseInt(this.params.from);
//     var userTo = parseInt(this.params.to);
//
//     var result = yield function(graphCallback) {
//         var query = 'g.V().has("journalist", "user_id", '+userFrom+').repeat(out().simplePath()).until(has("journalist", "user_id", '+userTo+')).path().count(local);';
//         client.executeGraph(query, graphCallback);
//     }
//     this.body = result.toArray();
// })
/*
 * Returns the journalist in between two users if they are not engaged.
 * If the two journalits are angaged, there is no result
 * If there are more than one journalists in between  in the shortest path. There is no result.
 */
.get('/common.connections/:to', auth.isJournalist, function *(next) {

    var userFrom = auth.getSession(this.cookies.get("KOASESSID")).id;
    var userTo = parseInt(this.params.to);

    var result = yield function(graphCallback) {

        //var query = 'g.V().has("journalist", "user_id", '+userFrom+').repeat(out().simplePath()).until(has("journalist", "user_id", '+userTo+')).path().count(local);';
        //var query = 'g.V().has("journalist", "user_id", '+userFrom+').repeat(out().simplePath()).until(has("journalist", "user_id", '+userTo+')).path().limit(1);';
        //g.V().has('name','Julia Child').repeat(out()).times(2).valueMap()
        //g.V().has("journalist", "user_id", 395).repeat(out().simplePath()).until(has("journalist", "user_id", 396)).path()
        // g.V().has("journalist","user_id",352).repeat( both("engaged").simplePath().timeLimit(200) ).until(has("journalist","user_id",401)).path()
        // g.V().has("journalist","user_id",352).repeat( both("engaged").simplePath().limit(3) ).until(has("journalist","user_id",395)).path()

        // g.V().has("journalist","user_id",352).
        //   repeat(both("engaged").simplePath().timeLimit(200)).
        //   until(has("journalist","user_id",401)).
        //   path().by(choose(hasLabel("person"),
        //             values("name"),values("title"))).limit(1)

// g.V().has("journalist","user_id",352).both("engaged").as("middle").both("engaged").has("user_id", 401).select("middle").valueMap("user_id", "name", "picture", "position", "location").limit(100)
// g.V().has("journalist","user_id",352).both("engaged").as("middle").both("engaged").has("user_id", 401).select("middle").count()

        var params = {"userTo": userTo};
        // var query = 'g.V().has("journalist","user_id", userFrom).both("engaged").as("middle").both("engaged").has("user_id", userTo).select("middle").valueMap("user_id", "name", "picture", "position", "location").limit(100)';
        var query = 'g.V().hasId("{~label=journalist, user_id='+userFrom+'}").both("engaged").as("middle").both("engaged").has("user_id", userTo).select("middle").valueMap("user_id", "name", "picture", "position", "location").limit(100)';

        if(auth.isAdminRole){
          query = 'g.V().hasLabel("journalist").hasNot("timestamp_deactivated").valueMap("user_id", "name", "picture", "position", "location")';
        }

        client.executeGraph(query, params, graphCallback);
    }

    this.body = JSON.stringify(result.toArray());
})

.get('/connections', auth.isJournalist, function *(next) {

    var result = yield function(graphCallback) {
        var query = 'g.V().hasLabel("journalist").hasNot("timestamp_deactivated").valueMap("user_id", "name", "picture", "position", "location")';
        client.executeGraph(query, graphCallback);
    }

    this.body = JSON.stringify(result.toArray());
})

.get('/count.common.connections/:to', auth.isJournalist, function *(next) {
    var userFrom = auth.getSession(this.cookies.get("KOASESSID")).id;
    var userTo = parseInt(this.params.to);

    var result = yield function(graphCallback) {
        var params = {"userTo": userTo};
        var query = 'g.V().hasId("{~label=journalist, user_id='+userFrom+'}").both("engaged").as("middle").both("engaged").has("user_id", userTo).select("middle").count()';
        client.executeGraph(query, params, graphCallback);
    }
    this.body = JSON.stringify(result.first());
})

.post('/save.picture', auth.isJournalist, jsonBody, function *(next) {

    const userID = auth.getSession(this.cookies.get("KOASESSID")).id;
    const fileName = userID + '.jpg';
    const base64Str = this.request.body.image;

    let imageInfo = imagesController.saveProfilePicture(base64Str, fileName);

    if(!imageInfo || !imageInfo.fileName){
        this.status = 500;
        this.body = 'Error while saving file - ' + imageInfo;
        return;
    }

    const params = {"picture":imageInfo.fileName};
    const query = 'g.V().hasId("{~label=journalist, user_id='+userID+'}").property("picture", picture).iterate()';
    client.executeGraph(query, params, graphCallback);

    this.body = imageInfo;
})

.post('/save.cover', auth.isJournalist, jsonBody, function *(next) {

    const cover = this.request.body.cover.trim();
    const userID = auth.getSession(this.cookies.get("KOASESSID")).id;

    const fileName = userID + '_cover.jpg';

    let imageInfo = imagesController.saveProfilePicture(cover, fileName);

    if(!cover){
        this.body = 'No params';
        return;
    }

    const params = {"cover":imageInfo.fileName};
    const query = 'g.V().hasId("{~label=journalist, user_id='+userID+'}").property("cover", cover).iterate()';
    client.executeGraph(query, params, graphCallback);

    this.body = 'Done';
})

.post('/save.bio', auth.isJournalist, jsonBody, function *(next) {

    var bio = this.request.body.bio || '';
    var userID = auth.getSession(this.cookies.get("KOASESSID")).id;

    const params = {"bio":bio};
    const query = 'g.V().hasId("{~label=journalist, user_id='+userID+'}").property("bio", bio).iterate()';
    yield function(graphCallback) {
        client.executeGraph(query, params, graphCallback);
    }

    this.body = 'Done';
})

.post('/save.quote', auth.isJournalist, jsonBody, function *(next) {

    var quote = this.request.body.quote;
    var userID = auth.getSession(this.cookies.get("KOASESSID")).id;

    if(!quote){
        quote = '';
    }
    quote = quote.trim();

    var result = yield function(graphCallback) {
        const params = {"quote":quote};
        const query = 'g.V().hasId("{~label=journalist, user_id='+userID+'}").property("quote", quote).iterate()';
        client.executeGraph(query, params, graphCallback);
    }
    this.body = 'Done';
})

.post('/save.profile.complete', auth.isJournalist, function *(next) {

    var userID = auth.getSession(this.cookies.get("KOASESSID")).id;

    var result = yield function(graphCallback) {
        const params = {"profile_completed":true};
        const query = 'g.V().hasId("{~label=journalist, user_id='+userID+'}").property("profile_completed", profile_completed).iterate()';
        client.executeGraph(query, params, graphCallback);
    }
    this.body = 'Done';
})

.post('/dashboard.profile.read', auth.isJournalist, function *(next) {

    var userID = auth.getSession(this.cookies.get("KOASESSID")).id;

    var result = yield function(graphCallback) {
        const params = {"dashboard_profile_read":true};
        const query = 'g.V().hasId("{~label=journalist, user_id='+userID+'}").property("dashboard_profile_read", dashboard_profile_read).iterate()';
        client.executeGraph(query, params, graphCallback);
    }
    this.body = 'Done';
})

.post('/dashboard.community.read', auth.isJournalist, function *(next) {

    var userID = auth.getSession(this.cookies.get("KOASESSID")).id;

    var result = yield function(graphCallback) {
        const params = {"dashboard_community_read":true};
        const query = 'g.V().hasId("{~label=journalist, user_id='+userID+'}").property("dashboard_community_read", dashboard_community_read).iterate()';
        client.executeGraph(query, params, graphCallback);
    }
    this.body = 'Done';
})

.post('/save.location', auth.isReader, jsonBody, function *(next) {

    var objLocation = this.request.body.location;
    var address = this.request.body.address;
    var userID = auth.getSession(this.cookies.get("KOASESSID")).id;
    var timestamp = Date.now();

    if( !objLocation || !objLocation.country_short || !address){
        this.body = 'Invalid parameters';
        return;
    }

    var resultAddress = yield function(graphCallback) {
        const query = 'g.addV(label, vertexLabel, "user_id", user_id, "timestamp", timestamp, "country_short", country_short, "country_long", country_long, '+
        ' "adm_1_short", adm_1_short, "adm_1_long", adm_1_long, "adm_2_short", adm_2_short, "adm_2_long", adm_2_long, '+
        ' "locality_short", locality_short, "locality_long", locality_long, "postal_code", postal_code )';

        var params = { vertexLabel: "address",
                        "user_id": userID,
                        "timestamp": timestamp,
                        "country_short": objLocation.country_short,
                        "country_long": objLocation.country_long,
                        "adm_1_short": objLocation.adm_level_1_short,
                        "adm_1_long": objLocation.adm_level_1_long,
                        "adm_2_short": objLocation.adm_level_2_short,
                        "adm_2_long": objLocation.adm_level_2_long,
                        "locality_short": objLocation.locality_short,
                        "locality_long": objLocation.locality_long,
                        "postal_code": objLocation.postal_code
                    };

        client.executeGraph(query, params, graphCallback);
    }

    if(resultAddress.length !== 1){
        this.body = 'Error while saving address';
        return;
    }

    var resultEdge = yield function(graphCallback) {
        const query = 'g.V().hasId("{~label=journalist, user_id='+userID+'}").as("a").V().hasId(["~label":"address", "user_id":'+userID+', "timestamp":'+timestamp+']).addE("operates").from("a").property("timestamp", '+timestamp+')';
        client.executeGraph(query, graphCallback);
    }

    if(resultEdge.length != 1){
        this.body = 'Error while saving address edge';
        return;
    }

    yield _updateJournalistLocation(userID, address, objLocation.country_short, objLocation.adm_level_1_short);

    this.body = 'Done';
});

module.exports = router;


/*
 * Agregar un nuevo vertex jurnalist con los datos del usuario en MYSQL
 */
module.exports.add = function *(journalist) {

    var journalistID = parseInt(journalist.user_id);

    // Si el usuario existe SALIR
    const user = yield this.getUser(journalistID);
    if(user) return user;

    // New vertex
    var result = yield function(graphCallback) {
        const params = { 'user_id': journalist.user_id,
                         'role': journalist.role_id,
                         'name': journalist.user_name,
                         'email': journalist.user_email,
                         'timestamp_created': parseInt(journalist.user_date_created) ? parseInt(journalist.user_date_created)*1000 : 0,
                         'timestamp_email_verified': parseInt(journalist.user_date_email_verified) ? parseInt(journalist.user_date_email_verified)*1000 : 0,
                         'enter_to_send': false,
                         'dashboard_profile_read': false,
                         'dashboard_community_read': false };

        const query = 'g.addV(label, "journalist", "user_id", user_id, "name", name, "email", email, "role", role, "timestamp_email_verified", timestamp_email_verified, "enter_to_send", enter_to_send, "dashboard_profile_read", dashboard_profile_read, "dashboard_community_read", dashboard_community_read)';
        client.executeGraph(query, params, graphCallback);
    }

    return yield this.getUser(journalistID);
}

module.exports.updateBio = function *(userID, bio){

    const params = {"bio":bio};
    const query = 'g.V().hasId("{~label=journalist, user_id='+userID+'}").property("bio", bio).iterate()';
    const result = yield function(graphCallback) {
        client.executeGraph(query, params, graphCallback);
    }
    return result;
}

module.exports.getUser = function *(userID) {

    const result = yield function(graphCallback) {
        const query = 'g.V().hasId("{~label=journalist, user_id='+userID+'}").valueMap(true)';
        client.executeGraph(query, graphCallback);
    }

    if(result.length == 0) return false;

    var journalist = result.first();
    if(journalist){
        journalist.user_id = journalist.id.user_id;
    }

    return journalist;
}

module.exports.isFollowing = function *(userID, targetUserID) {

    const result = yield function(graphCallback) {
        const query = 'g.V().hasId("{~label=journalist, user_id='+userID+'}").out("follows").has("user_id", '+targetUserID+')';
        client.executeGraph(query, graphCallback);
    }

    if(result.length == 0) return false;
    return true;
}

module.exports.isEngaged = function *(userID, targetUserID) {

    const result = yield function(graphCallback) {
        const query = 'g.V().hasId("{~label=journalist, user_id='+userID+'}").bothE("engaged").as("e").bothV().has("user_id", '+targetUserID+').select("e").valueMap(true)';
        client.executeGraph(query, graphCallback);
    }

    if(result.length == 0) return false;

    return result.first();
}

module.exports.engageRequested = function (userFrom, userTo, timestamp) {
    const params = { "currentUserID":userFrom, "targetUserID":userTo, "timestamp":timestamp };
    const query = 'g.V().has("journalist", "user_id", currentUserID).as("a").V().has("journalist", "user_id", targetUserID).addE("engaged").from("a").property("timestamp_requested", timestamp)';

    client.executeGraph(query, params, function(err, result){
        if(err){
            console.log(err);
        }
    });
}

module.exports.engageAccepted = function (userFrom, userTo, timestamp) {

    const params = { "userFrom":userFrom, "timestamp":timestamp };
    const query = 'g.V().hasId("{~label=journalist, user_id='+userTo+'}").inE("engaged").as("e").outV().has("user_id", userFrom).select("e").property("timestamp_accepted", timestamp).iterate()';
    client.executeGraph(query, params, function(err, result){
        if(err){
            console.log(err);
        }
    });
}

module.exports.engageRejected = function (userFrom, userTo, timestamp) {

    const params = { "userFrom":userFrom, "timestamp":timestamp };
    const query = 'g.V().hasId("{~label=journalist, user_id='+userTo+'}").inE("engaged").as("e").outV().has("user_id", userFrom).select("e").property("timestamp_rejected", timestamp).iterate()';

    client.executeGraph(query, params, function(err, result){
        if(err){
            console.log(err);
        }
    });
}

module.exports.isActive = function * (userId) {

    const params = { "userId":userId };

    let query = 'g.V().has("journalist", "user_id", userId).hasNot("timestamp_deactivated")';
    let result = yield client.executeGraph(query, params);

    return result.length;
}

module.exports.activateAccount = function * (userId) {

    const params = { "userId":userId };

    let query = 'g.V().has("journalist", "user_id", userId).properties("timestamp_deactivated").drop()';
    yield client.executeGraph(query, params);
}

module.exports.deactivateAccount = function * (userId) {

    let timestamp = Number(new Date());
    let params = { "userId":userId, "timestamp": timestamp };

    let query = 'g.V().has("journalist", "user_id", userId).property("timestamp_deactivated", timestamp)';
    yield client.executeGraph(query, params);
}

module.exports.handleIncompleteProfileAccounts = function() {

    const now = new Date();
    const ago12hs = now.setHours(now.getHours() - 12);
    const ago24hs = now.setHours(now.getHours() - 24);
    // const ago24hs = now.setHours(now.getHours() - 168); // 7 dias

    // Enviar email a las cuentas incompletas con más de 12 hs de antiguedad, menos de 24
    const query = 'g.V().hasLabel("journalist").hasNot("picture").hasNot("reminder_12hs").has("timestamp_email_verified", lt('+ago12hs+')).has("timestamp_email_verified", gt('+ago24hs+')).union( '+
                  '   g.V().hasLabel("journalist").hasNot("location").hasNot("reminder_12hs").has("timestamp_email_verified", lt('+ago12hs+')).has("timestamp_email_verified", gt('+ago24hs+')).valueMap("user_id", "name", "email", "timestamp_email_verified"), '+
                  '   g.V().hasLabel("journalist").hasNot("offers").hasNot("reminder_12hs").has("timestamp_email_verified", lt('+ago12hs+')).has("timestamp_email_verified", gt('+ago24hs+')).valueMap("user_id", "name", "email", "timestamp_email_verified"),   '+
                  '   g.V().hasLabel("journalist").hasNot("requests").hasNot("reminder_12hs").has("timestamp_email_verified", lt('+ago12hs+')).has("timestamp_email_verified", gt('+ago24hs+')).valueMap("user_id", "name", "email", "timestamp_email_verified"), '+
                  '   valueMap("user_id", "name", "email", "timestamp_email_verified")).dedup() ';

    client.executeGraph(query, function(err, result){
        if(err){
            console.log(err);
            return;
        }

        result.forEach(function(journalist){
            if(!journalist.email) return;

            const objJournalist = {
                user_id:journalist.user_id[0],
                name:journalist.name[0],
                email:journalist.email[0],
            };

            mandrillExpert.sendRemainderEmail(objJournalist);
            _updateReminder12hs(journalist.user_id[0]);
        });
    });

    // Enviar email a las cuentas incompletas con más de 24 hs de antiguedad
    const query2 = 'g.V().hasLabel("journalist").hasNot("picture").hasNot("reminder_24hs").has("timestamp_email_verified", lt('+ago24hs+')).union( '+
                   '   g.V().hasLabel("journalist").hasNot("location").hasNot("reminder_24hs").has("timestamp_email_verified", lt('+ago24hs+')).valueMap("user_id", "name", "email", "timestamp_email_verified"), '+
                   '   g.V().hasLabel("journalist").hasNot("offers").hasNot("reminder_24hs").has("timestamp_email_verified", lt('+ago24hs+')).valueMap("user_id", "name", "email", "timestamp_email_verified"),   '+
                   '   g.V().hasLabel("journalist").hasNot("requests").hasNot("reminder_24hs").has("timestamp_email_verified", lt('+ago24hs+')).valueMap("user_id", "name", "email", "timestamp_email_verified"), '+
                   '   valueMap("user_id", "name", "email", "timestamp_email_verified")).dedup() ';


    client.executeGraph(query2, function(err, result){
        if(err){
            console.log(err);
            return;
        }

        result.forEach(function(journalist){
            if(!journalist.email) return;

            const objJournalist = {
                user_id:journalist.user_id[0],
                name:journalist.name[0],
                email:journalist.email[0],
            };
            mandrillExpert.sendTimeExtendedEmail(objJournalist);
            _updateReminder24hs(journalist.user_id[0]);
      });
    });
}

function * _updateJournalistLocation(userID, location, country_short, adm_1_short){

    return yield function(graphCallback) {

        const params = {'userID': userID, 'location': location, 'country_short':country_short, 'adm_1_short':adm_1_short};
        const query = 'g.V().has("journalist", "user_id", userID).property("location", location).property("country_short", country_short).property("adm_1_short", adm_1_short).iterate()';
        client.executeGraph(query, params, graphCallback);
    }
}

function _updateReminder12hs(userID){
    const params = {now:new Date()};
    const query = 'g.V().hasId("{~label=journalist, user_id='+userID+'}").property("reminder_12hs", now).iterate()';
    client.executeGraph(query, params, graphCallback);
}

function _updateReminder24hs(userID){
    const params = {now:new Date()};
    const query = 'g.V().hasId("{~label=journalist, user_id='+userID+'}").property("reminder_24hs", now).iterate()';
    client.executeGraph(query, params, graphCallback);
}
