const Router = require('koa-router');
const auth = require('../sessionControl');
const graphConnections = require('./graphConnections');
const countryConstants = require('../../constants/countries');
const jsonBody = require('koa-json-body')();

const router = new Router({
  prefix: '/organizations'
});

const client = graphConnections.getClient();

var graphCallback = function(err, result){
    if(err){
        throw err;
    }
    return result;
}

router.get('/', function *(next) {
    this.body = 'estoy adentro de organizations !';
})

.post('/save', auth.isAdmin, jsonBody, function *(next) {

    var result = yield function(graphCallback) {

        const query = 'g.addV(label, "organization", "name", name, "country_short", countryShort, "country_short_location", countryShortLocation, "email", email, "location", location, "phone", phone, "description", description, "url", linkApply)';

        const params = { "name": this.request.body.name,
                         "countryShort": this.request.body.countryShort,
                         "countryShortLocation": this.request.body.countryShortLocation,
                         "email": this.request.body.email,
                         "location": this.request.body.location,
                         "phone": this.request.body.phone,
                         "description": this.request.body.description,
                         "linkApply": this.request.body.linkApply };

        client.executeGraph(query, params, graphCallback );
    }
    this.body = 'Done';
})

.post('/delete', auth.isAdmin, jsonBody, function *(next) {

    var result = yield function(graphCallback) {

        const query = 'g.V().hasId("{~label=organization, community_id='+this.request.body.community_id+', member_id='+this.request.body.member_id+'}").drop()';
        client.executeGraph(query, graphCallback );
    }

    this.body = 'Done';
})

.get('/get.list', auth.isJournalist, function *(next) {

    var result = yield _getList();
    this.body = result.toArray();
})

.get('/get.by.country/:country/:country_location', auth.isJournalist, function *(next) {

    const countryShortNames = countryConstants.getShortNames();
    let params = {};
    params.countryShort = (this.params.country !== 'all') ?  [this.params.country] : countryShortNames;
    params.countryShortLocation = (this.params.country_location !== 'all') ? [this.params.country_location] : countryShortNames;

    let query;

    if(this.params.country == 'all' && this.params.country_location == 'all'){
        var result = yield _getList();
        this.body = result.toArray();
        return;

    }else if (this.params.country !== 'all' && this.params.country_location === 'all') {
        query = 'g.V().hasLabel("organization").has("country_short", within(countryShort) ).valueMap(true)';
    }else if (this.params.country === 'all' && this.params.country_location !== 'all') {
        query = 'g.V().hasLabel("organization").has("country_short_location", within(countryShortLocation) ).valueMap(true)';
    }else if (this.params.country !== 'all' && this.params.country_location !== 'all') {
        query = 'g.V().hasLabel("organization").has("country_short", within(countryShort) ).has("country_short_location", within(countryShortLocation) ).valueMap(true)';
    }

    var result = yield function(graphCallback) {
        client.executeGraph(query, params, graphCallback );
    }
    this.body = result.toArray();
});



module.exports = router;

function * _getList(){

    const countryShortNames = countryConstants.getShortNames();
    const params = { 'shortNames':countryShortNames };
    const query = 'g.V().hasLabel("organization").has("country_short", within(shortNames)).valueMap(true)';

    return yield function(graphCallback) {
        client.executeGraph(query, params, graphCallback );
    }
}
