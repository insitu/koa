const Router = require('koa-router');
const auth = require('../sessionControl');
const constants = require('../../constants/tagConstants');
const graphConnections = require('./graphConnections');
const jsonBody = require('koa-json-body')();
// const matchController = require('../matches');


const router = new Router({
  prefix: '/tags'
});

const client = graphConnections.getClient();

var graphCallback = function(err, result){
    if(err){
        throw err;
    }
    return result;
}

router.get('/', function *(next) {
    this.body = 'estoy adentro de tags !';
})

.get('/create.categories', function *(next) {
// .get('/create.categories', function *(next) {

    var includeLevel = 1;
    var tagFrom;
    var tagTo;

    var arraySkills = ['Editing', 'Translation', 'Copy Writing', 'Design', 'Graphic Design', 'Broadcast', 'Data Journalism', 'Mobile Journalism', 'New Media', 'Photo Journalism', 'Social Media', 'Storytelling', 'Video Journalism', 'Visual Journalism', 'Writing'];
    var arraySkillsImages = ['editing.jpg', 'translation.jpg', 'copywriting.jpg', 'design.jpg', 'graphicdesign.jpg', 'broadcastradio.jpg', 'datajournalism.jpg', 'mobilejournalism.jpg', 'newmedia.jpg', 'photojournalism.jpg', 'socialmedia.jpg', 'storytelling.jpg', 'videojournalism.jpg', 'visualjournalism.png', 'writing.jpg'];

    var arrayBeats = ['Environment', 'Current Affairs', 'Education', 'Politics', 'Science', 'Policy', 'Economics', 'Business', 'Criminal', 'Human Rights', 'Religion', 'Health', 'Government', 'Finance', 'Security', 'Foreign Policy', 'Law', 'Civil Rights', 'Community', 'Social Justice', 'Conflict', 'Agriculture', 'Animals Welfare', 'Culture', 'Military', 'Elections', 'Children Rights', 'Climate Change', 'Privacy'];
    var arrayBeatsImages = ['environment.jpg', 'currentaffairs.jpg', 'education.jpg', 'politics.jpg', 'science.jpg', 'policy.jpg', 'economics.jpg', 'business.jpg', 'criminal.jpg', 'humanrights.jpg', 'religion.jpg', 'health.jpg', 'government.jpg', 'finance.jpg', 'security.jpg', 'foreignpolicy.jpg', 'law.jpeg', 'civilrights.jpg', 'community.jpg', 'socialjustice.jpg', 'conflict.jpg', 'agriculture.jpg', 'animalwelfare.jpg', 'culture.jpg', 'military.jpg', 'elections.jpg', 'childrenrights.jpg', 'climatechange.jpg', 'privacy.jpg'];

    var arrayFocus = ['Investigative', 'News', 'Review', 'Columns', 'Features', 'Opinion', 'Advocacy Journalism', 'Community Journalism', 'Participatory Media', 'Solutions Journalism'];
    var arrayFocusImages = ['investigative.jpg', 'news.jpg', 'review.jpg', 'columns.jpg', 'features.png', 'opinion.jpg', 'advocacyjournalism.jpg', 'communityjournalism.png', 'participatorymedia.jpg', 'solutionjournalism.png'];

    // var arrayLanguages = ['Aari', 'Aariya', 'Aasáx', 'Abadi', 'Abanyom', 'Acheron', 'Adabe', 'Adasen', 'Adhola', 'Adi, Galo', 'Afade', 'Afrikaans', 'Agarabi', 'Aghem', 'Agta, Isarog', 'Akar-Bale', 'Akaselem', 'Akposo', 'Albanian', 'Algonquin', 'Amharic', 'Andra-Hus', 'Arabic', 'Arammba', 'Argobba', 'Armenian',
    //                       'Bagri', 'Bahamas Creole English', 'Barbareño', 'Bardi', 'Bashkardi', 'Basque', 'Belize Kriol English', 'Bella Coola', 'Bengali', 'Bulgarian', 'Burarra', 'Burmese', 'Byelorussian', 'Catalan', 'Chinese', 'Chinese Mandarin', 'Como Karim', 'Cowlitz', 'Croatian', 'Czech', 'Danish', 'Dominican Creole French',
    //                       'Dutch', 'English', 'Farsi', 'Finnish', 'Franco-Provençal', 'Frankish', 'French', 'German', 'German, Colonia Tovar', 'German, Hutterite', 'German, Swiss', 'Hindi', 'Icelandic', 'Italian', 'Malay', 'Mazatec, Ixcatlán', 'Mixtec, Coatzospan', 'Mixtec, Yutanduchi', 'Portuguese', 'Qaqet',
    //                       'Russian', 'Slovak', 'Spanish', 'Uzbek', 'Zapotec, Santa Catarina Albarradas'];

    var arrayLanguages = ['Arabic', 'Chinese', 'Chinese Mandarin', 'Croatian', 'Czech',
                          'Dutch', 'English', 'French', 'German', 'Italian',
                          'Portuguese', 'Russian', 'Spanish'];

    var categoriesTop = ['Connections', 'Job Opportunities', 'Events', 'Support', 'Skills', 'Equipment', 'Translation', 'Upcoming category'];
    var categoriesLevelTwo = new Array();
    categoriesLevelTwo['Connections'] = ['Fixers', 'Sources', 'Domain Expert', 'Researcher'];
    categoriesLevelTwo['Work Opportunities'] = ['Assignments', 'Internships', 'Jobs', 'Grants', 'Fellowships'];
    categoriesLevelTwo['Events'] = ['Workshops', 'Trainings', 'Conferences', 'Meetups'];
    categoriesLevelTwo['Support'] = ['Journalism Organizations', 'Insurance', 'Press requirements'];
    categoriesLevelTwo['Skills'] = ['Mentorship', 'Teaching', 'Advice'];
    categoriesLevelTwo['Equipment'] = ['Accesories', 'Audio', 'Books', 'Camera', 'Gear', 'Laptop', 'Safety', 'Tablets'];
    categoriesLevelTwo['Translation'] = ['Technical', 'Bussiness', 'Legal', 'Software', 'Documents', 'Medical', 'Manufacturing', 'Accounting'];

    var categoriesLevelThree = new Array();
    categoriesLevelThree['Assignments'] = arraySkills;
    categoriesLevelThree['Trainings'] = arraySkills;
    categoriesLevelThree['Mentorship'] = arraySkills;
    categoriesLevelThree['Teaching'] = arraySkills;
    categoriesLevelThree['Advice'] = arraySkills;

    categoriesLevelThree['Technical'] = arrayLanguages;
    categoriesLevelThree['Bussiness'] = arrayLanguages;
    categoriesLevelThree['Legal'] = arrayLanguages;
    categoriesLevelThree['Software'] = arrayLanguages;
    categoriesLevelThree['Documents'] = arrayLanguages;
    categoriesLevelThree['Medical'] = arrayLanguages;
    categoriesLevelThree['Manufacturing'] = arrayLanguages;
    categoriesLevelThree['Accounting'] = arrayLanguages;

    var i;
    var type = constants.TYPE_SKILL;
    for(i=0; i < arraySkills.length; i++){
        yield _saveTag(arraySkills[i], type, arraySkillsImages[i], true);
    }
    type = constants.TYPE_BEAT;
    for(i=0; i < arrayBeats.length; i++){
        yield _saveTag(arrayBeats[i], type, arrayBeatsImages[i], true);
    }
    type = constants.TYPE_FOCUS;
    for(i=0; i < arrayFocus.length; i++){
        yield _saveTag(arrayFocus[i], type, arrayFocusImages[i], true);
    }
    type = constants.TYPE_LANGUAGE;
    for(i=0; i < arrayLanguages.length; i++){
        yield _saveTag(arrayLanguages[i], type, arrayLanguages[i], true);
    }

    var j;
    var k;
    var arrayLevelTwo;
    var arrayLevelThree;

    console.log('##### Iniciando generacion de tags... ');

    yield _saveTag('categories', null, null, true);

    for(i=0; i < categoriesTop.length; i++){

        console.log('********' + categoriesTop[i] + '************');
        yield _saveTag(categoriesTop[i], null, null, true);
        yield _saveEdgeInclude('categories', categoriesTop[i], 1);

        arrayLevelTwo = categoriesLevelTwo[categoriesTop[i]];
        if(!arrayLevelTwo) continue;

        for(j=0; j < arrayLevelTwo.length; j++){

            console.log(' --------->' + arrayLevelTwo[j]);
            yield _saveTag(arrayLevelTwo[j], null, null, true);
            yield _saveEdgeInclude(categoriesTop[i], arrayLevelTwo[j], 2);

            arrayLevelThree = categoriesLevelThree[arrayLevelTwo[j]];

            if(!arrayLevelThree) continue;

            for(k=0; k < arrayLevelThree.length; k++){

                console.log(' -------------------------->' + arrayLevelThree[k]);
                // yield _saveTag(arrayLevelThree[k], null, null, true);
                yield _saveEdgeInclude(arrayLevelTwo[j], arrayLevelThree[k], 3);
            }
        }
    }

    console.log('##### Finalizado. ');
    this.body = 'Done';
})

/*
 * Save an offer or request
 */
.post('/save.interchange', auth.isJournalist, jsonBody, function *(next) {

    var userID = auth.getSession(this.cookies.get("KOASESSID")).id;
    var tagNames = this.request.body.tags.trim();
    var type = this.request.body.type;
    var tagName;
    var timestamp = Date.now();

    if( (type != constants.TYPE_OFFER && type != constants.TYPE_REQUEST) || parseInt(userID) != userID){
        this.body = 'Invalid params';
        return;
    }

    var propertyName = 'requests';
    if(type == constants.TYPE_OFFER){
        propertyName = 'offers';
    }

    var arrayTags = tagNames.split(".");
    for(var i=0; i<arrayTags.length; i++){
        tagName = arrayTags[i];
        yield _saveTag(tagName);
        yield _saveEdgeHas(userID, tagName, type, 0, arrayTags.length - i);
    }

    const query = 'g.V().hasId("{~label=journalist, user_id='+userID+'}").property(list, "'+propertyName+'", "'+tagNames+'", "timestamp", '+timestamp+').iterate()';
    client.executeGraph(query, function(err,result){
        if(err)console.log(err);
    });

    this.body = 'Done';
})

.post('/delete.interchange', auth.isJournalist, jsonBody, function *(next) {

    var userID = auth.getSession(this.cookies.get("KOASESSID")).id;
    var tagNames = this.request.body.tags.trim();
    var type = this.request.body.type;
    var tagName;

    if( (type != constants.TYPE_OFFER && type != constants.TYPE_REQUEST) || parseInt(userID) != userID){
        this.body = 'Invalid params';
        return;
    }

    var arrayTags = tagNames.split(".");
    for(var i=0; i<arrayTags.length; i++){

        tagName = arrayTags[i];
        yield _deleteEdgeHas(userID, tagName, type);
    }

    var propertyName = 'offers';
    if(type == constants.TYPE_REQUEST){
        propertyName = 'requests';
    }

    // Borrar la property offer or requests del journalist
    const query = 'g.V().hasId("{~label=journalist, user_id='+userID+'}").properties("'+propertyName+'").hasValue("'+tagNames+'").limit(1).drop()';
    client.executeGraph(query, function(err,result){
        if(err)console.log(err);
    });
    this.body = 'Done';
})

/*
 * Save the user current position - called from Profile page
 */
.post('/save.position', auth.isJournalist, jsonBody, function *(next) {

    var userID = auth.getSession(this.cookies.get("KOASESSID")).id;
    var tagName = this.request.body.position.trim();
    var type = constants.TYPE_POSITION;

    if( parseInt(userID) != userID ){
        this.body = 'Invalid params';
        return;
    }

    // TODO: Pendiente descomentar cuando resuelva la validacion en el FE
    // if(tagName.length > 0){
    //     yield _saveTag(tagName);
    //     yield _saveEdgeHas(userID, tagName, type);
    // }

    yield _updateJournalistPosition(userID, tagName);

    this.body = 'Done';
})

/*
 * Adding skills beats focus and language - called from profile page
 */
.post('/save.expertise', auth.isJournalist, jsonBody, function *(next) {

    var userID = auth.getSession(this.cookies.get("KOASESSID")).id;
    var tagName = this.request.body.tag.trim();
    var type = this.request.body.type.trim();
    var expertise = this.request.body.expertise;

    if( parseInt(userID) != userID || (type != constants.TYPE_SKILL && type != constants.TYPE_BEAT
        && type != constants.TYPE_FOCUS && type != constants.TYPE_LANGUAGE) ){

        this.body = 'Invalid params';
        return;
    }

    yield _saveTag(tagName, type);

    // In case it does exists, delete it
    yield _deleteEdgeHas(userID, tagName, type);

    var edgeResult = yield _saveEdgeHas(userID, tagName, type, expertise);
    // if(edgeResult.length != 1){ this.status = 500; return; }

    this.body = 'Done';
})

.post('/delete.expertise', auth.isJournalist, jsonBody, function *(next) {

    const userID = auth.getSession(this.cookies.get("KOASESSID")).id;
    const type = this.request.body.type;
    const tagName = this.request.body.tag;

    if( parseInt(userID) != userID || (type != constants.TYPE_SKILL && type != constants.TYPE_BEAT
        && type != constants.TYPE_FOCUS && type != constants.TYPE_LANGUAGE) ){

        this.body = 'Invalid params';
        return;
    }

    var result = yield _deleteEdgeHas(userID, tagName, type);

    this.body = 'Done';
})

/*
 * Collaborate filters - Also used in profile for adding offers and requests
 */
.post('/save.category', auth.isJournalist, jsonBody, function *(next) {

    var includeLevel = parseInt(this.request.body.level);
    var tagFrom = this.request.body.parent.trim();
    var tagTo = this.request.body.tag.trim();

    if( includeLevel < 1 || tagFrom === '' || tagTo === '') throw new Error('Invalid params');

    var result = yield _saveTag(tagTo);
    yield _saveEdgeInclude(tagFrom, tagTo, includeLevel);

    this.body = 'Done';
})

// .post('/save.discussion', auth.isJournalist, jsonBody, function *(next) {
//
//     const userID = auth.getSession(this.cookies.get("KOASESSID")).id;
//     const discussionID = parseInt(this.request.body.dis_id);
//     const title = this.request.body.title;
//     const text = this.request.body.text;
//     const tagNames = this.request.body.tags;
//
//     yield discussionController.save(userID, discussionID);
//
//     for(var i=0; i<tagNames.length; i++){
//
//         yield _saveTag(tagNames[i]);
//         yield _saveEdgeHas(userID, tagNames[i], constants.TYPE_DISCUSSION_POST);
//         yield discussionController.saveTag(discussionID, tagNames[i]);
//     }
//
//     this.body = 'Done';
// })

.get('/search/:searchValue', auth.isJournalist, function *(next) {

    var searchValue = this.params.searchValue;

    var result = yield function(graphCallback) {
        var query = "g.V().hasLabel('tag').has('search_name', Search.tokenRegex('.*"+searchValue+".*')).limit(10).values('search_name')";
        client.executeGraph(query, graphCallback);
    }

    this.body = result.toArray();
})

.get('/children/:tag/:level', auth.isJournalist, function *(next) {

    const level = parseInt(this.params.level);
    const tagName = this.params.tag.toLowerCase();

    var result = yield function(graphCallback) {
        var query = 'g.V().hasId("{~label=tag, name='+tagName+'}").outE("include").has("level", edgeLevel).inV().has("verified", true).order().by("name").values("search_name")';
        client.executeGraph(query, {"edgeLevel": level}, graphCallback );
    }
    this.body = result.toArray();
})

.get('/get.filters', auth.isJournalist, function *(next) {

    var result = yield function(graphCallback) {
        const query = 'g.V().has("tag", "type", within("TYPE_SKILL", "TYPE_BEAT", "TYPE_FOCUS")).has("verified", true).order().by("type").by("name").valueMap("type", "name")';
        client.executeGraph(query, graphCallback);
    }

    this.body = result.toArray();
})

/*
 * Get tags by type, and the expertise for those that are linked to the user with "has" edge.
 * Used for the skills / beats / focus panel in profile and sign up process pages.
 */
.get('/get.by.type/:type', auth.isJournalist, function *(next) {

    const userID = auth.getSession(this.cookies.get("KOASESSID")).id;

    const result = yield function(graphCallback) {
        const params = {type:this.params.type};
        const query = 'g.V().has("tag", "type", type).valueMap("name", "image")';
        client.executeGraph(query, params, graphCallback);
    }

    const resultEdges = yield function(graphCallback) {
        const params = {userID:userID, type:this.params.type};
        const query = 'g.V().has("journalist", "user_id", userID).outE("has").has("type", type).as("edge").inV().as("tag").select("edge", "tag")';
        client.executeGraph(query, params, graphCallback);
    }

    var arrayTags = result.toArray();
    var arrayEdges = resultEdges.toArray();
    var response = [];
    var j;
    var item;

    for(var i=0; i<arrayTags.length; i++){

        item = {name: arrayTags[i].name, image:arrayTags[i].image};

        for(j=0; j<arrayEdges.length; j++){

            if( arrayEdges[j].tag.id.name == arrayTags[i].name){

                item.expertise = arrayEdges[j].edge.properties.expertise;
                item.timestamp = arrayEdges[j].edge.properties.timestamp;
                break;
            }
        }
        response.push(item);
    }

    this.body = JSON.stringify(response);
})

/*
 * Get the User's Skills / Beats / Focus / Languages grouped by expanded and collapsed
 * Used in the Profile page
 */
.get('/get.current.experience/:user_id', auth.isJournalist, function *(next) {

    // const userID = auth.getSession(this.cookies.get("KOASESSID")).id;
    const userID = parseInt(this.params.user_id);
    const result = yield function(graphCallback) {
        const params = {userID:userID};
        const query = 'g.V().has("journalist", "user_id", userID).outE("has")';
        client.executeGraph(query, params, graphCallback);
    }

    var response = {};
    var item;
    var countSkill = 0;
    var countBeat = 0;
    var countFocus = 0;
    response['beats_top'] = [];
    response['beats_collapse'] = [];
    response['focus_top'] = [];
    response['focus_collapse'] = [];
    response['skills_top'] = [];
    response['skills_collapse'] = [];
    response['languages'] = [];


    result.forEach(function(edge) {

        item = {"name": edge.inV.name,
                "type": edge.properties.type,
                "expertise": edge.properties.expertise,
                "timestamp": edge.properties.timestamp, };

        switch(edge.properties.type){

            case constants.TYPE_SKILL:

                if(countSkill < 3){
                    response['skills_top'].push(item);
                }else{
                    response['skills_collapse'].push(item);
                }
                countSkill++;
                break;

            case constants.TYPE_BEAT:

                if(countBeat < 3){
                    response['beats_top'].push(item);
                }else{
                    response['beats_collapse'].push(item);
                }
                countBeat++;
                break;

            case constants.TYPE_FOCUS:

                if(countFocus < 3){
                    response['focus_top'].push(item);
                }else{
                    response['focus_collapse'].push(item);
                }
                countFocus++;
                break;

            case constants.TYPE_LANGUAGE:

                response['languages'].push(item);
                break;
        }

    });

    this.body = JSON.stringify(response);
});

module.exports = router;

// function * getTag(tagName){
//     return yield function(graphCallback) {
//         const query = 'g.V().hasId("{~label=tag, name='+tagName+'}")';
//         client.executeGraph(query, graphCallback);
//     }
// }

module.exports.saveTag = function *(tagName){
    return yield _saveTag(tagName);
}

module.exports.saveEdgeHas = function *(userID, tagName, type, expertise){
    tagName = tagName.toLowerCase();
    return yield _saveEdgeHas(userID, tagName, type, expertise);
}

function _validateTagName(tagName){
  // const letterNumber = /^[0-9a-zA-Z]+$/;
  // const letters = /^[A-Za-z]+$/;
  const lettersNumbersAndSpaces = /^[0-9a-zA-Z\s]*$/;
  if(tagName === '' || tagName.length < 1 || tagName.length > constants.MAX_LENGTH || !tagName.match(lettersNumbersAndSpaces)){
      throw new Error('Invalid tag name: ' + tagName);
  }
}

function * _getTagFromDatabase(tagName){

    tagName = tagName.toLowerCase();
    var result = yield function(graphCallback) {
        query = 'g.V().hasId("{~label=tag, name='+tagName+'}").valueMap()';
        client.executeGraph(query, graphCallback);
    }

    // The tag does not exists
    if(result.length == 0){
        return false;
    }

    return result.first();
    // return parseInt(result.first().properties.counter[0].value) + 1;
}

/*
 * Puede recibir solo el nombre o más parametros. En caso de ser distintos a undefined, se usan para actualizar
 * el tag, evitando sobreescribir properties existentes.
 */
function * _saveTag(tagName, type, image, verified){

    tagName = tagName.toLowerCase();
    _validateTagName(tagName);

    var params = yield _getVerificationParamsToSave(tagName, verified);
    var query = 'g.addV(label, "tag", "name", tagName, "search_name", tagName, "counter", counter, "verified", verified';

    if(image){
        params.image = image;
        query += ', "image", image';
    }
    if(type){
        params.type = type;
        query += ', "type", type';
    }
    query += ')';

    return yield function(graphCallback) {
        client.executeGraph(query, params, graphCallback);
    }
}

/*
 * Controla la verificación de un tag, segun la cantidad de usos que tenga.
 */
function * _getVerificationParamsToSave(tagName, verified){

    tagName = tagName.toLowerCase();
    const tagResult = yield _getTagFromDatabase(tagName);
    const currentCounter = tagResult && parseInt(tagResult.counter[0]) ? parseInt(tagResult.counter[0]) + 1 : 1;
    verified = verified || (currentCounter >= 4) || ( tagResult && tagResult.verified[0])  ? true : false;

    return { "tagName": tagName, "counter": currentCounter, "verified": verified};
}


function * _saveEdgeInclude(tagFrom, tagTo, includeLevel){

    tagFrom = tagFrom.toLowerCase();
    tagTo = tagTo.toLowerCase();

    var edeIncludeResult = yield function(graphCallback) {
        const query = 'g.V().hasId("{~label=tag, name='+tagFrom+'}").outE("include").has("level", '+includeLevel+').as("a").inV().hasLabel("tag").has("name", "'+tagTo+'").select("a")';
        client.executeGraph(query, graphCallback);
    }

    if(edeIncludeResult.length > 0){
        return;
    }

    const query = 'g.V().has("tag", "name", tagFrom).as("a").V().has("tag", "name", tagTo).addE("include").from("a").property("level", includeLevel)';
    client.executeGraph(query, { "tagFrom":tagFrom, "tagTo":tagTo, "includeLevel":includeLevel }, graphCallback);
}

function _manageMatchesCallback(currentUser, tagName, type, result){

    const matchController = require('../matches');

    const timestamp = Date.now();
    tagName = tagName.toLowerCase();

    // Current User is the owner of the new saved tag
    var tagInfoSource = {
        name:tagName,
        type:type,
        timestamp:timestamp,
        owner:true
    };

    // Target User is NOT the owner of the new saved tag
    var tagInfoTarget = {
        name:tagName,
        type:type,
        timestamp:timestamp,
        owner:false
    };

    // Stringify the objects
    tagInfoSource = JSON.stringify(tagInfoSource);
    tagInfoTarget = JSON.stringify(tagInfoTarget);

    var matchSource = {
        last_update:timestamp,
        tags_info:{'$prepend':[tagInfoSource]}
    };

    var matchTarget = {
        last_update:timestamp,
        tags_info:{'$prepend':[tagInfoTarget]},
        target_adm_1_short:currentUser.adm_1_short ? currentUser.adm_1_short[0] : '',
        target_name:currentUser.name[0],
        target_position:currentUser.position ? currentUser.position[0] : '',
        target_picture:currentUser.picture ? currentUser.picture[0]: '',
        target_location:currentUser.location ? currentUser.location[0] : ''
    };

    result.forEach(function(vertex){

        // Properties
        matchSource.target_name = vertex.name[0];
        matchSource.target_picture = vertex.picture ? vertex.picture[0] : '';
        matchSource.target_position = vertex.position ? vertex.position[0] : '';
        matchSource.target_location = vertex.location ? vertex.location[0] : '';
        matchSource.target_adm_1_short = vertex.adm_1_short ? vertex.adm_1_short[0] : '';

        const primaryFieldsSource = {
            user_id: currentUser.user_id[0],
            target_country_short: vertex.country_short ? vertex.country_short[0] : '',
            target_user_id: vertex.user_id[0]
        };

        matchController.saveMatch(matchSource, primaryFieldsSource);

        const primaryFieldsTarget = {
            user_id: vertex.user_id[0],
            target_country_short: currentUser.country_short ? currentUser.country_short[0] : '',
            target_user_id: currentUser.user_id[0]
        };

        matchController.saveMatch(matchTarget, primaryFieldsTarget);

    });
}

/*
 * Searches for matches based on the new user tag
 */
function * _manageMatches(userID, tagName, type){

    tagName = tagName.toLowerCase();

    // Find the current user in the Graph database.
    var userResult = yield function(graphCallback) {
        const query = 'g.V().hasId("{~label=journalist, user_id='+userID+'}").valueMap("user_id", "name", "position", "location", "picture", "country_short")';
        client.executeGraph(query, graphCallback);
    }

    const currentUser = userResult.first();

    // Find matches in the graph database
    var query;
    switch(type){

        case constants.TYPE_OFFER:
        case constants.TYPE_SKILL:
        case constants.TYPE_BEAT:
        case constants.TYPE_FOCUS:
        case constants.TYPE_POSITION:
        case constants.TYPE_LANGUAGE:
        case constants.TYPE_DISCUSSION_POST:

            query = 'g.V().hasId("{~label=tag, name='+tagName+'}").inE().has("type", "TYPE_REQUEST").outV().has("user_id", neq('+userID+')).dedup().valueMap("user_id", "name", "location", "position", "picture", "country_short")';

            client.executeGraph(query, function(err, result){
                _manageMatchesCallback(currentUser, tagName, type, result);
            });
            break;

        case constants.TYPE_REQUEST:
        case constants.TYPE_DISCUSSION_POST:

            query = 'g.V().hasId("{~label=tag, name='+tagName+'}").inE().has("type", neq("TYPE_REQUEST") ).outV().has("user_id", neq('+userID+')).dedup().valueMap("user_id", "name", "location", "position", "picture", "country_short")';

            client.executeGraph(query, function(err, result){
                _manageMatchesCallback(currentUser, tagName, type, result);
            });
            break;
    }
}

function * _saveEdgeHas(userID, tagName, type, expertise, tagLevel){

    const timestamp = Date.now();
    const level = parseInt(tagLevel) || 1;
    var hasExpertise = parseInt(expertise) || 0;
    tagName = tagName.toLowerCase();

    // Buscar la conexion por type y level, para no repetirla.
    var query = 'g.V().hasId("{~label=journalist, user_id='+userID+'}")'+
                '.outE("has").has("type", hasType).has("level", hasLevel).as("e")'+
                '.inV().has("name", tagName).select("e")';

    var params = {
        "hasType": type,
        "tagName": tagName,
        "hasLevel": level
    };

    var result = yield function(graphCallback) {
        client.executeGraph(query, params, graphCallback);
    }

    if(result.length > 0){

        // UPDATE timestamp & expertise

        var existentEdge = result.first();

        // Si no se ha mandado expertise, no actualizar el valor.
        if(hasExpertise == 0){
            hasExpertise = existentEdge.properties.expertise;
        }

        query = 'g.V().hasId("{~label=journalist, user_id='+userID+'}")'+
                '.outE("has").has("type", hasType).has("level", hasLevel).as("e")'+
                '.inV().has("name", tagName).select("e")'+
                '.property("timestamp", hasTimestamp)'+
                '.property("expertise", hasExpertise).iterate()';

        params = {
            "hasType": type,
            "tagName": tagName,
            "hasLevel": level,
            "hasExpertise": hasExpertise,
            "hasTimestamp": timestamp,
        };

    }else{

        // INSERT new

        query = 'g.V().hasId("{~label=journalist, user_id='+userID+'}").as("a")'+
                      '.V().has("tag", "name", tagName).addE("has").from("a")'+
                      '.property("type", hasType)'+
                      '.property("timestamp", hasTimestamp)'+
                      '.property("level", hasLevel)'+
                      '.property("expertise", hasExpertise)';

        params = {
                "tagName": tagName,
                "hasExpertise": hasExpertise,
                "hasType": type,
                "hasTimestamp": timestamp,
                "hasLevel": level
        };
    }


    result = yield function(graphCallback) {
        client.executeGraph(query, params, graphCallback);
    }

    yield _manageMatches(userID, tagName, type);

    return result;
}

function * _deleteEdgeHas(userID, tagName, type){

    tagName = tagName.toLowerCase();

    const query = 'g.V().hasId("{~label=journalist, user_id='+userID+'}")'+
                  '.outE("has").has("type", hasType).as("e").inV()'+
                  '.has("name", tagName).select("e").drop()';

    const params = {"tagName":tagName, "hasType": type};

    var result = yield function(graphCallback) {
        client.executeGraph(query, params, graphCallback);
    }

    return result;
}

function * _updateJournalistPosition(userID, position){

    return yield function(graphCallback) {
        const query = 'g.V().hasId("{~label=journalist, user_id='+userID+'}").property("position", position).iterate()';
        client.executeGraph(query, {'position': position}, graphCallback);
    }
}
