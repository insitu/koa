const Router = require('koa-router');
const auth = require('../sessionControl');
const graphConnections = require('./graphConnections');

const router = new Router({
  prefix: '/filters'
});

const client = graphConnections.getClient();

var graphCallback = function(err, result){
    if(err){
        throw err;
    }
    return result;
}

router.get('/', function *(next) {
    this.body = 'estoy adentro de filtros !';
})

.get('/get.categories/:parent_tag/:level', auth.isJournalist, function *(next) {

    var level = parseInt(this.params.level);
    var parentTag = this.params.parent_tag;

    var categories = yield function(graphCallback) {
        var query = 'g.V().has("tag", "name", tagName).outE("include").has("level", edgeLevel)inV()';
        client.executeGraph(query, {"tagName": parentTag, "edgeLevel": level}, graphCallback );
    }

    this.body = categories.toArray();
});

module.exports = router;
