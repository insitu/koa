"use strict";
// let koa = require('koa');
const constants = require('../constants/activityConstants.js');
const Router = require('koa-router');
const models = require('express-cassandra');
const mysql = require('mysql');
const mysqlConnections = require('./mysqlConnections');
const auth = require('./sessionControl');

const router = new Router({
  prefix: '/notifications'
});

router.get('/', auth.isJournalist, function *(next) {
    this.body = 'estoy adentro de notificaciones !';
})

/*
 * Get the list of notifications for the user in session
 */
.get('/list', auth.isJournalist, function *(next) {

    const userID = auth.getSession(this.cookies.get("KOASESSID")).id;
    const selectOptions = {
        materialized_view: 'activity_notification_date',
        raw: true,
        select: ['toTimestamp(timeuuid) as timestamp',
                 'toUnixTimestamp(timeuuid) as unixtimestamp',
                 'user_id',
                 'type',
                 'target_object_id',
                 'timeuuid',
                 'read',
                 'read_time',
                 'users_id',
                 'users_name',
                 'users_pic',
                 'objects_id',
                 'details']
    };

    let notifications = yield function(findCallback) {
        models.instance.Activity_Notification.find({user_id: userID}, selectOptions, findCallback);
    };

    yield _resetCounter(constants.NOTIFICATIONS_GLOBAL, userID);

    this.body = notifications;
})

/*
 * Get the list of notifications for the user in session, BY TYPE
 */
.get('/by.type/:user/:type', auth.isJournalist, function *(next) {

    // const userID = auth.getSession(this.cookies.get("KOASESSID")).id;
    const userID = parseInt(this.params.user);
    const type = this.params.type;

    if(constants[type] != type){
        this.body = 'Bad parameters';
        return;
    }

    let notifications = yield function(findCallback) {
        models.instance.Activity_Notification.find({user_id: userID, type:type}, {raw:true}, findCallback);
    };

    this.body = notifications;
})

.get('/unsubscribe/new_discussions/:hash', function *(next) {

    const hasObject = auth.getJWTObject(this.params.hash);

    if (!hasObject) {
        this.body = 'Sorry, the link is invalid. Access denied.';
        return;

    }else if (new Date(hasObject.exp*1000) < new Date() ) {
        this.body = 'Sorry, the link has expired on ' + new Date(hasObject.exp*1000);
        return;
    }


    let params = [
        hasObject.id,
        constants.DISCUSSION_POST,
        Math.floor(Number(Date.now())/1000)
    ];

    let query = 'INSERT INTO unsubscriptions(user_id, type, timestamp) VALUES (?,?,?)';
    let result = yield mysqlConnections.queryDatabase(query, params);

    this.body = 'You have successfully unsubscribed from every new discussion post in Kulectiv !';
});

module.exports = router;

/*
 * Mark the notification as read and decrease it's counter
 */
module.exports.read = function(notification){

    if(notification.read){
        return;
    }

    const primaryKey = {
        user_id: notification.user_id,
        type: notification.type,
        target_object_id: notification.target_object_id
    };
    const values = {read: true};

    // Marcar notification como leída

    models.instance.Activity_Notification.update(primaryKey, values, function(err){
        if(err) console.error('Error reading notification ' + err);
    });

    // Decrementar en uno el counter de notificaciones pendientes
    const counter = models.datatypes.Long.fromInt(-1);
    models.instance.Activity_Notification_Counter.update({user_id: notification.user_id, type: notification.type}, {unreads_counter:counter}, function(err){
        if(err) console.error('Error decreasing notification counter ' + err);
    });
};

module.exports.resetCounter = function *(counterType, userID){
    yield _resetCounter(counterType, userID);
};


/*
 * Save notfication in database and call to the socket callback function
 */
module.exports.createNotification = function(activity, socketCallback){

    switch(activity.notificationType){

        case constants.MESSAGE_SENT:
            //no notifications
            return;

        case constants.DISCUSSION_POST:
            _getUsersToNotifyDiscussionPost(activity, socketCallback);
            return;

        case constants.DISCUSSION_POST_REPLY:
            _getUserToNotifyDiscussionPostReplay(activity, socketCallback);
            return;

        case constants.MARKETPLACE_POST:
            _getUsersToNotifyMarketplacePost(activity, socketCallback);
            return;

        case constants.MARKETPLACE_POST_REPLY:
            _getUsersToNotifyMarketPlaceReplay(activity, socketCallback);
            return;

        case constants.USER_ENGAGE_ACCEPT:
            _getUsersToNotifyEngageAcepted(activity, socketCallback);
            return;

        case constants.PROJECT_ADD_MEMBER:
            _getUserToNotifyDiscussionPostReplay(activity, socketCallback);
            return;

     }

    let usersToNotify = new Array();
    let targetUser;

    //Save in database
    for( let x in activity.users_id){
        targetUser = activity.users_id[x];
        _upsertNotification(activity, targetUser);
        usersToNotify.push(targetUser);
    }

    //Push notifications
    if(usersToNotify.length > 0 && activity.notifyUsers && socketCallback){
        socketCallback(usersToNotify, activity);
    }
};

function _upsertNotification(activity, targetUserID){

    //Validate user id
    targetUserID = parseInt(targetUserID);
    if(!targetUserID) throw 'Invalid user to notify';

    let counterValue = 1;

    let primaryKey = {
        user_id: targetUserID,
        type: activity.notificationType,
        target_object_id: activity.target_object_id,
    };

    // sourceUser data of the activity is used to show the notification
    let upsertFields = {
        timeuuid: activity.timeuuid,
        read: false,
        read_time: 0,
        objects_id: activity.objects_id,
        details:  activity.details
    };

    let users_id = activity.user_id;
    let users_name = activity.user_name;
    let users_pic = activity.user_pic;

    if(activity.users_id.length > 1 && !activity.revertNotification){

        //Necesito un array de usuarios para insertar en la base por unica vez.
        //Lo obtengo de los usuarios de la activdad, quitando el usuario actual (quien genera activity) y agregando al usuario source

        //transformo en array
        users_id = [users_id];
        users_name = [users_name];
        users_pic = [users_pic];


        for(let i=0; i<activity.users_id.length; i++){
            if(activity.users_id[i] == targetUserID){

                continue;

            }else{
                upsertFields.users_id = users_id.concat(activity.users_id[i]);
                upsertFields.users_name = users_name.concat(activity.users_name[i]);
                upsertFields.users_pic = users_pic.concat(activity.users_pic[i]);
            }
        }

    }else if(!activity.revertNotification){

        //Aca necesito un solo valor entero, para agrupar con las notificaciones anteriores si existen
        upsertFields.users_id = {'$add': [ users_id ]};
        upsertFields.users_name = {'$add': [ users_name ]};
        upsertFields.users_pic = {'$add': [ users_pic ]};

    }else{

        //Revert notification
        upsertFields.users_id = {'$remove': [ users_id ]};
        upsertFields.users_name = {'$remove': [ users_name ]};
        upsertFields.users_pic = {'$remove': [ users_pic ]};
        counterValue = -1;
    }

    models.instance.Activity_Notification.update(primaryKey, upsertFields, function(err){

        if(err){
            console.error('Error notification upsert ' + err);
        }else{

            counterValue = models.datatypes.Long.fromInt(counterValue);

            models.instance.Activity_Notification_Counter.update({user_id: parseInt(targetUserID), type:activity.notificationType}, {unreads_counter:counterValue}, function(err){
                if(err) console.error('Error notification counter upsert ' + err);
            });

            models.instance.Activity_Notification_Counter.update({user_id: targetUserID, type:constants.NOTIFICATIONS_GLOBAL}, {unreads_counter:counterValue}, function(err){
                if(err) console.error('Error updating global counter ' + err);
            });

            //TEMPORALMENTE COMENTADO -  No borrar, luego de probar en caso de obtener valores negativos, descomentar y arreglar
            //Update the global notification counter if it's greather than zero
            // models.instance.Activity_Notification_Counter.find({user_id: targetUserID, type:constants.NOTIFICATIONS_GLOBAL}, {raw:true}, function(err, result){
            //
            //     if(err) console.error('Error finding global counter ' + err);
            //     if(result[0]){
            //
            //         let unreadGlobal = parseInt(result[0].unreads_counter)
            //
            //         //prevent negative value
            //         if( (unreadGlobal + counterValue) > 0){
            //             models.instance.Activity_Notification_Counter.update({user_id: targetUserID, type:constants.NOTIFICATIONS_GLOBAL}, {unreads_counter:counterValue}, function(err){
            //                 if(err) console.error('Error updating global counter ' + err);
            //             });
            //         }
            //
            //     }
            // });
        }
    });
}


//TODO: filter by notification settings
function _getUsersToNotifyMarketplacePost(activity, socketCallback){
    const query = 'SELECT user_id, user_name, user_email FROM users where role_id >= 20 and user_accepted = 1 and user_id <> ' + activity.user_id;
    _queryAndNotify(query, activity, socketCallback);
}

//TODO: filter by notification settings
function _getUsersToNotifyDiscussionPost(activity, socketCallback){
    const query = 'SELECT user_id, user_name, user_email FROM users where role_id >= 20 and user_accepted = 1 and user_id <> ' + activity.user_id + ' AND user_id NOT IN(select distinct user_id FROM unsubscriptions WHERE type = "'+constants.DISCUSSION_POST+'")';
    _queryAndNotify(query, activity, socketCallback);
}

function _getUsersToNotifyEngageAcepted(activity, socketCallback){
    const query = 'SELECT user_id, user_name, user_email FROM users where role_id >= 20 and user_accepted = 1 and user_id = ' + parseInt(activity.target_object_id);
    _queryAndNotify(query, activity, socketCallback);
}

function _getUsersToNotifyMarketPlaceReplay(activity, socketCallback){
    const query = 'SELECT user_id, user_name, user_email FROM users where role_id >= 20 and user_accepted = 1 and user_id = ' + parseInt(activity.users_id[0]);
    _queryAndNotify(query, activity, socketCallback);
}

function _getUserToNotifyDiscussionPostReplay(activity, socketCallback){
    const query = 'SELECT user_id, user_name, user_email FROM users where role_id >= 20 and user_accepted = 1 and user_id = ' + parseInt(activity.users_id[0]);
    _queryAndNotify(query, activity, socketCallback);
}

let mySQLPool  = mysql.createPool({
    connectionLimit : 50,
    host     : '35.185.127.103',
    user     : 'remoto',
    password : 'n0Sabr1aDec1rt3',
    database : 'mainwejo'
});

function * _resetCounter(counterType, userID){

    // decrease the counter to zero
    let unreadCounter = 0;
    let notificationCounters = yield function(findCallback) {
        models.instance.Activity_Notification_Counter.find({user_id: userID, type:counterType}, {raw:true}, findCallback);
    };

    for( let i in notificationCounters){
        unreadCounter = parseInt(notificationCounters[i].unreads_counter) * -1;
    }

    if(unreadCounter !== 0){

        unreadCounter = models.datatypes.Long.fromInt(unreadCounter);
        models.instance.Activity_Notification_Counter.update({user_id:userID, type:counterType}, {unreads_counter:unreadCounter}, {hints:['int']}, function(err){
            if(err) console.error('Error Activity_Notification_Counter update - ' + err);
        });
    }
}

function _queryAndNotify(query, activity, socketCallback){

    mySQLPool.getConnection(function(err, connection) {

        if(err){
            console.error('Error MySQL obteniendo conexion:  ' + err);
            return;
        }

        connection.query(query, function(err, rows) {

            let usersToNotify = [];

            if(err){
                console.error('notifications MySQL Error:  ' + err);
                return;
            }

            for(let x in rows){

                _upsertNotification(activity, rows[x].user_id);
                usersToNotify.push(rows[x]);
            }

            // Si el activity fue llamado desde socket, existe socketCallback, caso contrario fue llamado por ajax.
            if(socketCallback){
                socketCallback(usersToNotify, activity);
            }
            connection.release();
        });
    });
}


function _getUsersAndNotify(query, activity, socketCallback){

    client.executeGraph(query, function(err, result){
        if(err){
            throw new Error(err);
        }
        let usersToNotify = [];

        for(let x in rows){

            _upsertNotification(activity, rows[x].user_id);
            usersToNotify.push(rows[x]);
        }

        // Si el activity fue llamado desde socket, existe socketCallback, caso contrario fue llamado por ajax.
        if(socketCallback){
            socketCallback(usersToNotify, activity);
        }

    });


    mySQLPool.getConnection(function(err, connection) {

        if(err){
            console.error('Error MySQL obteniendo conexion:  ' + err);
            return;
        }

        connection.query(query, function(err, rows) {

            let usersToNotify = [];

            if(err){
                console.error('notifications MySQL Error:  ' + err);
                return;
            }

            for(let x in rows){

                _upsertNotification(activity, rows[x].user_id);
                usersToNotify.push(rows[x]);
            }

            // Si el activity fue llamado desde socket, existe socketCallback, caso contrario fue llamado por ajax.
            if(socketCallback){
                socketCallback(usersToNotify, activity);
            }
            connection.release();
        });
    });
}
