function define(name, value) {
    Object.defineProperty(exports, name, {
        value:      value,
        enumerable: true
    });
}

define('P2P',                   'P2P');
// define('REQUEST_PENDING',       'REQUEST_PENDING');
// define('REQUEST_REJECTED',      'REQUEST_REJECTED');
define('MARKETPLACE',           'MARKETPLACE');

define('TASK_RESOURCE_FEEDBACK',     'TASK_RESOURCE_FEEDBACK');
define('TASK_MARKETPLACE_FEEDBACK',  'TASK_MARKETPLACE_FEEDBACK');

define('SUPPORT',       'SUPPORT');
define('USER_CHANNEL',  'USER_CHANNEL');
