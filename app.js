// var redisStore = require('koa-redis');

var koa = require('koa');
var cors = require('kcors');
var enforceHttps = require('koa-sslify');
var fs = require('fs');

// CASSANDRA
var models = require('express-cassandra');
var notifications = require('./controllers/notifications');
var activities = require('./controllers/activities');
var messages = require('./controllers/messages');
var connections = require('./controllers/connections');
var matches = require('./controllers/matches');
var admin = require('./controllers/admin');
var resources = require('./controllers/resources');
var users = require('./controllers/users');
// GRAPH
var journalists = require('./controllers/graph/journalists');
var tags = require('./controllers/graph/tags');
var filters = require('./controllers/graph/filters');
var countries = require('./controllers/graph/countries');
var organizations = require('./controllers/graph/organizations');
var requirements = require('./controllers/graph/requirements');
var discussions = require('./controllers/graph/discussions');
var projects = require('./controllers/graph/projects');

var login = require('./controllers/login');
var session = require('koa-generic-session');
var passport = require('koa-passport');
var images = require('./controllers/images');
var marketplace = require('./controllers/marketplace');
var publications =require('./controllers/publication');

var app = koa();

app.ENV_PRODUCTION = 'production';
app.ENV_DEVELOPMENT = 'development';

app.name = 'Kulectiv';
// app.env = app.ENV_DEVELOPMENT;
app.env = app.ENV_PRODUCTION;

var server;


if(app.env == 'production'){

    //Redirect to HTTPS on all pages - whitelist: HEAD - GET - el resto obtiene 403
    app.use(enforceHttps());

    const options = {
      key: fs.readFileSync('/etc/ssl/kulectiv/key.pem'),
      cert: fs.readFileSync('/etc/ssl/kulectiv/cert.pem')
    };

    server = require('https').createServer(options, app.callback());

}else{

    server = require('http').createServer(app.callback());
}

var io = require('socket.io')(server); // Damian: ver https://github.com/koajs/koa/issues/234  explica la variantes a como inicializar socket junto con app

var corsOptions = {credentials: true};
app.use(cors(corsOptions));

app.keys = ['mtGPgRneyUOmrHLKwRzoDooGfjH7YnI0H7fKdp1ar6VKnPp0iy'];

// app.use(session({
//     store: redisStore({
//         // Options specified here
//     })
// }));

// para que twitter passport funcione con https secure:true
app.use(session( {cookie:{secure:true}} ));

app.use(passport.initialize());
app.use(passport.session());

app .use(activities.routes())
    .use(activities.allowedMethods())
    .use(messages.routes())
    .use(messages.allowedMethods())
    .use(notifications.routes())
    .use(notifications.allowedMethods())
    .use(connections.routes())
    .use(connections.allowedMethods())
    .use(journalists.routes())
    .use(journalists.allowedMethods())
    .use(tags.routes())
    .use(tags.allowedMethods())
    .use(filters.routes())
    .use(filters.allowedMethods())
    .use(matches.routes())
    .use(matches.allowedMethods())
    .use(countries.routes())
    .use(countries.allowedMethods())
    .use(organizations.routes())
    .use(organizations.allowedMethods())
    .use(requirements.routes())
    .use(requirements.allowedMethods())
    .use(admin.routes())
    .use(admin.allowedMethods())
    .use(resources.routes())
    .use(resources.allowedMethods())
    .use(discussions.routes())
    .use(discussions.allowedMethods())
    .use(images.routes())
    .use(images.allowedMethods())
    .use(login.routes())
    .use(login.allowedMethods())
    .use(marketplace.routes())
    .use(marketplace.allowedMethods())
    .use(users.routes())
    .use(users.allowedMethods())
    .use(publications.routes())
    .use(publications.allowedMethods())
    .use(projects.routes())
    .use(projects.allowedMethods());

// response
app.use(function *(){
    //this.set('Access-Control-Allow-Origin', 'http://localhost:8000');
    //this.set('Access-Control-Allow-Credentials', true);
    // switch (this.path) {
    // case '/get':
    //   get.call(this);
    //   break;
    // case '/remove':
    //   remove.call(this);
    //   break;
    // case '/regenerate':
    //   yield regenerate.call(this);
    //   break;
    // }

    this.body = 'Welcome to Jouture API';
});


//Tell express-cassandra to use the models-directory, and
//use bind() to load the models using cassandra configurations.

//If your keyspace doesn't exist it will be created automatically
//using the default replication strategy provided here.

//If dropTableOnSchemaChange=true, then if your model schema changes,
//the corresponding cassandra table will be dropped and recreated with
//the new schema. Setting this to false will send an error message
//in callback instead for any model attribute changes.
//
//If createKeyspace=false, then it won't be checked whether the
//specified keyspace exists and, if not, it won't get created
// automatically.

var ormParams = {
    clientOptions: {
        // contactPoints: ['52.54.5.52', '54.208.169.190'],
        // contactPoints: ['35.185.35.220', '104.196.171.120', '35.185.77.178'],
        contactPoints: ['35.185.35.220'],
        protocolOptions: { port: 9042 },
        keyspace: 'jouture',
        // queryOptions: {consistency: models.consistencies.quorum}
        queryOptions: {consistency: models.consistencies.one}
    },
    ormOptions: {
        defaultReplicationStrategy : {
            // Configuracion para pruebas un solo nodo
            class: 'SimpleStrategy',
            replication_factor: 1

            // Configuracion para un cluster de 3 nodos
            // class: 'NetworkTopologyStrategy',
            // dc1: 3

            // estaba comentado
            // durable_writes: true
        },
        dropTableOnSchemaChange: false,
        createKeyspace: false
        // migration: 'safe'
    }
};

// server.listen(80, function(){
//     console.log('Listening 80 ');
// });

var ormCallback = function(err) {
    if(err){
        console.log(err.message);
    }else{
        require('./socket/index')(io);
        activities.initGammification();

        server.listen(3000, function(){

            console.log('Up and listening port 3000 - ' + app.env);
            console.log('ENVIRONMENT ', app.env);

            if (app.env === app.ENV_PRODUCTION) {
                startAccountTimer();
            }

        });
    }
};


models.setDirectory( __dirname + '/models').bind(ormParams, ormCallback);


/*
 * Enviar email a cuentas que no han completado el perfil, cada una hora.
 */
function startAccountTimer(){

    setInterval(function(){

        var now = new Date();
        console.log('******* Control de cuentas incompletas - cada una hora');
        console.log(now);
        console.log('');

        journalists.handleIncompleteProfileAccounts();
        // sessionCleanup();

    // }, 300000); // 5 minutos
    }, 3600000); // 1 hora
}

/*
 * Garbage collector for koa-generic-session
 * MemoryStore provides all() to get the list, and we can use get() to force reading and thus expire them.
 */
function sessionCleanup() {
    console.log('******** Corriendo garbage collector de koa-generic-session');
    session.all(function(err, sessions) {
        for (var i = 0; i < sessions.length; i++) {
            sessionStore.get(sessions[i], function() {} );
        }
    });
}
