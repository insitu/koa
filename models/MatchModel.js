module.exports = {
    fields:{
        user_id: "int",
        target_country_short: "text",
        target_user_id: "int",
        last_update: "timestamp",
        target_adm_1_short: "text",
        target_name: "text",
        target_picture: "text",
        target_position: "text",
        target_location: "text",
        tags_info: {
            type: "list",
            typeDef: "<text>"
        },
    },
    key:[["user_id"], "target_country_short", "target_user_id"],
    clustering_order: {"target_country_short": "asc", "target_user_id": "desc"},

//     materialized_views:{
//
// // TODO: ordenar por user_target_id para poder actualizar el campo "last_update" de la tabla
//
//         match_by_target:{
//             select: ["*"],
//             key:[["target_user_id"], "user_id", "target_country_short", "last_update"],
//             clustering_order: {"target_country_short": "asc", "last_update": "desc", "target_user_id":"desc"}
//         }
//     }

    // materialized_views:{
    //
    //     match_by_last_update:{
    //         select: ["*"],
    //         key:[["user_id"], "target_country_short", "last_update", "target_user_id"],
    //         // el ordenamiento por default es ASC
    //         // dado que los user_id estan ordenados en forma desc en la tabla principal, no hace falta esta vista
    //         clustering_order: {"target_country_short": "asc", "last_update": "desc", "target_user_id":"desc"}
    //     }
    //     //TODO: Cuando tengamos que filtrar por target_adm_1_short hacer una vista materializada incluyendo el campo en la PK
    // }
}
