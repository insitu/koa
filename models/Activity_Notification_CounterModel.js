
module.exports = {
    fields:{
        user_id: "int",
        type: "text",
        unreads_counter: "counter"
    },
    key:[["user_id"], "type"]
}
