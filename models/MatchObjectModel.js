/*
 *  user_id: es el usuario al cual pertenecen las recomendaciones
 *  el resto de la informacion pertenece al objeto recomendado
 */
module.exports = {
    fields:{
        user_id: "int",

        country_short: "text",
        adm_1_short: "text",
        locality_short: "text",

        object_id: "int",


        details: {
            type: "list",
            typeDef: "<text>"
        },

        tags_info: {
            type: "list",
            typeDef: "<text>"
        },

        title: "text",
        subtitle: "text",
        description: "text",
    },
    key:[["user_id"], "country_short"],
}
