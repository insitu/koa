/*
 * Agrupamos primero por usuario, para hacer push de todas las notificaciones de un mismo usuario a la vez
 * Luego agrupamos todas las no leidas de un mismo usuario y ordenadas por id de notificacion (timeuuid)
 * CAMBIO: no agrupamps ro leidas porque siempre mantenemos el orden cronologico para mostrar como cualquier notificacion
 * READ: necesitamos actualizar el timeuuid y READ por lo que no pueden ser claves primarias y en la vista, solo puede haber una no PK
 */
module.exports = {
    fields:{
        user_id: "int",
        type: "text",
        target_object_id: "text",
        timeuuid: "timeuuid",
        read: "boolean",
        read_time: "timestamp",
        users_id: {
            type: "list",
            typeDef: "<int>"
        },
        users_name: {
            type: "list",
            typeDef: "<text>"
        },
        users_pic: {
            type: "list",
            typeDef: "<text>"
        },
        objects_id: {
            type: "list",
            typeDef: "<text>"
        },
        details: {
            type: "list",
            typeDef: "<text>"
        }
    },
    // Tengo que mantener esta partition key para que funcione el agrupamiento de notificaciones
    key:[["user_id"], "type", "target_object_id"],
    // key:[["user_id"], "type", "timeuuid"],
    // clustering_order: {"type":"asc", "timeuuid":"desc"},

    materialized_views:{

        activity_notification_date:{
            select: ["*"],
            key:[["user_id"], "timeuuid", "type", "target_object_id"],
            clustering_order: {"timeuuid":"desc", "type":"asc", "target_object_id":"desc"},
        },

        activity_notification_type_date:{
            select: ["*"],
            key:[["user_id"], "type", "timeuuid", "target_object_id"],
            clustering_order: {"type":"asc", "timeuuid":"desc", "target_object_id":"desc"},
        },

    }

}
