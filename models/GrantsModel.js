module.exports = {
    fields:{
        country_short: "text",
        timeuuid: "timeuuid",
        amount: "int",
        deadline: "timestamp",
        title: "text",
        description: "text",
        url: "text",
        location: "text",
        focus: "text"
    },
    key:[["country_short"], "timeuuid", "amount", "deadline"],
    clustering_order: {"timeuuid":"desc", "amount":"desc", "deadline":"desc"}
}
