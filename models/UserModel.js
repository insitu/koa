module.exports = {
    fields:{
        id: "uuid",
        myuser_id: "int",
        profile_level: {
            type: "text",
            rule: {
                validator: function(value){
                    return value == 1 || value == 2 || value == 3 || value == 4 || value == 5 ;
                },
                message: "Invalid profile level"
            }
        },
        community_level: {
            type: "text",
            rule: {
                validator: function(value){
                    return value == 1 || value == 2 || value == 3 || value == 4 || value == 5 ;
                },
                message: "Invalid community level"
            }
        }
    },
    key:[["id"], "myuser_id"]
}
