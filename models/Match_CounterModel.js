// En tiempo real se calcula el el indice de importancia, siendo weight un valor parametriizado en el codigo segun el tipo:
// indice =  funcionCalculaRanking(  (weigth / (level * 1.5)) * counter), object_timestamp )

module.exports = {
    fields:{
        user_id: "int",
        type: "text", // tipo del objeto: discussion / grant / ngo / item

        object_timestamp: "timestamp",
        // level: "int",
        // weigth: "int",
        country_short: "text",
        adm_1_short: "text",
        locality_short: "text",
        object_id: "text",

        counter_weight: "counter"
    },
    // Siempre voy a buscar por un type especifico.
    key:[["user_id", "type"], "object_timestamp", "country_short", "adm_1_short", "locality_short", "object_id"],
    clustering_order: {"object_timestamp":"desc", "country_short":"asc", "adm_1_short":"asc", "locality_short":"asc", "object_id":"desc"},

        // materialized_views:{
        //     match_counter_by_country:{
        //         select: ["*"],
        //         key:[["user_id", "type"], "country_short", "object_timestamp", "weigth", "adm_1_short", "locality_short", "object_id"],
        //         clustering_order: {"country_short":"asc", "object_timestamp":"desc", "weigth":"desc", "adm_1_short":"asc", "locality_short":"asc", "object_id":"desc"},
        //     }
        // },
        //
        // materialized_views:{
        //     match_counter_by_state:{
        //         select: ["*"],
        //         key:[["user_id", "type"], "country_short", "adm_1_short", "object_timestamp", "weigth", "locality_short", "object_id"],
        //         clustering_order: {"country_short":"asc", "adm_1_short":"asc", "object_timestamp":"desc", "weigth":"desc", "locality_short":"asc", "object_id":"desc"},
        //     }
        // },
        //
        // materialized_views:{
        //     match_counter_by_state:{
        //         select: ["*"],
        //         key:[["user_id", "type"], "country_short", "adm_1_short", "locality_short", "object_timestamp", "weigth", "object_id"],
        //         clustering_order: {"country_short":"asc", "adm_1_short":"asc", "locality_short":"asc", "object_timestamp":"desc", "weigth":"desc", "object_id":"desc"},
        //     }
        // }
}
