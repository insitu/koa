module.exports = {
    fields:{
        user_id: "int",
        type: "text",
        times_counter: "counter"
    },
    key:[["user_id"], "type"]
}
