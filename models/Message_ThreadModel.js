module.exports = {
    fields:{
        user: "int",
        last_message_timestamp: "timestamp",
        thread_id: "uuid",
        single_user_target: "int",
        user_target: {
            type: "list",
            typeDef: "<int>"
        },
        user_target_name: {
            type: "list",
            typeDef: "<varchar>"
        },
        user_target_pic: {
            type: "list",
            typeDef: "<varchar>"
        },
        user_target_location: {
            type: "list",
            typeDef: "<varchar>"
        },
        last_message: "text",
        last_user_name: "text",
        join_timestamp: "timestamp",
        subject : "text",
        pending_delivery: "boolean",
        pending_read: "boolean",
        last_status: "int",
        mine: "boolean",
        type: "text"
    },
    key:[["user"], "thread_id", "type"],
    materialized_views:{

        thread_by_last_message:{
            select: ["*"],
            key: [["user"], "last_message_timestamp", "thread_id", "type"],
            clustering_order: {"last_message_timestamp": "desc", "thread_id": "asc", "type": "asc"}
        },
        thread_by_single_target:{
            select: ["*"],
            key: [["user"], "single_user_target", "type", "thread_id"]
        },
        thread_by_pending_delivery:{
            select: ["*"],
            key: [["user"], "pending_delivery", "thread_id", "type"]
        },
        thread_by_pending_read:{
            select: ["*"],
            key: [["user"], "pending_read", "thread_id", "type"]
        }
    }
}
