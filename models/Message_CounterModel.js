module.exports = {
    fields:{
        user_id: "int",
        thread_id: "uuid",
        unreads_counter: "counter"
    },
    key:[["user_id"], "thread_id"]
}
/*,
 unreads_counter: "counter"*/