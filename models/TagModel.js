module.exports = {
    fields:{
        uuid: "uuid",
        tag: "text",
        verified: "boolean"
    },
    key:[["uuid"], "tag"]
}
