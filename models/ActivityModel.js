module.exports = {
    fields:{
        user_id: "int",
        timeuuid: {
            type: "timeuuid",
            default: {"$db_function": "now()"}
        },
        type: "text",
        target_object_id: "text",
        user_name: "text",
        user_pic: "text",
        users_id: {
            type: "list",
            typeDef: "<int>"
        },
        users_name: {
            type: "list",
            typeDef: "<text>"
        },
        users_pic: {
            type: "list",
            typeDef: "<text>"
        },
        objects_id: {
            type: "list",
            typeDef: "<text>"
        },
        details: {
            type: "list",
            typeDef: "<text>"
        }
    },
    key:[["user_id"], "timeuuid"],
    clustering_order: {"timeuuid":"desc"},
    materialized_views:{

        activity_login:{
            select: ["*"],
            key: [["type"], "timeuuid", "user_id"],
            clustering_order: {"timeuuid":"desc", "user_id":"desc"}
        }
    }
}
