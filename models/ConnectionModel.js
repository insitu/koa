module.exports = {
    fields:{
        user_id: "int",
        other_user_id: "int",
        irequested: "boolean",
        accepted: "boolean",
        timestamp_requested: "timestamp",
        timestamp_accepted: "timestamp",
        name: "text",
        pic: "text",
        location: "text",
        country: "text",
        country_id: "int"
    },
    key:[["user_id"], "other_user_id"]
}
