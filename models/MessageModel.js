module.exports = {
    fields:{
        thread_id: "uuid",
        timeuuid: "timeuuid",
        sender_id: "int",
        sender_name: "text",
        message: "text",
        pending_delivery: "boolean",
        pending_read: "boolean",
        info: {
            type: "list",
            typeDef: "<text>"
        }
    },
    key:[["thread_id"], "timeuuid"],
    clustering_order: {"timeuuid":"desc"},
    materialized_views:{
        message_by_pending_delivery:{
            select: ["*"],
            key:[["thread_id"], "pending_delivery", "timeuuid"]
        },
        message_by_pending_read:{
            select: ["*"],
            key:[["thread_id"], "pending_read", "timeuuid"]
        }
    }
}