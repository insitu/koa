/*
 *  Guarda las tareas que componen un nivel adentro de cada Game (Profile / Community).
 *  Permite saber que tareas deben ser completadas para alcanzar un nivel dentro del game.
 *  Permite saber que actividades debes ser realizadas para cumplir una tarea.
 *  Indica la cantidad de puntos obtenidos por cumplir una tareas
 */
module.exports = {
    fields:{
        id: {
            type: "int",
            default: 1
        },
        game: "text",   //profile / community / audience
        level: "int", // cada tarea pertenece a un nivel especifico, el tener todas las tareas completas completa el nivel.
        group: "text", //discussion, connect, marketplace, resorce index
        type: "text",
        description: "text",
        quantity_goal: "int",
        points: "decimal",
        url: "text",
        tip: "text",
        help: "text"
    },
    key:[["id"], "game", "level", "group", "type"]
}
