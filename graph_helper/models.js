

module.exports.insertQuery = function(modelProperties, label){

    // 'g.addV(label, "discussion", "user_id", user_id, "dis_id", dis_id );';

    var query = 'g.addV(label, "'+label+'" ';
    for (var name in modelProperties) {

        if(typeof modelProperties[name] == 'string' || typeof modelProperties[name] == 'object'){

            query += ', "'+name+'", ' + '"' + modelProperties[name] + '"';

        }else{

            query += ', "'+name+'", ' + modelProperties[name];
        }
    }
    query += ');';

    return query;
};

/*
 * Recibe un query del vertex a actualizar
 */
module.exports.updatePropertiesQuery = function(query, modelProperties){

    for (var name in modelProperties) {

        if(typeof modelProperties[name] == 'string' || typeof modelProperties[name] == 'object'){

            query += '.property("'+name+'", "'+modelProperties[name]+'" )';

        }else{

            query += '.property("'+name+'", '+modelProperties[name]+')';
        }
    }
    query += '.iterate()';

    return query;
};
