const session = require('./session');
const notification = require('./notification');
const notificationsController = require('../controllers/notifications');
const messagesController = require('../controllers/messages');
const threadTypes = require('../constants/threadTypes');
const journalistsController = require('../controllers/graph/journalists');
const activitiesController = require('../controllers/activities');
const connectionsController = require('../controllers/connections');
const mandrillExpert = require('../experts/mandrill.js');
const auth = require('../controllers/sessionControl');

module.exports = function(io) {

    this.io = io;

    io.use(function(socket, next){

        var emitOnLine = function(err, result){

            if(err){
                console.log(err);
                return;
            }

            var socketSessionsByUser;

            for(var i = 0; i < result.length; i++){
                if(result[i].single_user_target != 0){

                    socketSessionsByUser = session.sessions()[result[i].single_user_target];

                    /* Obtener todas las sessiones de cada usuario y hacer emit a cada una */
                    for (var index in socketSessionsByUser) {
                        var socketSession = socketSessionsByUser[index];
                        if (socketSession != null) {
                            io.to(socketSession).emit('on-line', result[i].user);
                        }
                    }

                }
            }
        };

        if(parseInt(socket.handshake.query.user) > 0){

            console.log(' SOCKET IO INICIANDO SESSION PARA ' + socket.handshake.query.user);

            session.add({session: socket.id, user: parseInt(socket.handshake.query.user)});
            messagesController.getThreadOnlyByUser(parseInt(socket.handshake.query.user), emitOnLine);
            notification.iniNotifications(socket.id, parseInt(socket.handshake.query.user));

        }else{
            console.log(' SOCKET IO INICIANDO SIN SESSION ');
        }

        return next();
    });

    io.on('connection', function(socket) {

        socket.on('disconnect', function () {

            var emitOffLine = function(err, result){

                if(err){
                    console.log(err);
                    return;
                }

                var socketSessionsByUser;

                for(var i = 0; i < result.length; i++){

                    if(result[i].single_user_target != 0){

                        socketSessionsByUser = session.sessions()[result[i].single_user_target];

                        /* Obtener todas las sessiones de cada usuario y hacer emit a cada una */
                        for (var index in socketSessionsByUser) {
                            var socketSession = socketSessionsByUser[index];
                            if (socketSession != null) {
                                io.to(socketSession).emit('off-line', result[i].user);
                            }else{
                                console.log("EMIT ERROR");
                            }
                        }

                    }
                }
            };

            if(parseInt(socket.handshake.query.user) > 0){
                messagesController.getThreadOnlyByUser(parseInt(socket.handshake.query.user), emitOffLine);
            }

            session.remove({session: socket.id, user: parseInt(socket.handshake.query.user)});
        });

        const _sendDelivered = function(usersSenders){

            for(var us in usersSenders){

                var socketSessionsByUser = session.sessions()[usersSenders[us].user_id];

                /* Obtener todas las sessiones de cada usuario y hacer emit a cada una */
                for (var index in socketSessionsByUser) {
                    var socketSession = socketSessionsByUser[index];
                    if (socketSession != null) {
                        io.to(socketSession).emit('last_status', {thread_id: usersSenders[us].thread_id,
                            last_status: 2});
                    }
                }
            }
        }

        var _emitNotifications = function(usersToNotify, notification){
console.log('ADENTRO DE socket callback');
            var userSessions;

            for(var i=0; i<usersToNotify.length; i++){
                userSessions = session.sessions()[usersToNotify[i].user_id];
                if( userSessions ){
                    // Hacer emit a cada una de los dispositivos activos del usuario.
                    for(var x in userSessions){
                        // io.to(userSessions[x]).emit('new-notification', notification);
                    }
                    console.log('USUARIO ONLINE');
                    mandrillExpert.emailNotifications(notification, usersToNotify[i]);
                }else{
                    console.log('USUARIO OFF enviando email');
                    // Usuario no está online, enviar email.
                    mandrillExpert.emailNotifications(notification, usersToNotify[i]);
                }
            }
        };

        socket.on('welcome-message', function (data) {

            data.users.unshift(445);
            data.names.unshift('Joe from Kulectiv');
            data.pics.unshift('445.jpg');
            data.locations.unshift('San Francisco, CA');
            data.timestamp      = Date.now();
            data.sender_id      = 445;
            data.sender_name    = "Kulectiv support";
            data.message        = "Hey " + data.names[1] + ", \n\n" +
                                  "Welcome to Kulectiv! My name is Joe and I’m here to assist you with any inquires.\n\n" +
                                  "You’ve just accessed your inbox where you may chat with journalists, media companies, and the team at Kulectiv.\n\n" +
                                  "To make full use of your inbox, let’s make sure you’re visible to other journalists by completing your tasks in the collaborate stack.\n" +
                                  "<a href='/connect' style='color: blue'>Click here to be directed to the collaborate stack.</a>\n\n" +
                                  "Please let me know if you have any questions.\n\n" +
                                  "Best,\n" +
                                  "Joe";

            // data.message        = "Hey " + data.names[1] + ", \n\n" +
            //                       "Welcome to your inbox. You may chat with other journalists from here.\n\n" +
            //                       "To get started, let's make sure you're visible to other journalists in the collaborate section by completing the required tasks.\n" +
            //                       "<a href='/connect' style='color: blue'>Click here to be directed to the collaborate.</a>\n\n" +
            //                       "Otherwise, if you have any questions, please chat with me.\n\n" +
            //                       "Best,\n" +
            //                       "Joe";

            data.subject        = "";
            data.type           = threadTypes.SUPPORT;

            _newThread(data);

            setTimeout(function(){

                // Prevent Mo receiving his welcome message
                if(data.users[1] == 358) return;

                data.users[0] = 358;
                data.names[0] = 'Mo';
                data.pics[0] = '358.jpg';
                data.timestamp      = Date.now();
                data.sender_id      = 358;
                data.sender_name    = "Mo";
                data.message        = "Hey "+data.names[1]+",\n\nI’m Mo, one of Kulectiv’s founder. I’m very excited to have you try out our new approach!\n\n"
                +"Let me know how we can improve Kulectiv, by messaging me or “Joe from Kulectiv” directly. I’m shaping " +
                    "Kulectiv to suit your needs and I can’t do it alone.\n\nWelcome aboard "+data.names[1]+" and be sure to check out " +
                    "your <a href='/dashboard' style='color: blue'>dashboard</a> for tips, updates, and new tasks!";
                data.subject        = "";
                data.type           = threadTypes.SUPPORT;

                _newThread(data);

            }.bind(this),300000);

        });


        function _taskResourceFeedback(data){
            if(messagesController.isThreadTaskMarketplaceFeedback( message.sender_id, message.users[1], evaluateResult)){

            }
        }

        function _validateMessage(data){
            return data
            && data.users
            && data.names
            && data.pics
            && data.locations
            && data.message
            && data.users.length >= 2
            && data.users.length == data.names.length
            && data.users.length == data.pics.length
            && data.users.length == data.locations.length;
        }

        function _newMessageHandler(message){

            /*validate*/
            if(!_validateMessage(message)){
                console.log('Error al enviar mensaje inválido');
                console.log(message);
                return;
            }

            if(message.thread_id) {
                _newMessage(message);
                return;

            }else if(message.users.length > 2) {
                /* La charla es entre dos personas y busca el thread id de ese thread */
                /* evaluateResult revisa si se encontró el thread y llama a la funcion que corresponda*/
                _newThread(message);
                return;
            }

            var evaluateResult = function(err, result){
                if(result.length){

                    message.thread_id = result[0].thread_id;
                    _newMessage(message);

                }else
                    _newThread(message);
            };

            if(!message.thread_id) {
                /* El thread no fue encontrado y la cantidad de destinatarios es mayor a uno:
                 Se envía, se guarda el new-message; se crean y se envían los threads */
                messagesController.getThreadByUser( message.sender_id, message.users[1], message.type, evaluateResult);
            }
        }

        /*
         * Busca al menos una session activa del usuario en socket.
         */
        function _isUserOnline(userID){
            const socketSessionsByUser = session.sessions()[userID];

            for (var i in socketSessionsByUser) {
                if (socketSessionsByUser[i] != null) return true;
            }
            return false;
        }


        function _newMessage(message){

            if(!message.users){
                throw new Error('No hay usuarios en el mensaje');
            }

            /* Construir array de threads uno para cada usuario del thread */
            var threadsMessage = messagesController.makeMessages(message);
            var isDeliveredArray = [];
            var isDeliveredThread = true;

            /* Emitir new-message a todas las sesiones de cada usuario del thread */
            for (var i = 0; i < message.users.length; i++) {

                var isDelivered = false;
                var user_id = message.users[i];
                var socketSessionsByUser = session.sessions()[user_id];

                for (var index in socketSessionsByUser) {

                    if (socketSessionsByUser[index] != null){
                        isDelivered = true;
                        io.to(socketSessionsByUser[index]).emit('new-message', threadsMessage.message);
                    }
                }

                // Si el usuario no tiene sesiones activas, mandar email.
                if(!socketSessionsByUser || socketSessionsByUser.length == 0){

                    mandrillExpert.sendEmailMessage(message, i);
                }

                isDeliveredArray.push(isDelivered);

                if(!isDelivered){
                    isDeliveredThread = false;
                }
            }

            var socketSessionsByUser = session.sessions()[message.sender_id];

            for (var index in socketSessionsByUser) {

                if (socketSessionsByUser[index] != null){
                    io.to(socketSessionsByUser[index]).emit('last_status', {thread_id: threadsMessage.message.thread_id,
                        last_status: isDeliveredThread ? 2 : 1});
                }
            }

            /* Guardar en Cassandra */
            messagesController.newMessage(threadsMessage, isDeliveredArray, isDeliveredThread);
        }

        /*
         * Emite new-thread para las sesiones de cada usuario y guarda en Cassandra
         */
        function _newThread(message){

            /* Construir array de threads uno para cada usuario del thread */
            var threadsMessage = messagesController.makeThreads(message);

            var isDeliveredArray = [];
            var isSent = true;

            for (var i = 0; i < message.users.length; i++) {

                var user_id = message.users[i];
                var socketSessionsByUser = session.sessions()[user_id];
                var newThread = threadsMessage.threads[i];
                var isDelivered = false;

                /* Obtener todas las sessiones de cada usuario y hacer emit a cada una */
                for (var index in socketSessionsByUser) {

                    var socketSession = socketSessionsByUser[index];
                    if (socketSession != null) {
                        isDelivered = true;

                        if(newThread.single_user_target){
                            newThread.online = session.getByUser(newThread.single_user_target);
                        }

                        io.to(socketSession).emit('new-thread', newThread);
                    }

                }
                if(i != 0){
                    isDeliveredArray.push(isDelivered);
                }
                if(!isDelivered){
                    isSent = false;
                }
            }

            var socketSessionsByUser = session.sessions()[threadsMessage.threads[0].user];

            /* Obtener todas las sessiones de cada usuario y hacer emit a cada una */
            for (var index in socketSessionsByUser) {

                var socketSession = socketSessionsByUser[index];
                if (socketSession != null) {
                    io.to(socketSession).emit('last_status', {thread_id: threadsMessage.threads[0].thread_id, last_status: isSent ? 2 : 1});
                }
            }

            /* Guarda en Cassandra */
            messagesController.newThread(threadsMessage, isDeliveredArray, isSent);
        }


        /*******************************************************************************/
        // Messages
        /*******************************************************************************/

        socket.on('new-message', function (message) {

            var validSession = _verifySession(socket.id, socket.handshake.headers);
            if (!validSession) { return }

            _newMessageHandler(message);
        });

        socket.on('delivered-all', function (user_id) {

            var validSession = _verifySession(socket.id, socket.handshake.headers);
            if (!validSession) { return }

            messagesController.deliveryAll(user_id, _sendDelivered);
        });

        socket.on('read-all', function (data) {

            var validSession = _verifySession(socket.id, socket.handshake.headers);
            if (!validSession) { return }

            messagesController.readAll(data, _sendRead);
        });

        /*******************************************************************************/
        // Activity - Notifications
        /*******************************************************************************/

        socket.on('log-activity', function (jsonActivity) {

            var validSession = _verifySession(socket.id, socket.handshake.headers);
            if (!validSession) { return }

            var activity = JSON.parse(jsonActivity);
            activitiesController.logActivity(activity, _emitNotifications, socket.handshake.headers);
        });

        socket.on('read-notification', function (jsonNotification) {

            var validSession = _verifySession(socket.id, socket.handshake.headers);
            if (!validSession) { return }

            var notification = JSON.parse(jsonNotification);
            notificationsController.read(notification);
        });

        /*******************************************************************************/
        // Gamification
        /*******************************************************************************/

        socket.on('task-marketplace-feedback', function (data) {

            var validSession = _verifySession(socket.id, socket.handshake.headers);
            if (!validSession) { return }

            var _sendSupportMessage = function(err, result){

                if(err){
                    console.log(err);
                }

                //Verify if the taskResourceFeedback thread exist
                if(result.length != 0) {
                    return;
                }

                data.users.unshift(1);
                data.names.unshift('Kulectiv support');
                data.pics.unshift('default.png');
                data.locations.unshift('Everywhere');
                data.timestamp      = Date.now();
                data.sender_id      = 1;
                data.sender_name    = "Kulectiv support";
                data.message        = "Hey "+data.names[1]+", we would like to expand the Resource Stack. What resources do you need?" +
                    " If not, what were you expecting to find?";
                data.subject        = "";
                data.type           = "TASK_MARKETPLACE_FEEDBACK";

                _newThread(data);

            };

            messagesController.getThreadByUser(1, data.users[0], data.type, _sendSupportMessage);
        });

        socket.on('task-resource-feedback', function (data) {

            var validSession = _verifySession(socket.id, socket.handshake.headers);
            if (!validSession) { return }

            var _sendSupportMessage = function(err, result){

                if(err){
                    console.log(err);
                }

                //Verify if the taskResourceFeedback thread exist
                if(result.length != 0) {
                    return;
                }

                data.users.unshift(446);
                data.names.unshift('Kulectiv support');
                data.pics.unshift('default.png');
                data.locations.unshift('Everywhere');
                data.timestamp      = Date.now();
                data.sender_id      = 446;
                data.sender_name    = "Kulectiv support";
                data.message        = "Hey "+data.names[1]+", did you find what you were looking for in the Resource Stack?" +
                    " If not, what were you expecting to find?";
                data.subject        = "";
                data.type           = "TASK_RESOURCE_FEEDBACK";

                _newThread(data);

            };

            //446 is jpablo.vaz@gmail.com user (change for user support id)
            messagesController.getThreadByUser(446, data.users[0], data.type, _sendSupportMessage);//data.user_id, _sendSupportMessage

        });

        /*******************************************************************************/
        // Engage
        /*******************************************************************************/

        socket.on('connection-requested', function (message) {

            var validSession = _verifySession(socket.id, socket.handshake.headers);
            if (!validSession) { return }

            _newMessageHandler(message);
            connectionsController.requested(message);
        });


        socket.on('connection-accepted', function (params) {

            var validSession = _verifySession(socket.id, socket.handshake.headers);
            if (!validSession) { return }

            const users = [params.user_id, params.other_user_id[0]];
            connectionsController.accepted(params);
        });

        socket.on('connection-rejected', function (params) {

            var validSession = _verifySession(socket.id, socket.handshake.headers);
            if (!validSession) { return }

            const users = [params.user_id, params.other_user_id[0]];
            connectionsController.rejected(params);
        });

        const _sendRead = function(threadID, sender){

            const socketSessionsByUser = session.sessions()[sender];
            /* Obtener todas las sessiones de cada usuario y hacer emit a cada una */
            for (var index in socketSessionsByUser) {
                var socketSession = socketSessionsByUser[index];
                if (socketSession != null) {
                    io.to(socketSession).emit('last_status', {thread_id: threadID, last_status: 3});
                }
            }
        }



    });

    exports.io = io;
}


const _verifySession = function(socketId, headers){

    // verify socket.id
    if (!socketId) {
        return false;
    }

    // verify headers
    if (!headers || !headers.cookie) {
        this.io.to(socketId).emit('session-expired');
        return false;
    }

    // get the KOASESSID value
    var index = headers.cookie.indexOf('KOASESSID=');
    if (index < 0) {

        this.io.to(socketId).emit('session-expired');
        return false;

    }else {

        // validate KOASESSID value
        var subTemp = headers.cookie.substring(index);
        var index2 = subTemp.indexOf(';');
        var sessionStr = subTemp.substring(0, index2);
        var sessionArray = sessionStr.split('=');
        var koaSessionID = sessionArray[1];

        var jwtObject = auth.getJWTObject(koaSessionID);

        if (!jwtObject) {
            this.io.to(socketId).emit('session-expired');
            return false;
        }
    }

    return true;
}
