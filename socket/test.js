var models = require('express-cassandra');



exports.clear = function(id){
    models.instance.Notification.delete({myuser_id: id},
        function(err){
            console.log(err);
            console.log("Borré todo..!!");
            console.log("Borré todo..!!");
        });
}

exports.readLast = function(){


    models.instance.MessageLast.find(
        {},{raw:true},
        function(err,rows){
            console.log("last messages: "+rows.length);
            for(var i = 0; i < rows.length; i++){
                console.log("------------------------------------------------------");
                console.log("user: "+rows[i].user);
                console.log("uuid time: "+rows[i].last_message_timestamp);
                console.log("uuid: "+rows[i].other_user);
                console.log("read: "+rows[i].last_message);
                console.log("type: "+rows[i].unread);
                console.log("------------------------------------------------------");
            }
        });

}

exports.readMessage = function(){

    models.instance.Message.find(
        {},{raw:true},
        function(err,rows){
            console.log("messages: "+rows.length);
            for(var i = 0; i < rows.length; i++){
                console.log("======================================================");
                console.log("user: "+rows[i].id);
                console.log("uuid time: "+rows[i].timestamp);
                console.log("uuid: "+rows[i].sender_id);
                console.log("read: "+rows[i].target_id);
                console.log("type: "+rows[i].message);
                console.log("======================================================");
            }
        });

}

exports.saveLastMessages = function(){

    console.log("haciendo save...!!!!!!!!!   gfdsgfd");

    var dd = Number(new Date());

    models.instance.MessageLast.update({user: 52, other_user: 352, last_message_timestamp: dd, last_message: "Ultimo mensaje"},{unread:1},function(err){
        if(err) console.log(err);
        console.log("jojojo");
    });
}

exports.saveMessages = function(){

    console.log("haciendo save...!!!!!!!!!   Messages");

    var dd =models.timeuuid(new Date());


    /*
    models.instance.Message.update({id:"52:352", timestamp:dd, sender_id: 52, target_id: 352, message: "hola que tal?"},function(err){
        if(err){
            console.log("esto no estaría funcionando");
            console.log(err);
        }
        console.log("jijiji");
    });
     */

    var message = new models.instance.Message({id:"52:352", timestamp:dd, sender_id: 352, target_id: 52, message: "Otro, nos vemos.!"});
    message.save(function(err){
        if(err){
            console.log(err);
            console.log("ERROR CASSANDRA");
        }
        else console.log('Yuppiie!');
    });
}

exports.iniTest = function(){

    var moreDate = new Date();
    moreDate.setDate(moreDate.getDate() - 30);
    var moreUuid = models.timeuuid(moreDate);

    var lessDate = new Date();
    lessDate.setDate(lessDate.getDate() - 17);
    var lessUuid = models.timeuuid(lessDate);

    var dayDate = new Date();
    dayDate.setDate(dayDate.getDate() - 1);
    var dayUuid = models.timeuuid(dayDate);

    // Now timeOut -> models.timeuuid(new Date())


    // User: 32 - Read: True - Badge: 11 - activiti_id: moreUuid  ==  Void
    var john = new models.instance.Notification({myuser_id: 32, read: true, activity_id: moreUuid, read_time: null, type: "11"});
    john.save(function(err){
        if(err){
            console.log(err);
            console.log("ERROR CASSANDRA");
        }
        else console.log('Yuppiie!');
    });

    // User: 32 - Read: False - Badge: 11 - activiti_id: moreUuid  ==  Void
    john = new models.instance.Notification({myuser_id: 32, read: false, activity_id: moreUuid, read_time: null, type: "11"});
    john.save(function(err){
        if(err) console.log(err);
        else console.log('Yuppiie!');
    });

    // User: 32 - Read: False - Badge: 11 - activiti_id: lessUuid  ==  Void
    john = new models.instance.Notification({myuser_id: 32, read: false, activity_id: lessUuid, read_time: null, type: "11"});
    john.save(function(err){
        if(err) console.log(err);
        else console.log('Yuppiie!');
    });



    // User: 52 - Read: True - Badge: 15 - activiti_id: moreUuid  ==  Void
    john = new models.instance.Notification({myuser_id: 52, read: true, activity_id: moreUuid, read_time: null, type: "15"});
    john.save(function(err){
        if(err) console.log(err);
        else console.log('Yuppiie!');
    });

    // User: 52 - Read: True - Badge: 15 - activiti_id: lessUuid  ==  Void
    john = new models.instance.Notification({myuser_id: 52, read: true, activity_id: lessUuid, read_time: null, type: "15"});
    john.save(function(err){
        if(err) console.log(err);
        else console.log('Yuppiie!');
    });

    // User: 52 - Read: False - Badge: 15 - activiti_id: moreUuid  ==  Void
    john = new models.instance.Notification({myuser_id: 52, read: false, activity_id: moreUuid, read_time: null, type: "15"});
    john.save(function(err){
        if(err) console.log(err);
        else console.log('Yuppiie!');
    });

    // User: 52 - Read: False - Badge: 15 - activiti_id: lessUuid  ==  User 52 - Message +1
    john = new models.instance.Notification({myuser_id: 52, read: false, activity_id: lessUuid, read_time: null, type: "15"});
    john.save(function(err){
        if(err) console.log(err);
        else console.log('Yuppiie!');
    });

    // User: 52 - Read: False - Badge: 15 - activiti_id: dayUuid  ==  User 52 - Message +1
    john = new models.instance.Notification({myuser_id: 52, read: false, activity_id: dayUuid, read_time: null, type: "15"});
    john.save(function(err){
        if(err) console.log(err);
        else console.log('Yuppiie!');
    });




    // User: 352 - Read: True - Badge: 10 - activiti_id: moreUuid  ==  Void
    john = new models.instance.Notification({myuser_id: 352, read: true, activity_id: moreUuid, read_time: null, type: "10"});
    john.save(function(err){
        if(err) console.log(err);
        else console.log('Yuppiie!');
    });

    // User: 352 - Read: True - Badge: 10 - activiti_id: lessUuid  ==  Void
    john = new models.instance.Notification({myuser_id: 352, read: true, activity_id: lessUuid, read_time: null, type: "10"});
    john.save(function(err){
        if(err) console.log(err);
        else console.log('Yuppiie!');
    });

    // User: 352 - Read: False - Badge: 10 - activiti_id: moreUuid  ==  Void
    john = new models.instance.Notification({myuser_id: 352, read: false, activity_id: moreUuid, read_time: null, type: "10"});
    john.save(function(err){
        if(err) console.log(err);
        else console.log('Yuppiie!');
    });

    // User: 352 - Read: False - Badge: 10 - activiti_id: lessUuid  ==  User 352 - Message +1
    john = new models.instance.Notification({myuser_id: 352, read: false, activity_id: lessUuid, read_time: null, type: "10"});
    john.save(function(err){
        if(err) console.log(err);
        else console.log('Yuppiie!');
    });

    // User: 352 - Read: False - Badge: 10 - activiti_id: dayUuid  ==  User 352 - Message +1
    john = new models.instance.Notification({myuser_id: 352, read: false, activity_id: dayUuid, read_time: null, type: "10"});
    john.save(function(err){
        if(err) console.log(err);
        else console.log('Yuppiie!');
    });







    var sameUuid = models.timeuuid(new Date());
    // User: 52 - Read: False - Badge: 101 - activiti_id: dayUuid  ==  User 52 - Message +1
    john = new models.instance.Notification({myuser_id: 52, read: false, activity_id: sameUuid, read_time: null, type: "101"});
    john.save(function(err){
        if(err) console.log(err);
        else console.log('Yuppiie!');
    });
    // User: 52 - Read: False - Badge: 102 - activiti_id: dayUuid  ==  User 52 - Message +1
    john = new models.instance.Notification({myuser_id: 52, read: false, activity_id: sameUuid, read_time: null, type: "102"});
    john.save(function(err){
        if(err) console.log(err);
        else console.log('Yuppiie!');
    });
    // User: 52 - Read: False - Badge: 103 - activiti_id: dayUuid  ==  User 52 - Message +1
    john = new models.instance.Notification({myuser_id: 52, read: false, activity_id: sameUuid, read_time: null, type: "103"});
    john.save(function(err){
        if(err) console.log(err);
        else console.log('Yuppiie!');
    });


    // User: 352 - Read: False - Badge: 101 - activiti_id: dayUuid  ==  User 352 - Message +1
    john = new models.instance.Notification({myuser_id: 352, read: false, activity_id: sameUuid, read_time: null, type: "101"});
    john.save(function(err){
        if(err) console.log(err);
        else console.log('Yuppiie!');
    });
    // User: 352 - Read: False - Badge: 102 - activiti_id: dayUuid  ==  User 352 - Message +1
    john = new models.instance.Notification({myuser_id: 352, read: false, activity_id: sameUuid, read_time: null, type: "102"});
    john.save(function(err){
        if(err) console.log(err);
        else console.log('Yuppiie!');
    });
    // User: 352 - Read: False - Badge: 103 - activiti_id: dayUuid  ==  User 352 - Message +1
    john = new models.instance.Notification({myuser_id: 352, read: false, activity_id: sameUuid, read_time: null, type: "103"});
    john.save(function(err){
        if(err) console.log(err);
        else console.log('Yuppiie!');
    });



    setTimeout(function(){
        var nowUuid = models.timeuuid(new Date());
        // User: 52 - Read: False - Badge: 15 - activiti_id: dayUuid  ==  User 52 - Message +1
        john = new models.instance.Notification({myuser_id: 52, read: false, activity_id: nowUuid, read_time: null, type: "15"});
        john.save(function(err){
            if(err) console.log(err);
            else console.log('Yuppiie!');
        });
        nowUuid = models.timeuuid(new Date());
        // User: 352 - Read: False - Badge: 10 - activiti_id: dayUuid  ==  User 352 - Message +1
        john = new models.instance.Notification({myuser_id: 352, read: false, activity_id: nowUuid, read_time: null, type: "10"});
        john.save(function(err){
            if(err) console.log(err);
            else console.log('Yuppiie!');
        });



        var sameUuid = models.timeuuid(new Date());
        // User: 52 - Read: False - Badge: 101 - activiti_id: dayUuid  ==  User 52 - Message +1
        john = new models.instance.Notification({myuser_id: 52, read: false, activity_id: sameUuid, read_time: null, type: "101"});
        john.save(function(err){
            if(err) console.log(err);
            else console.log('Yuppiie!');
        });
        // User: 52 - Read: False - Badge: 102 - activiti_id: dayUuid  ==  User 52 - Message +1
        john = new models.instance.Notification({myuser_id: 52, read: false, activity_id: sameUuid, read_time: null, type: "102"});
        john.save(function(err){
            if(err) console.log(err);
            else console.log('Yuppiie!');
        });
        // User: 52 - Read: False - Badge: 103 - activiti_id: dayUuid  ==  User 52 - Message +1
        john = new models.instance.Notification({myuser_id: 52, read: false, activity_id: sameUuid, read_time: null, type: "103"});
        john.save(function(err){
            if(err) console.log(err);
            else console.log('Yuppiie!');
        });


        // User: 352 - Read: False - Badge: 101 - activiti_id: dayUuid  ==  User 352 - Message +1
        john = new models.instance.Notification({myuser_id: 352, read: false, activity_id: sameUuid, read_time: null, type: "101"});
        john.save(function(err){
            if(err) console.log(err);
            else console.log('Yuppiie!');
        });
        // User: 352 - Read: False - Badge: 102 - activiti_id: dayUuid  ==  User 352 - Message +1
        john = new models.instance.Notification({myuser_id: 352, read: false, activity_id: sameUuid, read_time: null, type: "102"});
        john.save(function(err){
            if(err) console.log(err);
            else console.log('Yuppiie!');
        });
        // User: 352 - Read: False - Badge: 103 - activiti_id: dayUuid  ==  User 352 - Message +1
        john = new models.instance.Notification({myuser_id: 352, read: false, activity_id: sameUuid, read_time: null, type: "103"});
        john.save(function(err){
            if(err) console.log(err);
            else console.log('Yuppiie!');
        });




    }.bind(this),15000);

    setTimeout(function(){
        var nowUuid = models.timeuuid(new Date());
        // User: 52 - Read: False - Badge: 15 - activiti_id: dayUuid  ==  User 52 - Message +1
        john = new models.instance.Notification({myuser_id: 52, read: false, activity_id: nowUuid, read_time: null, type: "15"});
        john.save(function(err){
            if(err) console.log(err);
            else console.log('Yuppiie!');
        });
        nowUuid = models.timeuuid(new Date());
        // User: 352 - Read: False - Badge: 10 - activiti_id: dayUuid  ==  User 352 - Message +1
        john = new models.instance.Notification({myuser_id: 352, read: false, activity_id: nowUuid, read_time: null, type: "10"});
        john.save(function(err){
            if(err) console.log(err);
            else console.log('Yuppiie!');
        });
    }.bind(this),30000);

    setTimeout(function(){
        var nowUuid = models.timeuuid(new Date());
        // User: 52 - Read: False - Badge: 15 - activiti_id: dayUuid  ==  User 52 - Message +1
        john = new models.instance.Notification({myuser_id: 52, read: false, activity_id: nowUuid, read_time: null, type: "15"});
        john.save(function(err){
            if(err) console.log(err);
            else console.log('Yuppiie!');
        });
        nowUuid = models.timeuuid(new Date());
        // User: 352 - Read: False - Badge: 10 - activiti_id: dayUuid  ==  User 352 - Message +1
        john = new models.instance.Notification({myuser_id: 352, read: false, activity_id: nowUuid, read_time: null, type: "10"});
        john.save(function(err){
            if(err) console.log(err);
            else console.log('Yuppiie!');
        });
    }.bind(this),45000);


    setTimeout(function(){
        var nowUuid = models.timeuuid(new Date());
        // User: 52 - Read: False - Badge: 15 - activiti_id: dayUuid  ==  User 52 - Message +1
        john = new models.instance.Notification({myuser_id: 52, read: false, activity_id: nowUuid, read_time: null, type: "203"});
        john.save(function(err){
            if(err) console.log(err);
            else console.log('Yuppiie!');
        });
        nowUuid = models.timeuuid(new Date());
        // User: 352 - Read: False - Badge: 10 - activiti_id: dayUuid  ==  User 352 - Message +1
        john = new models.instance.Notification({myuser_id: 352, read: false, activity_id: nowUuid, read_time: null, type: "205"});
        john.save(function(err){
            if(err) console.log(err);
            else console.log('Yuppiie!');
        });
        nowUuid = models.timeuuid(new Date());
        // User: 52 - Read: False - Badge: 15 - activiti_id: dayUuid  ==  User 52 - Message +1
        john = new models.instance.Notification({myuser_id: 52, read: false, activity_id: nowUuid, read_time: null, type: "207"});
        john.save(function(err){
            if(err) console.log(err);
            else console.log('Yuppiie!');
        });
        nowUuid = models.timeuuid(new Date());
        // User: 352 - Read: False - Badge: 10 - activiti_id: dayUuid  ==  User 352 - Message +1
        john = new models.instance.Notification({myuser_id: 352, read: false, activity_id: nowUuid, read_time: null, type: "206"});
        john.save(function(err){
            if(err) console.log(err);
            else console.log('Yuppiie!');
        });
    }.bind(this),45000);




    models.instance.Notification.find(
        {},{raw:true},
        function(err,rows){
            for(var i = 0; i < rows.length; i++){
                console.log("------------------------------------------------------");
                console.log("user: "+rows[i].myuser_id);
                console.log("uuid time: "+rows[i].activity_id.getDate());
                console.log("uuid: "+rows[i].activity_id.toString());
                console.log("read: "+rows[i].read_time);
                console.log("type: "+rows[i].type);
                console.log("------------------------------------------------------");
            }
            console.log(rows.length);
        });



}