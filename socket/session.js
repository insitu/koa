//  Indexado por usuario las sessions  >>  Notifications & Message: Envío a sessiones por usuario
var sessions = new Array(); // [user][session]
//  Array de int id_usuario  >>  Query Notifications
var users = [];

exports.add = function(connection){

    if(!connection.user)
        return;

    connection.user = parseInt(connection.user);

    //Array de sessiones de usuairo indexadas por id de usuario.
    if(sessions[connection.user] == null){
        sessions[connection.user] = new Array();
    }
    sessions[connection.user].push(connection.session);

    // Actualizar el array de ids de usuarios en session
    users.push(connection.user);
}

exports.remove = function(connection){

    // recorre las sessiones del usuario en caso de tener mas de una y la elimina,
    // luego elimina el usuario si no hay mas sessiones
    for(var e in sessions[connection.user]){

        if(sessions[connection.user][e] == connection.session){
            sessions[connection.user].splice(e, 1);

            if(sessions[connection.user].length == 0){
                delete sessions[connection.user];
            }
        }
    }

    // Actualizar el array de ids de usuarios en session
    var index = users.indexOf(connection.user); // Esto no funciona en IE<9
    if (index > -1) {
        users.splice(index, 1);
    }
}

exports.getByUser = function(user_id){
    return sessions[user_id] != null;
}

exports.users = function(){
    return users;
}

exports.sessions = function(){
    return sessions;
}
