var models = require('express-cassandra');
var index = require('./index');

exports.iniNotifications = function(session_id, user_id){

    // Notifications within the last 21 days
    var iniDate = new Date();
    iniDate.setDate(iniDate.getDate() - 21);
    var initLastNotification = models.timeuuid(iniDate);

    //Find in database
    models.instance.Activity_Notification_Counter.find( {user_id: user_id}, {raw:true}, function(err,rows){
        // send.iniSend(session_id, err, rows);
        index.io.to(session_id).emit('init-notifications', rows);
    });
}


// exports.sendMessage = function(uss, message, on) {
//     send.sendMessage(uss,message,on);
// }
