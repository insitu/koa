const koa = require('koa');
const http = require('http');
const enforceHttps = require('koa-sslify');
const app = koa();

app.name = 'Kulectiv_Client';

// Force HTTPS on all page
app.use(enforceHttps());

http.createServer(app.callback()).listen(80);
