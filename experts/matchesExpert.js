"use strict";
const models = require('express-cassandra');
const graphConnections = require('../controllers/graph/graphConnections');
const graphClient = graphConnections.getClient();
const tagConstants = require('../constants/tagConstants.js');

const DISCUSSION = 'DISCUSSION';
const GRANT = 'GRANT';
const ITEM = 'ITEM';
const NGO = 'NGO';


/*
 * Busca los usuarios relacionados al tag de una discussion. Calcula un valor para esa relación en
 * base al typo y level. Incrementa la recomendacion del objeto para ese usuario en el valor calculado.
 * Cuando una discussion deja de relacionarse con un tag, se elimina / decrementan sus recomendaciones.
 */
module.exports.matchDiscussion = function *(tagName, objDiscussionModel, discussionOwnerUserID, remove){

    tagName = tagName.toLowerCase();
    // buscar todos los Edges que relacionan un usuario con el Tag
    // Puedo tener mas de un edge para el mismo tag-usuario, distinto level.
    const query = 'g.V().hasId("{~label=tag, name='+tagName+'}").inE("has")';
    var edgesResult = yield function(graphCallback) {
        graphClient.executeGraph(query, graphCallback);
    }

    // segun el TIPO y LEVEL del "has" del usuario, le voy a calcular el valor numerico a la relacion
    // Ese valor se lo voy a incrementar al contador de la recomendacion del objeto para ese usuario
    var valueIncrement = 0;
    var keyValues;
    edgesResult.forEach(function(edge) {

        // Prevent recommendation of your own posts
        if(edge.outV.user_id == discussionOwnerUserID) return;

        valueIncrement = _calculateRelationValue(edge.properties.type, edge.properties.level)

        keyValues = {
            user_id: edge.outV.user_id,
            type: DISCUSSION,
            object_timestamp: objDiscussionModel.timestamp,
            object_id: JSON.stringify(objDiscussionModel.id)
        }

        // console.log('------------------------------------------------------------');
        // console.log('');
        // console.log(objDiscussionModel);
        // console.log('');
        // console.log('------------------------------------------------------------');

        keyValues.country_short = objDiscussionModel.country_short ? objDiscussionModel.country_short : '*';
        keyValues.adm_1_short = objDiscussionModel.adm_1_short ? objDiscussionModel.adm_1_short : '*';
        keyValues.locality_short = objDiscussionModel.locality_short ? objDiscussionModel.locality_short : '*';

        // Eliminar / decrementar la recomendacion
        if(remove){
            valueIncrement = valueIncrement * -1;
        }

        valueIncrement = models.datatypes.Long.fromInt(valueIncrement);

        // console.log('------------------------------------------------------------');
        // console.log('');
        // console.log('UPDATE Match_Counter con ', valueIncrement);
        // console.log(keyValues);
        // console.log('');
        // console.log('------------------------------------------------------------');

        // Validate primary key values
        for(var key in keyValues){
             if(!keyValues[key]){
                 console.log('Intentando escribir en Match_Counter con keyValues iválidos: ');
                 console.log(keyValues);
                 return;
             }
        }

        models.instance.Match_Counter.update(keyValues, {counter_weight: valueIncrement}, function(err){
            if(err) console.log(err);
        });
    });
}


module.exports.saveMatch = function(updateFields, primaryFields){
    models.instance.Match.update( primaryFields, updateFields, upsertCallback);
}

module.exports.rank = function(matchCounters){

    for (var i = 0; i < matchCounters.length; i++) {
        matchCounters[i].rank = _calculateHotValue(matchCounters[i].counter_weight, matchCounters[i].object_timestamp);
    }
    matchCounters.sort(_compare);
    return matchCounters;
}

/*
 * last_update: "2017-07-18T17:32:34.164Z"
 * tags_info: ["{"name":"skills","type":"TYPE_REQUEST","timestamp":1500399154164,"owner":false}"]
 * target_adm_1_short: ""
 * target_country_short: "AR"
 * target_location: "Mendoza Province, Argentina"
 * target_name: "Un loco"
 * target_picture: ""
 * target_position: ""
 * target_user_id: 678
 * user_id: 689
 */
module.exports.rankUsers = function(matches){

    for (let i = 0; i < matches.length; i++) {
        if(!matches[i].tags_info){
          matches[i].rank = 0;
          continue;
        }
        // let lastMatchDate = JSON.parse(matches[i].tags_info[0]).timestamp;
        let lastMatchDate = matches[i].last_update;
        matches[i].rank = _calculateHotValue(matches[i].tags_info.length, lastMatchDate);
    }
    matches.sort( (a,b) => b.rank - a.rank); // desc
    return matches;
}


function _compare(a, b) {
    if (a.rank < b.rank) return 1;
    if (a.rank > b.rank) return -1;
    return 0;
}


/*
 * Variation of the reddit algorith
 */
function _calculateHotValue(score, date){

    // Distintas formas de obtener timestamp en segundos
    // Math.floor(Date.now() / 1000);
    // Date.now() / 1000 | 0
    // Math.round(new Date().getTime()/1000)


    // Convertir la fecha recibida en milisegundos a segundos
    date =  Math.floor( (new Date(date)).getTime() / 1000 );
    var order = Math.log10( Math.max(Math.abs(score), 1) );
    var sign = 0;
    if (score > 0) {
        sign = 1;
    } else if (score < 0) {
        sign = -1;
    }

    var seconds = date - 1134028003;
    var hotValue = Math.round(sign + order * seconds / 45000, 7);

    return hotValue;
}

function _calculateRelationValue(type, level){

    var value = 1;
    switch (type) {

        case tagConstants.TYPE_OFFER:
        case tagConstants.TYPE_REQUEST:
        case tagConstants.TYPE_LANGUAGE:
            value = 100;
            break;
        case tagConstants.TYPE_SKILL:
        case tagConstants.TYPE_BEAT:
        case tagConstants.TYPE_FOCUS:
            value = 90;
            break;
        case tagConstants.TYPE_DISCUSSION_POST:
            value = 80;
            break;

        case tagConstants.TYPE_DISCUSSION_POST_REPLY:
        case tagConstants.TYPE_DISCUSSION_REPLY:
            value = 70;
            break;

        case tagConstants.TYPE_DISCUSSION_POST_UP:
        case tagConstants.TYPE_DISCUSSION_REPLY_UP:
            value = 60;
            break;

        case tagConstants.TYPE_DISCUSSION_PIN:
            value = 50;
            break;

        case tagConstants.TYPE_DISCUSSION_POST_DOWN:
        case tagConstants.TYPE_DISCUSSION_REPLY_DOWN:
            value = 40;
            break;

        default:
            console.log('ALERTA !!! tipo de tag no contemplado en el experto de matches');
    }

    if(level > 1){
        return  parseInt( value / (level * 1.5) );
    }else{
        return  parseInt( value );
    }

}
