"use strict";
const mandrill = require('mandrill-api/mandrill');
const mandrillClient = new mandrill.Mandrill('7hdzC2XeNONYkMf2Om0VoQ');
const activityConstants = require('../constants/activityConstants.js');
const journalist = require('../controllers/graph/journalists.js');
const sessionControl = require('../controllers/sessionControl');

const graphConnections = require('../controllers/graph/graphConnections');


// const MAIN_URL = "http://localhost:8000/";
const MAIN_URL = "https://kulectiv.com/";
const SERVER_KOA_URL = 'https://kulectiv.com:3000/';

//MODULES
const GET_PIC = SERVER_KOA_URL + 'images/get.email.profile.picture/';
const GET_PIC_MARKETPLACE = SERVER_KOA_URL + 'images/get.marketplace.picture/';
const MESSAGE_MODULE = '#!/message/';
const DISCUSSION_MODULE ='#!/discussion/dis/';
const PROJECT_MODULE ='#!/project/item/';
const MARKETPLACE_MODULE ='#!/marketplace/item/';
const PROFILE_MODULE = '#!/profile/';
const PASSWORD_RESET_MODULE = '#!/password.reset/code/';
const SIGNUP_PASSWORD_CREATE_MODULE = '#!/password/';

//subjects
const NEW_MESSAGE_SUBJECT = 'New message from';
const DISCUSSION_POST_REPLAY_SUBJECT = ' replied to your discussion post';
const NEW_ITEM_SUBJECT = 'New item added';
const ITEM_INTEREST_EXPRESSED_SUBJECT = ' is interested in your gear post!';
const ACCOUNT_DEACTIVATED_SUBJECT = 'Attention: Your Kulectiv account has been deactivated';
const ACCOUNT_REJECTED_SUBJECT = 'Attention: Your Kulectiv account has been suspended';
const FIRST_REMINDER_SUBJECT = 'Tik tok! You have 12 hours left before your account is deactivated';
const TIME_EXTENDED_SUBJECT = "Hooray! We've extended the deadline for another 24hrs. Complete your profile";
const PASSWORD_RESET_SUBJECT = "Oops! seems like you forgot your password";
const WELCOME_TWITTER_EMAIL = "Welcome to Kulectiv.";
const EMAIL_VERIFICATION_SUBJECT = "Welcome to Kulectiv.";

module.exports.emailNotifications = function(notification, recipientData){

    let senderData;
    let templateName;
    let message;
    let templateContent;
    let subject;
    let actionButtonData;
    let unsubscribeLink;

    switch(notification.type){

        case activityConstants.DISCUSSION_POST:
            unsubscribeLink = _buildUnsubscribeLink(recipientData);
            templateName = 'discussion-new';
            senderData = _getDiscussionSenderData(notification);
            message = _buildMessageParam(recipientData, _buildSubject(notification.details[0], 40));
            templateContent = _newPostTemplateContent(senderData, notification, unsubscribeLink);
            break;

        case activityConstants.PROJECT_ADD_MEMBER:
            unsubscribeLink = _buildUnsubscribeLink(recipientData);
            templateName = 'project-new';
            senderData = _getDiscussionSenderData(notification);

            subject = _buildSubject('Newsroom proposal: ' + notification.details[0], 40);
            message = _buildMessageParam(recipientData, subject);
            templateContent = _newProjectTemplateContent(senderData, notification, unsubscribeLink);
            break;


        case activityConstants.DISCUSSION_POST_REPLY:
            subject = notification.user_name + DISCUSSION_POST_REPLAY_SUBJECT;
            message = _buildMessageParam(recipientData, _buildSubject(notification.details[0], 40));
            templateContent = _buildDiscussionPostReplayTemplateContent(notification, recipientData);
            templateName = 'discussion-replay';
            break;

        case activityConstants.MARKETPLACE_POST:
            templateName = 'gear-new-item';
            templateContent = _newGearItemTemplateContent(notification);
            message = _buildMessageParam(recipientData, _buildSubject(notification.details[0], 40));
            break;

        case activityConstants.MARKETPLACE_POST_REPLY:

            templateName = 'gear-post-replay';
            subject = notification.users_name[0] +  ITEM_INTEREST_EXPRESSED_SUBJECT;
            message = _buildMessageParam(recipientData, subject);

            actionButtonData ={
                label: 'View message on Kulectiv',
                url: MAIN_URL + MESSAGE_MODULE
            };

            templateContent = _buildTemplateContent(notification, actionButtonData, recipientData);
            break;

        case activityConstants.USER_ENGAGE_REQUEST:

            // var seconds = Math.floor(Math.random()*(70-2+1)+2);
            // var locationString = 'Sent '+seconds+' seconds ago.';
            // var picture = notification.user_pic || 'default.jpg';
            // senderData = {
            //     name: notification.user_name,
            //     id: notification.user_id,
            //     pic: picture,
            //     location: locationString
            // };
            //
            // const templateContent = _makeNewMessageTemplateContent(senderData, recipientData);
            // const messages = _buildMessageParam(recipientData, NEW_MESSAGE_SUBJECT + ' ' + message.sender_name);
            //
            // const sendParams = {
            //     "template_name": templateName,
            //     "template_content": templateContent,
            //     "message": messages,
            //     "async": false
            // };
            //
            // _sendEmail(sendParams);


            // templateName = 'engage-request';
            // subject = "New engagement request from " + notification.users_name[0];
            // message = _buildMessageParam(recipientData, subject);
            //
            // actionButtonData ={
            //     label: 'View response on Kulectiv',
            //     url: MAIN_URL + MESSAGE_MODULE
            // };
            //damian
            // templateContent = _buildTemplateContent(notification, actionButtonData, recipientData);
            // break;

        case activityConstants.USER_ENGAGE_ACCEPT:
            templateName = 'agreed-engagement';
            subject = "Yes, you've received a response from " + notification.users_name[0] + " on your engagement request";
            message = _buildMessageParam(recipientData, subject);

            actionButtonData ={
                label: 'View response on Kulectiv',
                url: MAIN_URL + MESSAGE_MODULE
            };

            templateContent = _buildTemplateContent(notification, actionButtonData, recipientData);
            break;

        default:
            return;
    }

    const sendParams = {
        "template_name": templateName,
        "template_content": templateContent,
        "message": message,
        "async": false
    };

    _sendEmail(sendParams);

};

module.exports.emailPasswordRecovery = function(recipientData, hashedCode){

    const message = _buildMessageParam(recipientData, PASSWORD_RESET_SUBJECT);
    const templateContent = _getResetPasswordTemplateContent(recipientData, hashedCode);

    const sendParams = {
        "template_name": "password-reset",
        "template_content": templateContent,
        "message": message,
        "async": false
    };

    _sendEmail(sendParams);
};


// module.exports.sendEmailMessage = function(threadId, senderData, recipientsData){
module.exports.sendEmailMessage = function(message, index){

    const journalist = require('../controllers/graph/journalists.js');

    let senderData;
    const templateName = "new-message";
    let recipientData;

    const getRecipient = journalist.getUser(message.users[index]).next().value;
    getRecipient(getRecipientCallback);

    function getRecipientCallback(error, recipientResult) {

        if(!recipientResult.first() || !recipientResult.first().email || !recipientResult.first().email[0]){
            return
        }

        recipientData = {
            name: message.names[index],
            id: message.users[index],
            email: recipientResult.first().email[0]
        };

        const getSender = journalist.getUser(message.sender_id).next().value;
        getSender(getSendercallback);


        function getSendercallback(algo, result) {

            var locationString = result.first().location[0] ? result.first().location[0] : '';
            var seconds = Math.floor(Math.random()*(70-2+1)+2);

            locationString += '. Sent '+seconds+' seconds ago.';
            var picture = result.first().picture ? result.first().picture[0] : message.sender_id+'.jpg';
            senderData = {
                name: message.sender_name,
                id: message.sender_id,
                pic: picture,
                location: locationString
            };

            const templateContent = _makeNewMessageTemplateContent(senderData, recipientData);
            const messages = _buildMessageParam(recipientData, NEW_MESSAGE_SUBJECT + ' ' + message.sender_name);

            const sendParams = {
                "template_name": templateName,
                "template_content": templateContent,
                "message": messages,
                "async": false
            };

            _sendEmail(sendParams);
        }
    }
};

module.exports.sendEmailEngagementRequest = function(message, index){

    const journalist = require('../controllers/graph/journalists.js');

    let senderData;
    const templateName = "engagement-request";
    let recipientData;

    const getRecipient = journalist.getUser(message.users[index]).next().value;
    getRecipient(getRecipientCallback);

    function getRecipientCallback(error, recipientResult) {

        if(!recipientResult.first() || !recipientResult.first().email || !recipientResult.first().email[0]){
            return
        }

        recipientData = {
            name: message.names[index],
            id: message.users[index],
            email: recipientResult.first().email[0]
        };

        const getSender = journalist.getUser(message.sender_id).next().value;
        getSender(getSendercallback);


        function getSendercallback(algo, result) {

            var locationString = result.first().location[0] ? result.first().location[0] : '';
            var seconds = Math.floor(Math.random()*(70-2+1)+2);

            locationString += '. Sent '+seconds+' seconds ago.';
            var picture = result.first().picture ? result.first().picture[0] : message.sender_id+'.jpg';
            senderData = {
                name: message.sender_name,
                id: message.sender_id,
                pic: picture,
                location: locationString
            };

            const templateContent = _makeNewMessageTemplateContent(senderData, recipientData);
            const messages = _buildMessageParam(recipientData, NEW_MESSAGE_SUBJECT + ' ' + message.sender_name);

            const sendParams = {
                "template_name": templateName,
                "template_content": templateContent,
                "message": messages,
                "async": false
            };

            _sendEmail(sendParams);
        }
    }
};

module.exports.sendDeactivateEmail = function (recipientData) {

    let message;
    let sendParams;

    if (recipientData.reject) {

        message = _buildMessageParam(recipientData, ACCOUNT_REJECTED_SUBJECT);

        sendParams = {
            "template_name": 'account-rejected',
            "template_content": "",
            "message": message,
            "async": false
        };

    } else{

        message = _buildMessageParam(recipientData, ACCOUNT_DEACTIVATED_SUBJECT);

        const templateContent =  [
            {
                "name": "reinstate-account",
                "content": '<a class="mcnButton " title="Reinstate my account" ' +
                'href="'+ MAIN_URL + PROFILE_MODULE + recipientData.user_id + '" ' +
                'target="_self" style="font-weight: normal;letter-spacing: normal;line-height: 100%;' +
                'text-align: center;text-decoration: none;color: #FFFFFF;">' +
                'Reinstate my account' +
                '</a>'
            }
        ];

        sendParams = {
            "template_name": 'account-deactivated',
            "template_content": templateContent,
            "message": message,
            "async": false
        };
    }

    _sendEmail(sendParams);


};

module.exports.sendRemainderEmail = function (recipientData) {

    const message = _buildMessageParam(recipientData, FIRST_REMINDER_SUBJECT);

    const templateContent =  [
        {
            "name": "complete-profile-button",
            "content": '<a class="mcnButton " title="Click here to complete your profile" ' +
            'href="'+ MAIN_URL + PROFILE_MODULE + recipientData.user_id + '" ' +
            'target="_self" style="font-weight: normal;letter-spacing: normal;line-height: 100%;' +
            'text-align: center;text-decoration: none;color: #FFFFFF;">' +
            'Click here to complete your profile' +
            '</a>'
        }
    ];

    const sendParams = {
        "template_name": 'profile-reminder-1',
        "template_content": templateContent,
        "message": message,
        "async": false
    };

    _sendEmail(sendParams);
};


module.exports.sendTimeExtendedEmail = function (recipientData) {

    const message = _buildMessageParam(recipientData, TIME_EXTENDED_SUBJECT);

    const templateContent =  [
        {
            "name": "go-profile-button",
            "content": '<a class="mcnButton " title="Click here to complete your profile" ' +
            'href="'+ MAIN_URL + PROFILE_MODULE + recipientData.user_id + '" ' +
            'target="_self" style="font-weight: normal;letter-spacing: normal;line-height: 100%;' +
            'text-align: center;text-decoration: none;color: #FFFFFF;">' +
            'Click here to complete your profile' +
            '</a>'
        }
    ];

    const sendParams = {
        "template_name": 'time-extended',
        "template_content": templateContent,
        "message": message,
        "async": false
    };

    _sendEmail(sendParams);
};

module.exports.sendTwitterWelcomeEmail = function (userTwitter) {

    const message = _buildMessageParam(userTwitter, WELCOME_TWITTER_EMAIL);

    const sendParams = {
        "template_name": 'welcome-email-twitter',
        "template_content": [],
        "message": message,
        "async": false
    };

    _sendEmail(sendParams);
};

module.exports.sendEmailVerification = function (recipientData, hash_code) {

    const message = _buildMessageParam(recipientData, EMAIL_VERIFICATION_SUBJECT );

    const templateContent = [
        {
        "name":"verify-button",
        "content": '<a class="mcnButton " title="Use this email address" ' +
        'href="'+ MAIN_URL + SIGNUP_PASSWORD_CREATE_MODULE + hash_code + '/' + recipientData.user_email + '/' + recipientData.user_name +'" ' +
        'target="_self" style="font-weight: normal;letter-spacing: normal;line-height: 100%;' +
        'text-align: center;text-decoration: none;color: #FFFFFF;">' +
        'Use this email address' +
        '</a>'
        }
    ];
    const sendParams = {
        "template_name": 'email-verification',
        "template_content": templateContent,
        "message": message,
        "async": false
    };


    _sendEmail(sendParams);
};



function _buildUnsubscribeLink(userData){
    let noexpire = true;
    let payload = sessionControl.buildPayload(userData, noexpire);
    let hash = sessionControl.getKOASESSID(payload);

    return '<a href="'+SERVER_KOA_URL+'notifications/unsubscribe/new_discussions/'+hash+'">Unsubscribe from these emails</a>';
}

function _getResetPasswordTemplateContent(recipientData, hashedCode){
    return [
        {
            "name":"recipient-name",
            "content": recipientData.name,

        },
        {
            "name":"go-password-reset-button",
            "content": '<a class="mcnButton " title="View discussion post" ' +
            'href="'+ MAIN_URL + PASSWORD_RESET_MODULE + hashedCode + '/user/' + recipientData.name + '/email/' + recipientData.email +'" ' +
            'target="_self" style="font-weight: normal;letter-spacing: normal;line-height: 100%;' +
            'text-align: center;text-decoration: none;color: #FFFFFF;">' +
            'GO TO CHANGE PASSWORD' +
            '</a>'

        }
    ]
}

function _getEngageAgreedSenderData(notification) {

    let senderData = {
        name: notification.user_name,
        id: notification.user_id,
        pic: notification.user_pic,
        location: notification.details[0],
        // currentPosition: 'Developer'
    };

    return senderData;
}

function _buildSubject(text, length) {
    if (text.length > length) {
        return text.substr(0,length - 1) + ' ...';
    }
    return text;
}


function _getDiscussionSenderData(notification){

    let senderData = {
        name: notification.user_name,
        id: notification.user_id,
        pic: notification.user_pic,
        location: notification.details[2],
    };

    return senderData;
}



function _buildDiscussionPostReplayTemplateContent(notification, recipientData) {

    let discussionID = JSON.parse(notification.objects_id[1]);
    return [
        {
            "name":"discussion-title",
            "content": notification.details[1]
        },
        {
            "name":"recipient-name",
            "content": recipientData.user_name
        },
        {

            "name": "view-response-button",
            "content": '<a class="mcnButton " title="View discussion post" ' +
            'href="'+ MAIN_URL+DISCUSSION_MODULE+discussionID.uuid+'/'+ Number(new Date(discussionID.timestamp)) +'" ' +
            'target="_self" style="font-weight: normal;letter-spacing: normal;line-height: 100%;' +
            'text-align: center;text-decoration: none;color: #FFFFFF;">' +
            'View discussion post' +
            '</a>'
        },

    ]
}

function _newPostTemplateContent(senderData, notification, unsubscribeLink) {

    let discussionID = JSON.parse(notification.target_object_id);

    return [
        {
            "name":"sender-picture-name",
            "content":'<img align="left" height="47" src="' + GET_PIC +'mini/'+ senderData.pic + '" ' +
            'style="width: 47px; height: 47px; margin: 0px 20px 5px 7px; border-radius:50%" width="47"> ' + senderData.name
        },
        {
            "name": "sender-location",
            "content": senderData.location
        },
        {
            "name" : "discussion-title",
            "content":notification.details[0]
        },

        {
            "name" : "discussion-body",
            "content": notification.details[1]
        },
        {
            "name": "view-discussion-button",
            "content": '<a class="mcnButton " title="View discussion post" ' +
            'href="'+ MAIN_URL+DISCUSSION_MODULE + discussionID.uuid +'/'+ Number(new Date(discussionID.timestamp)) + '" ' +
            'target="_self" style="font-weight: normal;letter-spacing: normal;line-height: 100%;' +
            'text-align: center;text-decoration: none;color: #FFFFFF;">' +
            'View discussion post' +
            '</a>'
        },
        {
            "name":"unsubscribe-link",
            "content": unsubscribeLink
        },
    ]

}

function _newProjectTemplateContent(senderData, notification, unsubscribeLink) {

    let discussionID = JSON.parse(notification.target_object_id);

    return [
        {
            "name":"sender-picture-name",
            "content":'<img align="left" height="47" src="' + GET_PIC +'mini/'+ senderData.pic + '" ' +
            'style="width: 47px; height: 47px; margin: 0px 20px 5px 7px; border-radius:50%" width="47"> ' + senderData.name
        },
        {
            "name": "sender-location",
            "content": senderData.location
        },
        {
            "name" : "discussion-title",
            "content":notification.details[0]
        },

        {
            "name" : "discussion-body",
            "content": notification.details[1]
        },
        {
            "name": "view-discussion-button",
            "content": '<a class="mcnButton " title="View discussion post" ' +
            'href="'+ MAIN_URL+PROJECT_MODULE + discussionID.uuid +'/'+ Number(new Date(discussionID.timestamp)) + '" ' +
            'target="_self" style="font-weight: normal;letter-spacing: normal;line-height: 100%;' +
            'text-align: center;text-decoration: none;color: #FFFFFF;">' +
            'View private project' +
            '</a>'
        },
        {
            "name":"unsubscribe-link",
            "content": unsubscribeLink
        },
    ]

}

function _newGearItemTemplateContent(notification) {

    return [
        {
            "name" : "item-title",
            "content":notification.details[0]
        },

        {
            "name" : "item-pic",
            "content": '<img align="center" ' +
            'src="' + GET_PIC_MARKETPLACE + notification.details[1] + '" ' +
            'width="400" ' +
            'style="max-width:400px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" ' +
            'class="mcnImage">'
        },
        {
            "name" : "item-detail",
            "content":notification.details[2]
        },
        {
            "name": "view-item-button",
            "content": '<a class="mcnButton " title="View item on Kulectiv" ' +
            'href="'+ MAIN_URL + MARKETPLACE_MODULE + notification.objects_id[0] + '" ' +
            'target="_self" style="font-weight: normal;letter-spacing: normal;line-height: 100%;' +
            'text-align: center;text-decoration: none;color: #FFFFFF;">' +
            'Contact owner' +
            '</a>'
        }
    ]
}




function _makeNewMessageTemplateContent(senderData, recipientData) {

    return [
        {
            "name":"sender-picture-name",
            "content":'<img align="left" height="47" src="' + GET_PIC +'mini/'+ senderData.pic +'" '+
            'style="width: 47px; height: 47px; margin: 0px 20px 5px 7px; border-radius:50%" width="47"> ' + senderData.name
        },
        {
            "name": "sender-current-position",
            "content": senderData.currentPosition + '&nbsp;'
        },
        {
            "name": "sender-location",
            "content": senderData.location
        },
        {
            "name" : "recipient-name",
            "content": recipientData.name
        },
        {
            "name": "view-message-button",
            "content": '<a class="mcnButton " title="View message on Kulectiv" ' +
            'href="'+ MAIN_URL + MESSAGE_MODULE + '" ' +
            'target="_self" style="font-weight: normal;letter-spacing: normal;line-height: 100%;' +
            'text-align: center;text-decoration: none;color: #FFFFFF;">' +
            'View message on Kulectiv' +
            '</a>'
        },

    ];
}

function _sendEmail(params) {

    mandrillClient.messages.sendTemplate(params, function(result) {

    }, function(e) {
        // Mandrill returns the error as an object with name and message keys
        console.error('A mandrill error occurred: ' + e.name + ' - ' + e.message);
        // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
    });
}

function _buildMessageParam(recipientData, subject) {
    return {
        "subject": subject,
        "from_email": "no-reply@kulectiv.com",
        "from_name": "Kulectiv",
        "to": [{
            // "email": "daniel.stocco@gmail.com",
            "email": recipientData.email || recipientData.user_email,
            "name": recipientData.name || recipientData.user_name,
            "type": "to"
        }],
        "headers": {
            "Reply-To": "no-reply@kulectiv.com"
        },
        "important": true,
        "track_opens": true,
        "track_clicks": true,
        "preserve_recipients": true,
        "metadata": {
            "website": "www.kulectiv.com"
        },
        "recipient_metadata": [{
            "rcpt": recipientData.email || recipientData.user_email,
            "values": {
                "user_id": recipientData.user_id || recipientData.id || recipientData.twitter_id || null,
                "user_name": recipientData.name || recipientData.user_name,
            }
        }],
    };

}

function _buildTemplateContent (notification, actionButtonData, recipientData) {

    notification.user_pic = notification.user_pic || 'default.jpg';

    return [
        {
            "name":"sender-picture",
            "content":'<img align="left" height="47" src="' + GET_PIC +'mini/'+ notification.user_pic + '" '+
            'style="width: 47px; height: 47px; margin: 0px 20px 5px 7px; border-radius:50%" width="47"> '
        },
        {
            "name": "sender-name",
            "content": notification.user_name
        },
        {
            "name": "sender-location",
            "content": notification.details[0]
        },
        {
            "name" : "recipient-name",
            "content": recipientData.user_name
        },
        {
            "name": "action-button",
            "content": '<a class="mcnButton " title="'+ actionButtonData.label +'" ' +
            'href="'+ actionButtonData.url + '" ' +
            'target="_self" style="font-weight: normal;letter-spacing: normal;line-height: 100%;' +
            'text-align: center;text-decoration: none;color: #FFFFFF;">' + actionButtonData.label + '</a>'
        },

    ];
}
